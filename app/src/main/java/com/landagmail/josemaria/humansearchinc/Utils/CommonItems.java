package com.landagmail.josemaria.humansearchinc.Utils;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.support.v7.app.AlertDialog;

import com.facebook.Profile;
import com.facebook.login.LoginManager;
import com.landagmail.josemaria.humansearchinc.Activities.InitScreens.WelcomeActivity;
import com.landagmail.josemaria.humansearchinc.MainApplication;
import com.landagmail.josemaria.humansearchinc.R;
import com.landagmail.josemaria.humansearchinc.Tasks.POST.POJO.ResponseClasses.MyQuestionFetchResponseClass;
import com.landagmail.josemaria.humansearchinc.Tasks.POST.Queries.PostHelperClass;
import com.landagmail.josemaria.humansearchinc.Utils.Storage.SharedPreferences.ComplexPreferences;
import com.landagmail.josemaria.humansearchinc.Utils.UrbanAirshipUtils.DeviceAdressing;
import com.landagmail.josemaria.humansearchinc.adapters.MyActivityAdapter;
import com.urbanairship.UAirship;

import java.util.ArrayList;

public class CommonItems {

    public static final String NAME_PREFERENCE = "NAME_PREFERENCE";
    public static final String USERNAME = "USERNAME";

    static final String PICTURE_PREFERENCE = "PICTURE_PREFERENCE";
    static final String PROFILE_PICTURE_URL = "PICTURE_URL";
    private static final String VIEW_PREFERENCE = "VIEW_PREFERENCE";
    private static final String MAP_VISIBLE = "MAP_VISIBLE";

    public static String getName() {
        return MainApplication.getAppContext().getSharedPreferences(NAME_PREFERENCE, Context.MODE_PRIVATE).getString(USERNAME, null);
    }

    public static void setName(String alias) {
        SharedPreferences preferences = MainApplication.getAppContext().getSharedPreferences(NAME_PREFERENCE, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.clear();
        editor.putString(USERNAME, alias);
        editor.apply();
    }

    public static String getProfilePictureUrl() {
        return MainApplication.getAppContext().getSharedPreferences(PICTURE_PREFERENCE, Context.MODE_PRIVATE).getString(PROFILE_PICTURE_URL, null);
    }

    public static void setProfilePictureUrl(String pictureUrl) {
        MainApplication.getAppContext().getSharedPreferences(PICTURE_PREFERENCE, Context.MODE_PRIVATE).edit().putString(PROFILE_PICTURE_URL, pictureUrl).apply();
    }

    public static void logout(final Activity context) {
        AlertDialog alertDialog = new AlertDialog.Builder(context).create();
        alertDialog.setMessage(context.getString(R.string.signOut));
        alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, context.getString(R.string.confirmTxt), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                DeviceAdressing.NamedUsers.disassociateChannelFromNamedUserID();
                // LOGOUT FROM FACEBOOK
                LoginManager.getInstance().logOut();
                // DELETE SHARED_PREFERENCES
                MainApplication.getAppContext().getSharedPreferences("PREFERENCE", Context.MODE_PRIVATE).edit()
                        .putBoolean("isFirstRun", true).commit();

                MainApplication.getAppContext().getSharedPreferences("PREFERENCE", Context.MODE_PRIVATE).edit()
                        .putBoolean("firstTimeUser", true).commit();

                MainApplication.getAppContext().getSharedPreferences("PREFERENCE", Context.MODE_PRIVATE).edit()
                        .putBoolean("isPictureAndNameSet", false).commit();

                context.finish();
                Intent intent = new Intent(context, WelcomeActivity.class);
                context.startActivity(intent);
                ComplexPreferences preferences = MainApplication.getInstance().getComplexPreference();
                preferences.clear();
                preferences.commit();
            }
        });
        alertDialog.setButton(DialogInterface.BUTTON_NEGATIVE, context.getString(R.string.cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // DO NOTHING
            }
        });
        alertDialog.show();
    }

    public static void dissociate_question(final Context context, final int position, final ArrayList<MyQuestionFetchResponseClass> questionArrayList, final MyActivityAdapter adapter, final String userID, final String questionID) {
        AlertDialog alertDialog = new AlertDialog.Builder(context).create();
        alertDialog.setMessage(context.getString(R.string.unfollowQuestion2));
        alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, context.getString(R.string.unfollow), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                PostHelperClass.CartoDBQueries.dissociate_question(userID, questionID);
                questionArrayList.remove(position);
                adapter.setData(questionArrayList);
            }
        });
        alertDialog.setButton(DialogInterface.BUTTON_NEGATIVE, context.getString(R.string.cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // DO NOTHING
            }
        });
        alertDialog.show();
    }

    public static void delete_question(final Context context,
                                       final int position, final ArrayList<MyQuestionFetchResponseClass> questionArrayList,
                                       final MyActivityAdapter adapter, final String userID, final String questionID) {
        AlertDialog alertDialog = new AlertDialog.Builder(context).create();
        alertDialog.setMessage(context.getString(R.string.deleteQuestion));
        alertDialog.setIcon(R.drawable.com_facebook_button_send_icon);
        alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, context.getString(R.string.delete), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                PostHelperClass.CartoDBQueries.delete_questions(userID, questionID);
                questionArrayList.remove(position);
                adapter.setData(questionArrayList);
            }
        });
        alertDialog.setButton(DialogInterface.BUTTON_NEGATIVE, context.getString(R.string.cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // DO NOTHING
            }
        });
        alertDialog.show();
    }

    public static boolean openApp(Context context, String packageName) {
        PackageManager manager = context.getPackageManager();
        Intent i = manager.getLaunchIntentForPackage(packageName);
        if (i == null) {
            return false;
            //throw new PackageManager.NameNotFoundException();
        }
        i.addCategory(Intent.CATEGORY_LAUNCHER);
        context.startActivity(i);
        return true;
    }

    public static void setFragmentViewPrefs(boolean trueFalse) {
        SharedPreferences preferences = MainApplication.getAppContext().getSharedPreferences(VIEW_PREFERENCE, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.clear();
        editor.putBoolean(MAP_VISIBLE, trueFalse);
        editor.apply();
    }

    public static boolean getFragmentViewPrefs() {
        return MainApplication.getAppContext().getSharedPreferences(VIEW_PREFERENCE, Context.MODE_PRIVATE).getBoolean(MAP_VISIBLE, true);
    }
}
