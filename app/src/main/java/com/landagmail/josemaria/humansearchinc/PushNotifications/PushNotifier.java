package com.landagmail.josemaria.humansearchinc.PushNotifications;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import com.landagmail.josemaria.humansearchinc.MainActivity;
import com.landagmail.josemaria.humansearchinc.MainApplication;
import com.landagmail.josemaria.humansearchinc.R;

public class PushNotifier {

    static Uri uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
    private static int mid = 0;

    public static void sendShortPush(String title) {

        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(MainApplication.getAppContext())
                        .setSmallIcon(R.drawable.speak_icon)
                        .setContentTitle(title)
                        .setSound(uri);

        Intent resultIntent = new Intent(MainApplication.getAppContext(), MainActivity.class);
        resultIntent.putExtra("NOTIFICATION", true);

        TaskStackBuilder stackBuilder = TaskStackBuilder.create(MainApplication.getAppContext());
        // Adds the back stack for the Intent (but not the Intent itself)
        stackBuilder.addParentStack(MainActivity.class);
        // Adds the Intent that starts the Activity to the top of the stack
        stackBuilder.addNextIntent(resultIntent);
        PendingIntent resultPendingIntent =
                stackBuilder.getPendingIntent(
                        0,
                        PendingIntent.FLAG_UPDATE_CURRENT
                );
        mBuilder.setContentIntent(resultPendingIntent);
        NotificationManager mNotificationManager =
                (NotificationManager) MainApplication.getAppContext().getSystemService(Context.NOTIFICATION_SERVICE);
        // mId allows you to update the notification later on.
        mNotificationManager.notify(mid++, mBuilder.build());
    }

}
