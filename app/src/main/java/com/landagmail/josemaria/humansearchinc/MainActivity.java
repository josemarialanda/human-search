package com.landagmail.josemaria.humansearchinc;

import android.Manifest;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.facebook.Profile;
import com.facebook.appevents.AppEventsLogger;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.single.PermissionListener;
import com.landagmail.josemaria.humansearchinc.Activities.ProfileActivity;
import com.landagmail.josemaria.humansearchinc.Fragments.MapFragment;
import com.landagmail.josemaria.humansearchinc.Fragments.MyActivityFragment;
import com.landagmail.josemaria.humansearchinc.Fragments.LocalQuestionsFragment;
import com.landagmail.josemaria.humansearchinc.Logging.L;
import com.landagmail.josemaria.humansearchinc.Services.NewAnswersMonitorService;
import com.landagmail.josemaria.humansearchinc.Services.QuestionMonitorService;
import com.landagmail.josemaria.humansearchinc.Tasks.POST.Queries.PostHelperClass;
import com.landagmail.josemaria.humansearchinc.Utils.CommonItems;
import com.landagmail.josemaria.humansearchinc.Utils.Tabs.SlidingTabLayout;
import com.urbanairship.analytics.Analytics;
import com.google.android.gms.gcm.GcmNetworkManager;
import com.google.android.gms.gcm.PeriodicTask;
import com.google.android.gms.gcm.Task;
import com.urbanairship.google.PlayServicesUtils;

public class MainActivity extends AppCompatActivity implements LocationListener, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    //Track App Installs and App Opens
    //you'll see this data reflected in your app's Insights dashboard. (FACEBOOK)

    private LocationRequest mLocationRequest;
    private GoogleApiClient mGoogleApiClient;

    @Override
    public void onConnected(Bundle bundle) {
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onLocationChanged(Location location) {
        MainApplication.setIsLocationSet(true);
        MainApplication.setLocation(location);

        Intent intent;

        intent = new Intent(LocalQuestionsFragment.ResponseReceiver.ACTION_RESP);
        intent.putExtra(LocalQuestionsFragment.ResponseReceiver.INIT, "start_fetching");
        sendBroadcast(intent);

        intent = new Intent(MapFragment.ResponseReceiver.ACTION_RESP);
        intent.putExtra(MapFragment.ResponseReceiver.ACTION_LOCATION_SET, MapFragment.ResponseReceiver.ACTION_RESP);
        sendBroadcast(intent);
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    @Override
    protected void onResume() {
        super.onResume();
        // Logs 'install' and 'app activate' App Events.
        AppEventsLogger.activateApp(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        // Logs 'app deactivate' App Event.
        AppEventsLogger.deactivateApp(this);
        stopLocationUpdates();
    }

    protected void stopLocationUpdates() {
        if (mGoogleApiClient.isConnected()) {
            LocationServices.FusedLocationApi.removeLocationUpdates(
                    mGoogleApiClient, this);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        Analytics.activityStarted(this);

        Dexter.checkPermission(new PermissionListener() {
            @Override
            public void onPermissionGranted(PermissionGrantedResponse response) {
                mGoogleApiClient.connect();
            }

            @Override
            public void onPermissionDenied(PermissionDeniedResponse response) {
                L.m(getString(R.string.locationPermissionDenied));
            }

            @Override
            public void onPermissionRationaleShouldBeShown(PermissionRequest permission, PermissionToken token) {
                token.continuePermissionRequest();
            }
        }, Manifest.permission.ACCESS_FINE_LOCATION);

        if (PlayServicesUtils.isGooglePlayStoreAvailable(this)) {
            PlayServicesUtils.handleAnyPlayServicesError(this);
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        Analytics.activityStopped(this);
        if (mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
    }

    private int taskID = 1;
    PeriodicTask periodicTaskAnswer;
    PeriodicTask periodicTaskQuestion;

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);

        long periodSecs = Constants.INTERVAL_FIFTEEN_MINUTES;
        long flexSecs = 10L;

        String tag = "periodic  | " + taskID++ + ": " + periodSecs + "s, f:" + flexSecs;  // a unique task identifier

        periodicTaskAnswer = new PeriodicTask.Builder()
                .setService(NewAnswersMonitorService.class)
                .setPeriod(periodSecs)
                .setFlex(flexSecs)
                .setTag(tag)
                .setPersisted(true)
                .setRequiredNetwork(Task.NETWORK_STATE_CONNECTED)
                .setRequiresCharging(false)
                .build();
        GcmNetworkManager.getInstance(this).schedule(periodicTaskAnswer);

        periodicTaskQuestion = new PeriodicTask.Builder()
                .setService(QuestionMonitorService.class)
                .setPeriod(periodSecs)
                .setFlex(flexSecs)
                .setTag(tag)
                .setPersisted(true)
                .setRequiredNetwork(Task.NETWORK_STATE_CONNECTED)
                .setRequiresCharging(false)
                .build();
        GcmNetworkManager.getInstance(this).schedule(periodicTaskQuestion);
    }

    private void isPictureAndNameSet() {
        Boolean isFirstRun = getSharedPreferences("PREFERENCE", MODE_PRIVATE)
                .getBoolean("isFirstRun", true);
        if (!isFirstRun) {
            Boolean isPictureAndNameSet = getSharedPreferences("PREFERENCE", MODE_PRIVATE)
                    .getBoolean("isPictureAndNameSet", false);
            if (!isPictureAndNameSet) {
                PostHelperClass.ProfileQueries.fetch_profile(Profile.getCurrentProfile().getId());
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle("LOKL");
            //getSupportActionBar().setIcon(R.drawable.logo_actionbar);
        }

        isPictureAndNameSet();

        mLocationRequest = LocationRequest.create();
        //mLocationRequest.setInterval(10000);
        //mLocationRequest.setFastestInterval(5000);
        mGoogleApiClient = new GoogleApiClient.Builder(this).addApi(LocationServices.API).addConnectionCallbacks(this).addOnConnectionFailedListener(this).build();

        ViewPager mPager = (ViewPager) findViewById(R.id.home_fragment_pager_questionsAndAnswers);
        FragmentManager manager = getSupportFragmentManager();
        mPager.setAdapter(new HomePagerAdapter(manager));
        mPager.setCurrentItem(1);
        mPager.setOffscreenPageLimit(4);
        SlidingTabLayout mTabs = (SlidingTabLayout) findViewById(R.id.home_fragment_tabs_questionsAndAnswers);
        mTabs.setDistributeEvenly(true);
        mTabs.setSelectedIndicatorColors(getResources().getColor(R.color.accentColor));
        mTabs.setCustomTabView(R.layout.tab_indicator, android.R.id.text1);
        mTabs.setViewPager(mPager);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            /*
                Intent intent = new Intent(this, PushPreferencesActivity.class);
                startActivity(intent);
                */
            Intent intent = new Intent(this, ProfileActivity.class);
            startActivity(intent);
        }
        if (id == R.id.action_logout) {
            CommonItems.logout(this);
        }
        return super.onOptionsItemSelected(item);
    }

    private class HomePagerAdapter extends FragmentStatePagerAdapter {

        private String[] tabs = {"Mi actividad", "Local", "Mundo"};
        int length = tabs.length;

        public HomePagerAdapter(FragmentManager fragmentManager) {
            super(fragmentManager);
        }

        @Override
        public int getCount() {
            return length;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return tabs[position];
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0: // Fragment # 0
                    return new MyActivityFragment();
                case 1: // Fragment # 1
                    return new LocalQuestionsFragment();
                case 2: // Fragment # 2
                    return new MapFragment();
                default:
                    return null;
            }
        }
    }
}
