package com.landagmail.josemaria.humansearchinc.Utils.UrbanAirshipUtils;

import android.app.Activity;

import com.urbanairship.UAirship;
import com.urbanairship.push.PushManager;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class PushUtils {

    public static void userPushNotifications(boolean trueFalse) {
        UAirship.shared().getPushManager().setUserNotificationsEnabled(trueFalse);
    }

    public static void pushVibration(boolean trueFalse) {
        PushManager pushManager = UAirship.shared().getPushManager();
        pushManager.setVibrateEnabled(trueFalse);
    }

    public static void pushSound(boolean trueFalse) {
        PushManager pushManager = UAirship.shared().getPushManager();
        pushManager.setSoundEnabled(trueFalse);
    }

    public static void Push(boolean trueFalse){
        PushManager pushManager = UAirship.shared().getPushManager();
        pushManager.setPushEnabled(trueFalse);
    }

    public static void QuitTime(int fromHours, int fromMinutes, int toHours, int toMinutes) throws ParseException {
        SimpleDateFormat formatter = new SimpleDateFormat("hh:mm", Locale.getDefault());
        Date startDate = formatter.parse(fromHours + ":" + fromMinutes);
        Date endDate = formatter.parse(toHours + ":" + toMinutes);
        UAirship.shared().getPushManager().setQuietTimeInterval(startDate, endDate);
        UAirship.shared().getPushManager().setQuietTimeEnabled(true);
    }

    public static class InAppMessagingPrefs {

        private static void setAutoDisplayDelay(long milliseconds) {
            UAirship.shared()
                    .getInAppMessageManager()
                    .setAutoDisplayDelay(milliseconds);
        }

        private static void setDisplayASAP(boolean trueFalse) {
            UAirship.shared()
                    .getInAppMessageManager()
                    .setDisplayAsapEnabled(trueFalse);
        }

        private static void triggerInAppMessage(Activity activity) {
            UAirship.shared()
                    .getInAppMessageManager()
                    .showPendingMessage(activity);
        }

        private static void disableInAppMessages(boolean trueFalse) {
            UAirship.shared()
                    .getInAppMessageManager()
                    .setAutoDisplayEnabled(trueFalse);
        }
    }
}
