package com.landagmail.josemaria.humansearchinc.Services;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;

import com.amazonaws.auth.CognitoCachingCredentialsProvider;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.AmazonS3Exception;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.gson.Gson;
import com.landagmail.josemaria.humansearchinc.AWSUtils.CredentialsProvider;
import com.landagmail.josemaria.humansearchinc.Activities.InitScreens.SetupActivity;
import com.landagmail.josemaria.humansearchinc.Activities.ProfileActivity;
import com.landagmail.josemaria.humansearchinc.Constants;
import com.landagmail.josemaria.humansearchinc.Logging.L;
import com.landagmail.josemaria.humansearchinc.MainApplication;
import com.landagmail.josemaria.humansearchinc.R;
import com.landagmail.josemaria.humansearchinc.Tasks.POST.POJO.RequestClasses.ProfileRequestClass;
import com.landagmail.josemaria.humansearchinc.Utils.CommonItems;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;

public class UpdateProfileService extends IntentService {

    public UpdateProfileService() {
        super("UpdateProfileService");
    }

    static String userID;
    static String alias;
    static String pictureURL_OR_filePath;
    static String imageUrl;

    @Override
    protected void onHandleIntent(Intent intent) {

        userID = intent.getStringExtra("userID");
        pictureURL_OR_filePath = intent.getStringExtra("pictureURL");
        alias = intent.getStringExtra("alias");
        boolean newPicture = intent.getBooleanExtra("newPicture", true);

        if (newPicture) {
            uploadImage(userID, alias, new File(pictureURL_OR_filePath));
        } else {
            updateProfilejsonObjectRequest(Constants.UPDATE_PROFILE, new Gson().toJson(new ProfileRequestClass(userID, pictureURL_OR_filePath, alias)));
        }
    }

    public void uploadImage(String userID, String alias, File file) {

        CognitoCachingCredentialsProvider credentialsProvider = CredentialsProvider.getCredentials(MainApplication.getAppContext());
        AmazonS3Client s3Client = new AmazonS3Client(credentialsProvider);

        try {
            PutObjectRequest putRequest = new PutObjectRequest("human.search.images", file.getName(), file).withCannedAcl(CannedAccessControlList.PublicRead);
            s3Client.putObject(putRequest);
            imageUrl = "https://s3-us-west-2.amazonaws.com/human.search.images/" + file.getName();
            updateProfilejsonObjectRequest(Constants.UPDATE_PROFILE, new Gson().toJson(new ProfileRequestClass(userID, imageUrl, alias)));

        } catch (AmazonS3Exception e) {
            Log.i("ERROR", e.getErrorMessage());
        }
    }

    public static void updateProfilejsonObjectRequest(final String API_ENDPOINT, String requestString) {

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, API_ENDPOINT, requestString, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                final Intent intent;

                try {
                    switch (Integer.parseInt(response.getString("response"))) {
                        case Constants.PHOTO_UPLOAD_OR_NO_CHANGE:

                            CommonItems.setProfilePictureUrl(imageUrl);

                            intent = new Intent(ProfileActivity.ResponseReceiver.ACTION_RESP);
                            intent.putExtra("UPDATE_INFO", Constants.PHOTO_UPLOAD_OR_NO_CHANGE_STRING);
                            MainApplication.getAppContext().sendBroadcast(intent);

                            break;
                        case Constants.USERNAME_IN_USE:
                            Boolean firstTimeUser = MainApplication.getAppContext().getSharedPreferences("PREFERENCE", MODE_PRIVATE)
                                    .getBoolean("firstTimeUser", true);

                            L.m(MainApplication.getAppContext().getString(R.string.usernameInUse));

                            if (firstTimeUser) {
                                intent = new Intent(MainApplication.getAppContext(), SetupActivity.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                MainApplication.getAppContext().startActivity(intent);
                            } else {
                                intent = new Intent(ProfileActivity.ResponseReceiver.ACTION_RESP);
                                intent.putExtra("UPDATE_INFO", Constants.USERNAME_IN_USE_STRING);
                                MainApplication.getAppContext().sendBroadcast(intent);
                            }

                            break;
                        case Constants.PHOTO_AND_OR_USERNAME_UPDATED:

                            CommonItems.setName(alias);
                            CommonItems.setProfilePictureUrl(imageUrl);

                            intent = new Intent(ProfileActivity.ResponseReceiver.ACTION_RESP);
                            intent.putExtra("UPDATE_INFO", Constants.PHOTO_AND_OR_USERNAME_UPDATED_STRING);
                            MainApplication.getAppContext().sendBroadcast(intent);

                            break;
                        case Constants.NEW_USER_ADDED:

                            CommonItems.setProfilePictureUrl(imageUrl);
                            CommonItems.setName(alias);

                            MainApplication.getAppContext().getSharedPreferences("PREFERENCE", Context.MODE_PRIVATE).edit()
                                    .putBoolean("firstTimeUser", false).apply();
                            break;
                        default:
                            break;
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.getMessage();
            }
        });

        request.setRetryPolicy(new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        MainApplication.getInstance().addToRequestQueue(request);
    }
}
