package com.landagmail.josemaria.humansearchinc.Activities.Friends;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.facebook.Profile;
import com.landagmail.josemaria.humansearchinc.MainApplication;
import com.landagmail.josemaria.humansearchinc.R;
import com.landagmail.josemaria.humansearchinc.Tasks.POST.POJO.ResponseClasses.FriendResponseClass;
import com.landagmail.josemaria.humansearchinc.Tasks.POST.Queries.PostHelperClass;
import com.landagmail.josemaria.humansearchinc.Utils.CustomViews.SearchViewLayout.SearchViewLayout;
import com.landagmail.josemaria.humansearchinc.adapters.Friends.FollowingAdapter;

import java.util.ArrayList;


public class FollowingActivity extends AppCompatActivity {

    private FollowingAdapter adapter;
    private ResponseReceiver receiver;
    private SearchViewLayout searchViewLayout;
    private ColorDrawable collapsed;
    private ArrayList<String> friendIds = new ArrayList<>();

    @Override
    public void onResume() {
        super.onResume();
        MainApplication.activityResumed();
        IntentFilter filter = new IntentFilter(ResponseReceiver.ACTION_RESP);
        filter.addCategory(Intent.CATEGORY_DEFAULT);
        registerReceiver(receiver, filter);
    }

    @Override
    public void onPause() {
        super.onPause();
        MainApplication.activityPaused();
        unregisterReceiver(receiver);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_friends);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(null);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        searchViewLayout = (SearchViewLayout) findViewById(R.id.search_view_container);
        final SearchStaticRecyclerFragment fragment = new SearchStaticRecyclerFragment();
        searchViewLayout.setExpandedContentFragment(this, fragment);
        searchViewLayout.handleToolbarAnimation(toolbar);
        searchViewLayout.setCollapsedHint(getString(R.string.searchFriendsHint));
        searchViewLayout.setExpandedHint(getString(R.string.searchFriendsHint2));

        collapsed = new ColorDrawable(ContextCompat.getColor(this, R.color.primary));
        ColorDrawable expanded = new ColorDrawable(ContextCompat.getColor(this, R.color.default_color_expanded));
        searchViewLayout.setTransitionDrawables(collapsed, expanded);
        searchViewLayout.setSearchListener(new SearchViewLayout.SearchListener() {
            @Override
            public void onFinished(String searchKeyword) {
                Snackbar.make(searchViewLayout, getString(R.string.searchProgress) + searchKeyword + "...", Snackbar.LENGTH_LONG).show();
                fragment.fetchData(searchKeyword.toLowerCase().replaceAll("\\s+", ""), friendIds);
            }
        });
        searchViewLayout.setOnToggleAnimationListener(new SearchViewLayout.OnToggleAnimationListener() {
            @Override
            public void onStart(boolean expanding) {
            }

            @Override
            public void onFinish(boolean expanded) {
                fetchData();
            }
        });

        receiver = new ResponseReceiver();
        RecyclerView friendsRecyclerView = (RecyclerView) findViewById(R.id.friendsRecyclerView);
        friendsRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapter = new FollowingAdapter(this);
        friendsRecyclerView.setAdapter(adapter);
        fetchData();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return true;
    }

    private void setData(ArrayList<FriendResponseClass> data) {
        adapter.setData(data);
    }

    private void fetchData() {
        PostHelperClass.ProfileQueries.fetch_following(Profile.getCurrentProfile().getId());
    }

    public class ResponseReceiver extends BroadcastReceiver {

        public static final String ACTION_RESP = "FRIENDS";

        @Override
        public void onReceive(Context context, Intent intent) {

            ArrayList<FriendResponseClass> response = (ArrayList<FriendResponseClass>) intent.getSerializableExtra("FRIENDS_DATA");
            for (FriendResponseClass friend : response) {
                friendIds.add(friend.getUser_id());
            }
            setData(response);
        }
    }
}
