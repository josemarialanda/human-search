package com.landagmail.josemaria.humansearchinc;

import android.*;
import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.location.Location;
import android.support.multidex.MultiDex;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;

import com.amazonaws.auth.CognitoCachingCredentialsProvider;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.Volley;
import com.crashlytics.android.Crashlytics;
import com.digits.sdk.android.Digits;
import com.facebook.FacebookSdk;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.listener.multi.DialogOnAnyDeniedMultiplePermissionsListener;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.landagmail.josemaria.humansearchinc.AWSUtils.CredentialsProvider;
import com.landagmail.josemaria.humansearchinc.Activities.InitScreens.WelcomeActivity;
import com.landagmail.josemaria.humansearchinc.Activities.InitScreens.SetupActivity;
import com.landagmail.josemaria.humansearchinc.Activities.PermissionActivity;
import com.landagmail.josemaria.humansearchinc.Logging.L;
import com.landagmail.josemaria.humansearchinc.Utils.Storage.SharedPreferences.ComplexPreferences;
import com.twitter.sdk.android.core.TwitterAuthConfig;
import com.twitter.sdk.android.core.TwitterCore;
import com.urbanairship.AirshipConfigOptions;
import com.urbanairship.UAirship;

import io.fabric.sdk.android.Fabric;

import java.lang.reflect.Method;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class MainApplication extends Application {

    private static final String TAG = "ObjectPreference";
    private ComplexPreferences complexPrefenreces = null;

    private RequestQueue mRequestQueue;

    public ComplexPreferences getComplexPreference() {
        if (complexPrefenreces != null) {
            return complexPrefenreces;
        }
        return null;
    }


    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    // Note: Your consumer key and secret should be obfuscated in your source code before shipping.
    private static final String TWITTER_KEY = "HfxHZ2gvk9tYvFTCliRynVhgB";
    private static final String TWITTER_SECRET = "ax07z45DhqAJ2wcxCf1XEiDm2e7tP3ke32mMmhv5WvrtK9Tjhy";
    private static final String URBAN_AIRSHIP_DEVELOPMENT_KEY = "O42Du_QSTOeZHjkU1nP-zA";
    private static final String URBAN_AIRSHIP_DEVELOPMENT_APP_SECRET = "6l2tIMYaSI6DxceDC6epXg";

    private final static String URBAN_AIRSHIP_PRODUCTION_KEY = "O42Du_QSTOeZHjkU1nP-zA";
    private static final String URBAN_AIRSHIP_PRODUCTION_APP_SECRET = "6l2tIMYaSI6DxceDC6epXg";

    private static MainApplication appInstance;
    private static boolean isLocationSet = false;
    public static double latitude;
    public static double longitude;

    public static void setLocation(Location location) {
        latitude = location.getLatitude();
        longitude = location.getLongitude();
    }

    public static boolean isLocationSet() {
        return isLocationSet;
    }

    public static void setIsLocationSet(boolean isLocationSet) {
        MainApplication.isLocationSet = isLocationSet;
    }


    private static CognitoCachingCredentialsProvider credentialsProvider;

    public static CognitoCachingCredentialsProvider getCredentials() {
        if (credentialsProvider == null) {
            credentialsProvider = CredentialsProvider.getCredentials(MainApplication.getAppContext());
        }
        return credentialsProvider;
    }

    public RequestQueue getRequestQueue() {
        // lazy initialize the request queue, the queue instance will be
        // created when it is accessed for the first time
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(getApplicationContext(), new HurlStack());
        }

        return mRequestQueue;
    }

    public <T> void addToRequestQueue(Request<T> req, String tag) {
        // set the default tag if tag is empty
        req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);

        VolleyLog.d("Adding request to queue: %s", req.getUrl());

        getRequestQueue().add(req);
    }

    public <T> void addToRequestQueue(Request<T> req) {
        // set the default tag if tag is empty
        req.setTag(TAG);

        getRequestQueue().add(req);
    }

    public void cancelPendingRequests(Object tag) {
        if (mRequestQueue != null) {
            mRequestQueue.cancelAll(tag);
        }
    }


    @Override
    public void onCreate() {
        super.onCreate();
        TwitterAuthConfig authConfig = new TwitterAuthConfig(TWITTER_KEY, TWITTER_SECRET);
        Fabric.with(this, new Crashlytics(), new TwitterCore(authConfig), new Digits());
        FacebookSdk.sdkInitialize(this);
        appInstance = this;

        printHashKey();

        Dexter.initialize(this);
        getCredentials();
        mRequestQueue = Volley.newRequestQueue(getApplicationContext());
        complexPrefenreces = ComplexPreferences.getComplexPreferences(getBaseContext(), "user_data", MODE_PRIVATE);

        AirshipConfigOptions options = new AirshipConfigOptions.Builder()
                //.setDevelopmentAppKey(URBAN_AIRSHIP_DEVELOPMENT_KEY)
                //.setDevelopmentAppSecret(URBAN_AIRSHIP_DEVELOPMENT_APP_SECRET)
                .setInProduction(true)
                .setProductionAppKey(URBAN_AIRSHIP_PRODUCTION_KEY)
                .setProductionAppSecret(URBAN_AIRSHIP_PRODUCTION_APP_SECRET)
                        //.setInProduction(!BuildConfig.DEBUG)
                .setGcmSender("59240037161")
                .build();
        UAirship.takeOff(this, options);

        Boolean isFirstRun = getSharedPreferences("PREFERENCE", MODE_PRIVATE)
                .getBoolean("isFirstRun", true);

        if (isFirstRun) {
            Intent intent = new Intent(this, WelcomeActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        } else {
            Boolean firstTimeUser = getSharedPreferences("PREFERENCE", MODE_PRIVATE)
                    .getBoolean("firstTimeUser", true);

            if (firstTimeUser) {
                Intent intent = new Intent(this, SetupActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        }
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
    }

    public static MainApplication getInstance() {
        return appInstance;
    }

    public static String getUniqueID() {
        String serial = null;
        try {
            Class<?> c = Class.forName("android.os.SystemProperties");
            Method get = c.getMethod("get", String.class);
            serial = (String) get.invoke(c, "ro.serialno");
        } catch (Exception ignored) {
        }
        return serial;
    }

    public static Context getAppContext() {
        return appInstance.getApplicationContext();
    }

    public void printHashKey() {

        try {
            PackageInfo info = getPackageManager().getPackageInfo(
                    "com.landagmail.josemaria.humansearchinc",
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.i("hashKey", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {
            Log.i("hashKey", e.getLocalizedMessage());

        } catch (NoSuchAlgorithmException e) {
            Log.i("hashKey", e.getLocalizedMessage());
        }
    }

    public static boolean isExternalProfileFetchActivityVisible() {
        return activityVisible;
    }

    public static boolean isFriendsActivityVisible() {
        return activityVisible;
    }

    public static void activityResumed() {
        activityVisible = true;
    }

    public static void activityPaused() {
        activityVisible = false;
    }

    private static boolean activityVisible;

}
