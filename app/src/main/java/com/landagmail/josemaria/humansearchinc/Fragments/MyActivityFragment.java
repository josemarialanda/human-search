package com.landagmail.josemaria.humansearchinc.Fragments;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.provider.Settings;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.facebook.AccessToken;
import com.facebook.Profile;
import com.landagmail.josemaria.humansearchinc.Activities.AskActivity;
import com.landagmail.josemaria.humansearchinc.Animations.TogicLoadingView.TogicLoadingView;
import com.landagmail.josemaria.humansearchinc.Logging.L;
import com.landagmail.josemaria.humansearchinc.R;
import com.landagmail.josemaria.humansearchinc.Tasks.POST.POJO.ResponseClasses.MyQuestionFetchResponseClass;
import com.landagmail.josemaria.humansearchinc.Tasks.POST.Queries.PostHelperClass;
import com.landagmail.josemaria.humansearchinc.Utils.CustomViews.PreCachingLayoutManager;
import com.landagmail.josemaria.humansearchinc.Utils.DeviceUtils;
import com.landagmail.josemaria.humansearchinc.adapters.MyActivityAdapter;

import java.util.ArrayList;

public class MyActivityFragment extends Fragment {

    public MyActivityFragment() {
    }

    private SwipeRefreshLayout swipeRefreshLayout;
    public RecyclerView questionRecyclerView;
    private MyActivityAdapter adapter;
    private ResponseReceiver receiver;
    FloatingActionButton newQuestionBtn;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        receiver = new ResponseReceiver();
    }

    private boolean isOnline() {
        ConnectivityManager cm =
                (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_my_activity, container, false);

        newQuestionBtn = (FloatingActionButton) view.findViewById(R.id.newQuestionBtn);
        newQuestionBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), AskActivity.class));
            }
        });

        questionRecyclerView = (RecyclerView) view.findViewById(R.id.questionRecyclerView);

        PreCachingLayoutManager layoutManager = new PreCachingLayoutManager(getActivity());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        layoutManager.setExtraLayoutSpace(DeviceUtils.getScreenHeight(getActivity()));

        questionRecyclerView.setLayoutManager(layoutManager);
        adapter = new MyActivityAdapter(getActivity(), MyActivityFragment.this);
        questionRecyclerView.setAdapter(adapter);

        swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipeRefreshLayout);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refreshItems();
            }
        });

        return view;
    }

    public void refreshItems() {
        if (isOnline()) {
            fetchQuestion();
            swipeRefreshLayout.setRefreshing(true);
        } else {
            Snackbar.make(getActivity().findViewById(android.R.id.content), getString(R.string.noInternetConnection), Snackbar.LENGTH_LONG)
                    .setAction(getString(R.string.connectToInternet), new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            startActivity(new Intent(Settings.ACTION_WIFI_SETTINGS));
                        }
                    })
                    .setActionTextColor(Color.RED)
                    .show();
        }
    }

    private void fetchQuestion() {

        if (AccessToken.getCurrentAccessToken() != null) {
            PostHelperClass.CartoDBQueries.fetch_my_questions(Profile.getCurrentProfile().getId());
        } else {
            swipeRefreshLayout.setRefreshing(false);
        }
    }

    public void setListData(ArrayList<MyQuestionFetchResponseClass> items) {
        if (items != null) {
            adapter.setData(items);
        } else {
            L.m("List null");
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        IntentFilter filter = new IntentFilter(ResponseReceiver.ACTION_RESP);
        filter.addCategory(Intent.CATEGORY_DEFAULT);
        getActivity().registerReceiver(receiver, filter);
        refreshItems();
    }

    @Override
    public void onPause() {
        super.onPause();
        getActivity().unregisterReceiver(receiver);
    }

    public class ResponseReceiver extends BroadcastReceiver {
        public static final String ACTION_RESP = "QUESTIONS_ASKED";

        @Override
        public void onReceive(Context context, Intent intent) {

            if (intent.getStringExtra("OUT_SEND").equals("questionList")) {
                setListData((ArrayList<MyQuestionFetchResponseClass>) intent.getSerializableExtra("QUESTIONS_LIST"));
                swipeRefreshLayout.setRefreshing(false);
            }
        }
    }
}
