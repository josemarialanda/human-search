package com.landagmail.josemaria.humansearchinc.Tasks.POST.POJO.RequestClasses;

public class UserSettingsRequestClass {
	
	String userID;
	String json;
	
	public UserSettingsRequestClass() {
	
	}
	
	public UserSettingsRequestClass(String userID, String json) {
		this.userID = userID;
		this.json = json;
	}
	
	public String getUserID() {
		return userID;
	}
	public void setUserID(String userID) {
		this.userID = userID;
	}
	public String getJson() {
		return json;
	}
	public void setJson(String json) {
		this.json = json;
	}

}
