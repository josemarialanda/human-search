package com.landagmail.josemaria.humansearchinc.Logging;

import android.widget.Toast;
import com.landagmail.josemaria.humansearchinc.MainApplication;

import java.util.ArrayList;

public class L {

    private L(){

    }

    public static void t(String message){
        Toast.makeText(MainApplication.getAppContext(), message + "", Toast.LENGTH_SHORT).show();
    }
    public static void m(String message){
        Toast.makeText(MainApplication.getAppContext(), message + "", Toast.LENGTH_LONG).show();
    }

    public static void loop(ArrayList<String> list){
        StringBuilder builder = new StringBuilder();
        for (String  b : list) {
            builder.append(b).append("\n");
        }
        Toast.makeText(MainApplication.getAppContext(), builder + "", Toast.LENGTH_LONG).show();
    }
}
