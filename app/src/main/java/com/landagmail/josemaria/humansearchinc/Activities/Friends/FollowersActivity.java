package com.landagmail.josemaria.humansearchinc.Activities.Friends;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.Profile;
import com.landagmail.josemaria.humansearchinc.Logging.L;
import com.landagmail.josemaria.humansearchinc.R;
import com.landagmail.josemaria.humansearchinc.Tasks.POST.POJO.ResponseClasses.FriendResponseClass;
import com.landagmail.josemaria.humansearchinc.Tasks.POST.Queries.PostHelperClass;
import com.landagmail.josemaria.humansearchinc.adapters.Friends.FollowersAdapter;
import com.landagmail.josemaria.humansearchinc.adapters.Friends.FollowingAdapter;

import java.util.ArrayList;

public class FollowersActivity extends AppCompatActivity {

    private FollowersAdapter adapter;
    private ResponseReceiver receiver;

    @Override
    public void onResume() {
        super.onResume();
        IntentFilter filter = new IntentFilter(ResponseReceiver.ACTION_RESP);
        filter.addCategory(Intent.CATEGORY_DEFAULT);
        registerReceiver(receiver, filter);
    }

    @Override
    public void onPause() {
        super.onPause();
        unregisterReceiver(receiver);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_followers);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(null);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        receiver = new ResponseReceiver();

        RecyclerView friendsRecyclerView = (RecyclerView) findViewById(R.id.friendsRecyclerView);
        friendsRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapter = new FollowersAdapter(this);
        friendsRecyclerView.setAdapter(adapter);

        fetchData();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return true;
    }

    private void setData(ArrayList<FriendResponseClass> data) {
        adapter.setData(data);
    }

    private void fetchData() {
        PostHelperClass.ProfileQueries.fetch_followers(Profile.getCurrentProfile().getId());
    }

    public class ResponseReceiver extends BroadcastReceiver {

        public static final String ACTION_RESP = "FRIENDS";

        @Override
        public void onReceive(Context context, Intent intent) {

            ArrayList<FriendResponseClass> response = (ArrayList<FriendResponseClass>) intent.getSerializableExtra("FRIENDS_DATA");
            setData(response);
        }
    }
}
