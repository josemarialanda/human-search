package com.landagmail.josemaria.humansearchinc.Utils.UrbanAirshipUtils;


import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.google.android.gms.maps.model.LatLng;
import com.landagmail.josemaria.humansearchinc.Activities.AnswerScreenActivity;
import com.landagmail.josemaria.humansearchinc.Logging.L;
import com.landagmail.josemaria.humansearchinc.MainActivity;
import com.landagmail.josemaria.humansearchinc.Utils.CommonItems;
import com.urbanairship.UAirship;
import com.urbanairship.push.BaseIntentReceiver;
import com.urbanairship.push.PushMessage;

public class PushIntentReceiver extends BaseIntentReceiver {

    private static final String TAG = "IntentReceiver";

    @Override
    protected void onChannelRegistrationSucceeded(Context context, String channelId) {
        Log.i(TAG, "Channel registration updated. Channel Id:" + channelId + ".");
    }

    @Override
    protected void onChannelRegistrationFailed(Context context) {
        Log.i(TAG, "Channel registration failed.");
    }

    @Override
    protected void onPushReceived(Context context, PushMessage message, int notificationId) {
        Log.i(TAG, "Received push notification. Alert: " + message.getAlert() + ". Notification ID: " + notificationId);
    }

    @Override
    protected void onBackgroundPushReceived(Context context, PushMessage message) {
        Log.i(TAG, "Received background push message: " + message);
    }

    @Override
    protected boolean onNotificationOpened(Context context, PushMessage message, int notificationId) {
        Log.i(TAG, "User clicked notification. Alert: " + message.getAlert());


        CommonItems.openApp(context, "com.landagmail.josemaria.humansearchinc");

        /*
        Bundle pushBundle = message.getPushBundle();

        if (!pushBundle.isEmpty()) {
            String questionID = pushBundle.get("questionID").toString();
            String userID = pushBundle.get("userID").toString();

            String longitude = pushBundle.get("longitude").toString();
            String latitude = pushBundle.get("latitude").toString();

            String name = pushBundle.get("name").toString();
            String question = pushBundle.get("question").toString();
            String profilePicture = pushBundle.get("profilePicture").toString();
            String time = pushBundle.get("date_time").toString();
            String address = pushBundle.get("address").toString();


            Intent intent = new Intent(context, AnswerScreenActivity.class);
            intent.putExtra("questionID", questionID);
            intent.putExtra("userID", userID);
            intent.putExtra("longitude", longitude);
            intent.putExtra("latitude", latitude);
            intent.putExtra("name", name);
            intent.putExtra("question", question);
            intent.putExtra("profilePicture", profilePicture);
            intent.putExtra("date_time", time);
            intent.putExtra("address", address);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(intent);

            return true;
        } else {
            return false;
        }
        */
        /*
        return false;
        */
        return true;
    }

    @Override
    protected boolean onNotificationActionOpened(Context context, PushMessage message, int notificationId, String buttonId, boolean isForeground) {
        Log.i(TAG, "User clicked notification button. Button ID: " + buttonId + " Alert: " + message.getAlert());

        // Return false to let UA handle launching the launch activity
        return false;
    }

    @Override
    protected void onNotificationDismissed(Context context, PushMessage message, int notificationId) {
        Log.i(TAG, "Notification dismissed. Alert: " + message.getAlert() + ". Notification ID: " + notificationId);
    }

}
