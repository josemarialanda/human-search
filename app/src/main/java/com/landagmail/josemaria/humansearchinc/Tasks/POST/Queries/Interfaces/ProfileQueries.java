package com.landagmail.josemaria.humansearchinc.Tasks.POST.Queries.Interfaces;

import com.landagmail.josemaria.humansearchinc.Tasks.POST.POJO.RequestClasses.ProfileRequestClass;
import com.landagmail.josemaria.humansearchinc.Tasks.POST.POJO.RequestClasses.UserSettingsRequestClass;
import com.landagmail.josemaria.humansearchinc.Tasks.POST.POJO.ResponseClasses.FollowResponseClass;
import com.landagmail.josemaria.humansearchinc.Tasks.POST.POJO.ResponseClasses.FriendResponseClass;
import com.landagmail.josemaria.humansearchinc.Tasks.POST.POJO.ResponseClasses.ProfileResponseClass;

import java.util.ArrayList;

public interface ProfileQueries {

    Integer update_user_settings(UserSettingsRequestClass Input);

    String profile_check(String Input);

    ProfileResponseClass fetch_profile(String Input);

    FollowResponseClass profile_follow_fetch(String Input);

    ArrayList<FriendResponseClass> fetch_following(String Input);

    ArrayList<FriendResponseClass> fetch_followers(String Input);

    void fetch_external_profile(String alias);

}
