package com.landagmail.josemaria.humansearchinc.Tasks.POST.POJO.RequestClasses;

public class FetchRequestClass {
	
	double latitude;
    double longitude;
    String userID;
    
	public FetchRequestClass() {
	
	}

	public FetchRequestClass(double latitude, double longitude, String userID) {
		this.latitude = latitude;
		this.longitude = longitude;
		this.userID = userID;
	}
	
	public double getLatitude() {
		return latitude;
	}
	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}
	public double getLongitude() {
		return longitude;
	}
	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}
	public String getUserID() {
		return userID;
	}
	public void setUserID(String userID) {
		this.userID = userID;
	}

}
