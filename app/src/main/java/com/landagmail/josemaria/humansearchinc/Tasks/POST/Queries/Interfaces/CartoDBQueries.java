package com.landagmail.josemaria.humansearchinc.Tasks.POST.Queries.Interfaces;


import com.landagmail.josemaria.humansearchinc.Tasks.POST.POJO.RequestClasses.AnswerFetchRequestClass;
import com.landagmail.josemaria.humansearchinc.Tasks.POST.POJO.RequestClasses.DeleteRequestClass;
import com.landagmail.josemaria.humansearchinc.Tasks.POST.POJO.RequestClasses.FetchRequestClass;
import com.landagmail.josemaria.humansearchinc.Tasks.POST.POJO.RequestClasses.PushRequestClass;
import com.landagmail.josemaria.humansearchinc.Tasks.POST.POJO.ResponseClasses.FetchResponseClass;
import com.landagmail.josemaria.humansearchinc.Tasks.POST.POJO.ResponseClasses.MyQuestionFetchResponseClass;

import org.json.JSONException;

import java.util.ArrayList;

public interface CartoDBQueries {

    ArrayList<FetchResponseClass> fetch_answers(AnswerFetchRequestClass Input) throws JSONException;

    Integer insert_answers(PushRequestClass Input) throws JSONException;

    Integer delete_questions(DeleteRequestClass Input) throws JSONException;

    Integer dissociate_question(DeleteRequestClass Input) throws JSONException;

    ArrayList<FetchResponseClass> fetch_questions(FetchRequestClass Input) throws JSONException;

    ArrayList<MyQuestionFetchResponseClass> fetch_my_questions(String Input) throws JSONException;

    Integer insert_questions(PushRequestClass Input) throws JSONException;

    void fetch_all_questions();

    void fetch_hashtag_questions(String hashTag);

    void check_for_answers(String input);

    void check_for_questions(FetchRequestClass Input) throws JSONException;

}
