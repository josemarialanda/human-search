package com.landagmail.josemaria.humansearchinc.Services;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import com.facebook.Profile;
import com.google.android.gms.gcm.GcmTaskService;
import com.google.android.gms.gcm.TaskParams;
import com.landagmail.josemaria.humansearchinc.Activities.InitScreens.WelcomeActivity;
import com.landagmail.josemaria.humansearchinc.Tasks.POST.Queries.PostHelperClass;

public class NewAnswersMonitorService extends GcmTaskService {

    public NewAnswersMonitorService() {
        super();
    }

    @Override
    public int onRunTask(TaskParams taskParams) {
        Log.i("MonitorService", "onRunTask");

        Boolean isFirstRun = getSharedPreferences("PREFERENCE", MODE_PRIVATE)
                .getBoolean("isFirstRun", true);
        if (!isFirstRun) {
            Log.i("AnswerMonitorService", "Running service");
            if (isOnline()) {
                PostHelperClass.CartoDBQueries.check_for_answers(Profile.getCurrentProfile().getId());
            }
        }
        return 0;
    }

    private boolean isOnline() {
        ConnectivityManager cm =
                (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }
}