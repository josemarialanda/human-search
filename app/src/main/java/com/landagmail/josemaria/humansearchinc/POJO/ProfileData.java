package com.landagmail.josemaria.humansearchinc.POJO;

import java.io.Serializable;

public class ProfileData implements Serializable {

    String alias;
    boolean enableNotifications = false;

    public ProfileData(String alias, boolean enableNotifications) {
        this.alias = alias;
        this.enableNotifications = enableNotifications;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public boolean isEnableNotifications() {
        return enableNotifications;
    }

    public void setEnableNotifications(boolean enableNotifications) {
        this.enableNotifications = enableNotifications;
    }
}
