package com.landagmail.josemaria.humansearchinc.Utils.HashTagHelper.TagFetch;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class GetTags {

    public static List<String> getHashTags(String text) {
        ArrayList<String> hashTags = new ArrayList<>();
        String regexPattern = "(#\\w+)";

        Pattern p = Pattern.compile(regexPattern);
        Matcher m = p.matcher(text);

        while (m.find()) {
            String hashtag = m.group(1).substring(1);
            hashTags.add(hashtag);
        }

        return hashTags;
    }

    public static List<String> getCallouts(String text) {
        ArrayList<String> callouts = new ArrayList<>();
        String regexPattern = "(@\\w+)";

        Pattern p = Pattern.compile(regexPattern);
        Matcher m = p.matcher(text);
        while (m.find()) {
            String callout = m.group(1).substring(1);
            callouts.add(callout);
        }
        return callouts;
    }

}
