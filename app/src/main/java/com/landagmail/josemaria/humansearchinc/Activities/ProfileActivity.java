package com.landagmail.josemaria.humansearchinc.Activities;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookSdk;
import com.facebook.Profile;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.LikeView;
import com.facebook.share.widget.ShareDialog;
import com.google.gson.Gson;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.karumi.dexter.listener.single.PermissionListener;
import com.landagmail.josemaria.humansearchinc.Activities.Friends.FollowersActivity;
import com.landagmail.josemaria.humansearchinc.Activities.Friends.FollowingActivity;
import com.landagmail.josemaria.humansearchinc.Camera.CameraActivity;
import com.landagmail.josemaria.humansearchinc.Camera.ImageUtility;
import com.landagmail.josemaria.humansearchinc.Constants;
import com.landagmail.josemaria.humansearchinc.Dialogs.TimePickerDialog;
import com.landagmail.josemaria.humansearchinc.Logging.L;
import com.landagmail.josemaria.humansearchinc.MainApplication;
import com.landagmail.josemaria.humansearchinc.POJO.ProfileData;
import com.landagmail.josemaria.humansearchinc.R;
import com.landagmail.josemaria.humansearchinc.Services.UpdateProfileService;
import com.landagmail.josemaria.humansearchinc.Tasks.POST.POJO.ResponseClasses.FollowResponseClass;
import com.landagmail.josemaria.humansearchinc.Tasks.POST.POJO.ResponseClasses.ProfileResponseClass;
import com.landagmail.josemaria.humansearchinc.Tasks.POST.POJO.UserSettings;
import com.landagmail.josemaria.humansearchinc.Tasks.POST.Queries.PostHelperClass;
import com.landagmail.josemaria.humansearchinc.Tasks.POST.Queries.Utils;
import com.landagmail.josemaria.humansearchinc.Utils.CommonItems;
import com.landagmail.josemaria.humansearchinc.Utils.CustomViews.RoundImageView.RoundedImageView;
import com.landagmail.josemaria.humansearchinc.Utils.Storage.SharedPreferences.ComplexPreferences;
import com.landagmail.josemaria.humansearchinc.Utils.UrbanAirshipUtils.PushUtils;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;
import com.urbanairship.UAirship;

import net.yazeed44.imagepicker.model.ImageEntry;
import net.yazeed44.imagepicker.util.Picker;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ProfileActivity extends AppCompatActivity implements Picker.PickListener {

    Bitmap bitmap = null;

    @Override
    public void onPickedSuccessfully(final ArrayList<ImageEntry> images) {
        bitmap = ImageUtility.cropPicture(this, decodeFile(images.get(0).path));
        profilePicture.setImageBitmap(bitmap);
    }

    @Override
    public void onCancel() {

    }

    private void pickImage() {

        new Picker.Builder(this, this)
                .setPickMode(Picker.PickMode.SINGLE_IMAGE)
                .setAlbumNameTextColor(Color.WHITE)
                .setAlbumImagesCountTextColor(Color.WHITE)
                .setDoneFabIconTintColor(Color.WHITE)
                .disableCaptureImageFromCamera()
                .build()
                .startActivity();

    }

    RoundedImageView profilePicture;
    EditText aliasTxt;
    Button editProfileBtn;
    TextView followingNumTxt;
    TextView followingYouNumTxt;
    Switch enableNotificationsSwitch;
    Button cancelBtn;
    ProfileData profileData;
    ProfileResponseClass profileResponseData;
    String alias;
    boolean inEditMode = false;
    private ResponseReceiver receiver;
    private boolean enableNotifications;
    ProgressDialog dialog;
    private boolean NO_IMAGE = false;
    private Intent intentService;

    @Override
    public void onResume() {
        super.onResume();
        IntentFilter filter = new IntentFilter(ResponseReceiver.ACTION_RESP);
        filter.addCategory(Intent.CATEGORY_DEFAULT);
        registerReceiver(receiver, filter);
        PostHelperClass.ProfileQueries.profile_follow_fetch(com.facebook.Profile.getCurrentProfile().getId());
    }

    @Override
    public void onPause() {
        super.onPause();
        unregisterReceiver(receiver);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return true;
    }

    private final static String NAME_PREFERENCE = "NAME_PREFERENCE";
    private final static String USERNAME = "USERNAME";

    LikeView likeView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(null);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        likeView = (LikeView) findViewById(R.id.like_view);
        likeView.setObjectIdAndType(
                "https://www.facebook.com/loklapp/",
                LikeView.ObjectType.PAGE);

        receiver = new ResponseReceiver();

        dialog = new ProgressDialog(this);
        dialog.setIndeterminate(true);
        dialog.setCancelable(false);
        dialog.setMessage(getString(R.string.loading));

        profilePicture = (RoundedImageView) findViewById(R.id.profilePicture);
        aliasTxt = (EditText) findViewById(R.id.aliasTxt);
        editProfileBtn = (Button) findViewById(R.id.editProfileBtn);
        followingNumTxt = (TextView) findViewById(R.id.followingNumTxt);
        followingYouNumTxt = (TextView) findViewById(R.id.followingYouNumTxt);
        enableNotificationsSwitch = (Switch) findViewById(R.id.enableNotificationsSwitch);

        aliasTxt.setText(getSharedPreferences(NAME_PREFERENCE, MODE_PRIVATE).getString(USERNAME, "Username"));

        followingNumTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ProfileActivity.this, FollowingActivity.class);
                startActivity(intent);
            }
        });

        followingYouNumTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ProfileActivity.this, FollowersActivity.class);
                startActivity(intent);
            }
        });

        cancelBtn = (Button) findViewById(R.id.cancelBtn);
        profilePicture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PopupMenu popup = new PopupMenu(ProfileActivity.this, v);
                MenuInflater inflater = popup.getMenuInflater();
                inflater.inflate(R.menu.menu_profile_picture, popup.getMenu());
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        int id = item.getItemId();
                        if (id == R.id.action_take_picture) {
                            requestForCameraPermission();
                            return true;
                        }
                        if (id == R.id.action_choose_image) {

                            Dexter.checkPermission(new PermissionListener() {
                                @Override
                                public void onPermissionGranted(PermissionGrantedResponse response) {
                                    pickImage();
                                }

                                @Override
                                public void onPermissionDenied(PermissionDeniedResponse response) {
                                    L.m(getString(R.string.storagePermissionDenied));
                                }

                                @Override
                                public void onPermissionRationaleShouldBeShown(PermissionRequest permission, PermissionToken token) {
                                    token.continuePermissionRequest();
                                }
                            }, Manifest.permission.READ_EXTERNAL_STORAGE);
                            return true;
                        }
                        if (id == R.id.action_remove_picture) {
                            NO_IMAGE = true;
                            profilePicture.setImageDrawable(getResources().getDrawable(R.drawable.profile_image));
                            return true;
                        }
                        return false;
                    }
                });
                popup.show();

            }
        });

        aliasTxt.setClickable(false);
        aliasTxt.setEnabled(false);

        enableNotificationsSwitch.setClickable(false);
        profilePicture.setClickable(false);

        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                filePath = "";
                bitmap = null;
                inEditMode = false;
                NO_IMAGE = false;
                cancelBtn.setVisibility(View.GONE);
                editProfileBtn.setText(R.string.editProfile);
                aliasTxt.setClickable(false);
                aliasTxt.setEnabled(false);
                enableNotificationsSwitch.setClickable(false);
                enableNotificationsSwitch.setChecked(profileData.isEnableNotifications());
                profilePicture.setClickable(false);
                loadPicture(profileResponseData.getProfile_picture());
                aliasTxt.setText(profileResponseData.getAlias());
            }
        });

        editProfileBtn.setEnabled(false);
        editProfileBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (inEditMode) {

                    showAlertDialog();

                } else {
                    inEditMode = true;
                    cancelBtn.setVisibility(View.VISIBLE);
                    editProfileBtn.setText(R.string.makeChangesToProfile);
                    aliasTxt.setClickable(true);
                    aliasTxt.setEnabled(true);
                    enableNotificationsSwitch.setClickable(true);
                    profilePicture.setClickable(true);
                }
            }
        });
        PostHelperClass.ProfileQueries.fetch_profile(com.facebook.Profile.getCurrentProfile().getId());
    }

    private void showAlertDialog() {
        final AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        alertDialog.setMessage(getString(R.string.makeChangesToProfileConfirmation));
        alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, getString(R.string.confirm), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        if (profileResponseData.getAlias().equals(aliasTxt.getText().toString()) && getFilePath().equals("") && !NO_IMAGE && bitmap == null) {

                        } else {
                            alias = aliasTxt.getText().toString();
                            if (alias.length() > 3) {

                                Pattern p = Pattern.compile("[^a-z0-9]");
                                Matcher m = p.matcher(alias);
                                boolean b = m.find();

                                if (b) {
                                    L.m(getString(R.string.setNameError));
                                } else {

                                    if (bitmap != null) {
                                        filePath = ImageUtility.saveAlreadyCroppedPicture(ProfileActivity.this, bitmap);
                                    }

                                    if (!getFilePath().equals("")) {

                                        intentService = new Intent(ProfileActivity.this, UpdateProfileService.class);
                                        intentService.putExtra("userID", Profile.getCurrentProfile().getId());
                                        intentService.putExtra("pictureURL", getFilePath());
                                        intentService.putExtra("alias", alias);
                                        intentService.putExtra("newPicture", true);
                                        startService(intentService);
                                        ProfileActivity.this.dialog.show();

                                    } else {

                                        intentService = new Intent(ProfileActivity.this, UpdateProfileService.class);
                                        intentService.putExtra("userID", Profile.getCurrentProfile().getId());
                                        if (NO_IMAGE) {
                                            intentService.putExtra("pictureURL", Constants.NO_IMAGE);
                                        } else {
                                            intentService.putExtra("pictureURL", profileResponseData.getProfile_picture());
                                        }
                                        intentService.putExtra("alias", alias);
                                        intentService.putExtra("newPicture", false);
                                        startService(intentService);
                                        ProfileActivity.this.dialog.show();

                                    }
                                }
                            } else {
                                L.m(getString(R.string.setNameError2));
                            }

                        }
                        updateUserSettings();
                    }
                }

        );
        alertDialog.setButton(DialogInterface.BUTTON_NEGATIVE, getString(R.string.cancel), new DialogInterface.OnClickListener()

                {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // DO NOTHING
                    }
                }
        );
        alertDialog.show();
    }

    private void updateUserSettings() {

        if (enableNotificationsSwitch.isChecked() == profileData.isEnableNotifications()) {
        } else {
            PostHelperClass.ProfileQueries.update_user_settings(com.facebook.Profile.getCurrentProfile().getId(), new Gson().toJson(getUserSettings()));
            PushUtils.userPushNotifications(enableNotificationsSwitch.isChecked());
            dialog.show();
        }
    }

    private UserSettings getUserSettings() {
        enableNotifications = enableNotificationsSwitch.isChecked();
        return new UserSettings(enableNotifications);
    }

    private static final int REQUEST_CAMERA = 0;
    public static final int CAMERA_REQUEST_CODE = 2;
    private String filePath = "";
    Uri imageUri;

    public void requestForCameraPermission() {
        Dexter.checkPermissions(new MultiplePermissionsListener() {
            @Override
            public void onPermissionsChecked(MultiplePermissionsReport report) {

                if (report.areAllPermissionsGranted()) {
                    Intent startCustomCameraIntent = new Intent(ProfileActivity.this, CameraActivity.class);
                    startActivityForResult(startCustomCameraIntent, REQUEST_CAMERA);
                }
            }

            @Override
            public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                token.continuePermissionRequest();
            }
        }, Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode != RESULT_OK) return;

        if (requestCode == REQUEST_CAMERA) {
            imageUri = data.getData();

            filePath = getRealPathFromURI(String.valueOf(imageUri));
            profilePicture.setImageBitmap(decodeFile(filePath));

        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private String getRealPathFromURI(String contentURI) {
        Uri contentUri = Uri.parse(contentURI);
        Cursor cursor = getContentResolver().query(contentUri, null, null, null, null);
        if (cursor == null) {
            return contentUri.getPath();
        } else {
            cursor.moveToFirst();
            int index = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            return cursor.getString(index);
        }
    }

    public Bitmap decodeFile(String path) {
        try {
            // Decode image size
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            BitmapFactory.decodeFile(path, o);
            // The new size we want to scale to
            final int REQUIRED_SIZE = 500;

            // Find the correct scale value. It should be the power of 2.
            int scale = 1;
            while (o.outWidth / scale / 2 >= REQUIRED_SIZE && o.outHeight / scale / 2 >= REQUIRED_SIZE)
                scale *= 2;

            // Decode with inSampleSize
            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize = scale;
            return BitmapFactory.decodeFile(path, o2);
        } catch (Throwable e) {
            e.printStackTrace();
        }
        return null;
    }

    public String getFilePath() {
        return filePath;
    }

    public void logout(View view) {
        CommonItems.logout(this);
    }

    public void info(View view) {
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://lokl.static.website.s3-website-us-east-1.amazonaws.com/"));
        startActivity(browserIntent);
    }

    public class ResponseReceiver extends BroadcastReceiver {

        public static final String ACTION_RESP = "FOLLOW_DATA";

        @Override
        public void onReceive(Context context, Intent intent) {

            if (intent.getStringExtra("UPDATE_INFO") != null) {

                switch (intent.getStringExtra("UPDATE_INFO")) {
                    case Constants.PHOTO_UPLOAD_OR_NO_CHANGE_STRING:

                        filePath = "";
                        dialog.dismiss();

                        inEditMode = false;
                        cancelBtn.setVisibility(View.GONE);
                        editProfileBtn.setText(getString(R.string.editProfile));
                        aliasTxt.setClickable(false);
                        aliasTxt.setEnabled(false);
                        enableNotificationsSwitch.setClickable(false);
                        profilePicture.setClickable(false);
                        break;
                    case Constants.USERNAME_IN_USE_STRING:
                        L.m(getString(R.string.usernameInUse));
                        dialog.dismiss();
                        break;
                    case Constants.PHOTO_AND_OR_USERNAME_UPDATED_STRING:

                        filePath = "";
                        dialog.dismiss();

                        inEditMode = false;
                        cancelBtn.setVisibility(View.GONE);
                        editProfileBtn.setText(getString(R.string.editProfile));
                        aliasTxt.setClickable(false);
                        aliasTxt.setEnabled(false);
                        enableNotificationsSwitch.setClickable(false);
                        profilePicture.setClickable(false);
                        break;
                    case Constants.USER_SETTINGS_UPDATED_STRING:

                        dialog.dismiss();

                        inEditMode = false;
                        cancelBtn.setVisibility(View.GONE);
                        editProfileBtn.setText(getString(R.string.editProfile));
                        aliasTxt.setClickable(false);
                        aliasTxt.setEnabled(false);
                        enableNotificationsSwitch.setClickable(false);
                        profilePicture.setClickable(false);
                        break;
                }
            }

            if (intent.getSerializableExtra("PROFILE_RESPONSE_DATA") != null) {
                setData((ProfileResponseClass) intent.getSerializableExtra("PROFILE_RESPONSE_DATA"));
            }

            if (intent.getSerializableExtra(ACTION_RESP) != null) {
                FollowResponseClass response = (FollowResponseClass) intent.getSerializableExtra(ACTION_RESP);
                followingNumTxt.setText(response.getFollowing() + getString(R.string.personas));
                followingYouNumTxt.setText(response.getFollowed_by() + getString(R.string.personas));
            }
        }
    }

    public void setData(ProfileResponseClass response) {

        profileResponseData = response;

        if (response != null) {

            editProfileBtn.setEnabled(true);
            aliasTxt.setText(response.getAlias());

            if (response.getJson() != null) {

                JSONObject object;

                try {
                    object = new JSONObject(response.getJson());
                    enableNotifications = object.getBoolean("enableNotifications");
                    enableNotificationsSwitch.setChecked(enableNotifications);

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

            if (response.getProfile_picture().equals("NO_IMAGE")) {
                // DO NOTHING
            } else {
                loadPicture(response.getProfile_picture());
            }

            profileData = new ProfileData(
                    alias,
                    enableNotifications);

        }
    }

    public void loadPicture(String profilePictureString) {
        Picasso.with(MainApplication.getAppContext())
                .load(profilePictureString)
                .into(new Target() {
                    @Override
                    public void onBitmapLoaded(final Bitmap loadedImage, Picasso.LoadedFrom from) {
                        profilePicture.setImageBitmap(loadedImage);
                    }

                    @Override
                    public void onBitmapFailed(Drawable errorDrawable) {

                    }

                    @Override
                    public void onPrepareLoad(Drawable placeHolderDrawable) {

                    }
                });
    }

}
