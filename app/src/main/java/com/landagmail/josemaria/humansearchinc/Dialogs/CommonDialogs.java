package com.landagmail.josemaria.humansearchinc.Dialogs;


import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.widget.AdapterView;

import com.landagmail.josemaria.humansearchinc.R;
import com.landagmail.josemaria.humansearchinc.Utils.CustomViews.materialdialogsearchview.SearchAdapter;
import com.landagmail.josemaria.humansearchinc.Utils.CustomViews.materialdialogsearchview.SharedPreference;
import com.landagmail.josemaria.humansearchinc.Utils.CustomViews.materialdialogsearchview.Utils;

import java.util.ArrayList;

public class CommonDialogs {

    public static void delete_question(final Context context, final int position, final AdapterView<?> adapterView, final SearchAdapter searchAdapter) {
        AlertDialog alertDialog = new AlertDialog.Builder(context).create();
        alertDialog.setTitle(context.getString(R.string.remove));
        alertDialog.setIcon(R.drawable.ic_clear);
        alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, context.getString(R.string.remove), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String tag = String.valueOf(adapterView.getItemAtPosition(position));
                SharedPreference.removeList(context, Utils.PREFS_NAME, Utils.KEY_SEARCHES, tag);

                ArrayList<String> tagsStored = SharedPreference.loadList(context, Utils.PREFS_NAME, Utils.KEY_SEARCHES);
                searchAdapter.updateList(tagsStored, false);
            }
        });
        alertDialog.setButton(DialogInterface.BUTTON_NEGATIVE, context.getString(R.string.cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // DO NOTHING
            }
        });
        alertDialog.show();
    }
}
