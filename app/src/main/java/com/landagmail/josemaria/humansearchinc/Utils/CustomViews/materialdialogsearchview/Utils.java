package com.landagmail.josemaria.humansearchinc.Utils.CustomViews.materialdialogsearchview;

import android.content.Context;
import android.net.ConnectivityManager;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import android.widget.ListView;

public class Utils {
    //https://github.com/TakeoffAndroid/MaterialDialogSearchView

    public static final String PREFS_NAME = "TAKEOFFANDROID";
    public static final String KEY_SEARCHES = "KEY_SEARCHES";
    public static final String KEY_SEARCHES_ASK_ACTIVITY = "KEY_SEARCHES_ASK_ACTIVITY";

    public static final String KEY_SEARCHES_USERS = "KEY_SEARCHES_USERS";
    public static final String KEY_SEARCHES_TAGS = "KEY_SEARCHES_TAGS";
    public static final String KEY_SEARCHES_LOCATION = "KEY_SEARCHES_LOCATION";

    public static final int REQUEST_CODE = 1234;

    public static void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null) {
            // pre-condition
            return;
        }

        int totalHeight = 0;
        for (int i = 0; i < listAdapter.getCount(); i++) {
            View listItem = listAdapter.getView(i, null, listView);
            listItem.measure(0, 0);
            totalHeight += listItem.getMeasuredHeight();
        }

        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
        listView.requestLayout();
    }


    public static boolean isConnectedNetwork (Context context) {

        ConnectivityManager cm = (ConnectivityManager) context.getSystemService (Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo () != null && cm.getActiveNetworkInfo ().isConnectedOrConnecting ();

    }
}