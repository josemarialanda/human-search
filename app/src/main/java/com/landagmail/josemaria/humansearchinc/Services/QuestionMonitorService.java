package com.landagmail.josemaria.humansearchinc.Services;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;

import com.facebook.Profile;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.gcm.GcmTaskService;
import com.google.android.gms.gcm.TaskParams;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.single.PermissionListener;
import com.landagmail.josemaria.humansearchinc.Logging.L;
import com.landagmail.josemaria.humansearchinc.Tasks.POST.Queries.PostHelperClass;

public class QuestionMonitorService extends GcmTaskService implements LocationListener, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;

    private boolean isOnline() {
        ConnectivityManager cm =
                (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }

    @Override
    public void onConnected(Bundle bundle) {
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onLocationChanged(Location location) {
        PostHelperClass.CartoDBQueries.check_for_questions(Profile.getCurrentProfile().getId(), location.getLatitude(), location.getLongitude());
        if (mGoogleApiClient.isConnected()) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
        }
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    public QuestionMonitorService() {
        super();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.i("QuestionMonitorService", "onCreate");
        mLocationRequest = LocationRequest.create();
        mGoogleApiClient = new GoogleApiClient.Builder(this).addApi(LocationServices.API).addConnectionCallbacks(this).addOnConnectionFailedListener(this).build();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.i("QuestionMonitorService", "onStartCommand");
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public IBinder onBind(Intent intent) {
        Log.i("QuestionMonitorService", "onBind");
        return super.onBind(intent);
    }

    @Override
    public void onInitializeTasks() {
        super.onInitializeTasks();
        Log.i("QuestionMonitorService", "onInitializeTasks");
    }

    @Override
    public int onRunTask(TaskParams taskParams) {
        Log.i("MonitorServiceTest", "onRunTask");

        Boolean isFirstRun = getSharedPreferences("PREFERENCE", MODE_PRIVATE)
                .getBoolean("isFirstRun", true);
        if (!isFirstRun) {
            Log.i("MonitorService", "Running service");
            if (isOnline()) {
                Dexter.checkPermission(new PermissionListener() {
                    @Override
                    public void onPermissionGranted(PermissionGrantedResponse response) {
                        mGoogleApiClient.connect();
                    }

                    @Override
                    public void onPermissionDenied(PermissionDeniedResponse response) {
                        L.m("Location permission denied\nGo to settings and allow Lokl to access location in order to use app properly");
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(PermissionRequest permission, PermissionToken token) {
                        L.m("Permission: " + permission.getName() + " previously denied");
                        token.continuePermissionRequest();
                    }
                }, Manifest.permission.ACCESS_FINE_LOCATION);
            }
        }
        return 0;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.i("QuestionMonitorService", "onDestroy");
        if (mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
    }
}