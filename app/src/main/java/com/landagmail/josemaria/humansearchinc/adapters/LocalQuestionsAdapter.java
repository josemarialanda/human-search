package com.landagmail.josemaria.humansearchinc.adapters;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.facebook.Profile;
import com.landagmail.josemaria.humansearchinc.Activities.AnswerScreenActivity;
import com.landagmail.josemaria.humansearchinc.Activities.ExternalProfileFetch;
import com.landagmail.josemaria.humansearchinc.Activities.ZoomInActivity;
import com.landagmail.josemaria.humansearchinc.Constants;

import com.landagmail.josemaria.humansearchinc.R;
import com.landagmail.josemaria.humansearchinc.Tasks.POST.POJO.ResponseClasses.FetchResponseClass;
import com.landagmail.josemaria.humansearchinc.Tasks.POST.Queries.PostHelperClass;
import com.landagmail.josemaria.humansearchinc.Utils.CustomViews.RoundImageView.RoundedImageView;
import com.landagmail.josemaria.humansearchinc.Utils.HashTagHelper.HashTagHelper;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class LocalQuestionsAdapter extends RecyclerView.Adapter<LocalQuestionsAdapter.myViewHolder> {
    // FIX ME
    private LayoutInflater layoutInflater;
    Context context;
    private ArrayList<FetchResponseClass> questionArrayList = new ArrayList<>();

    public LocalQuestionsAdapter(FragmentActivity activity) {
        layoutInflater = LayoutInflater.from(activity);
        context = activity;
    }

    public void setData(ArrayList<FetchResponseClass> list) {
        questionArrayList = list;
        notifyDataSetChanged();
    }

    @Override
    public myViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.question_feed_list_layout, parent, false);
        return new myViewHolder(view);
    }

    @Override
    public int getItemCount() {
        return questionArrayList.size();
    }

    private void showAlertDialog(final int position) {
        AlertDialog alertDialog = new AlertDialog.Builder(context).create();
        alertDialog.setMessage(context.getString(R.string.follow) + questionArrayList.get(position).getName());
        alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, context.getString(R.string.follow) + questionArrayList.get(position).getName(), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                String friendUserId = questionArrayList.get(position).getUserID();
                String name = questionArrayList.get(position).getName();
                String userId = Profile.getCurrentProfile().getId();

                PostHelperClass.FriendsQueries.follow(friendUserId, userId);

            }
        });
        alertDialog.setButton(DialogInterface.BUTTON_NEGATIVE, context.getString(R.string.cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // DO NOTHING
            }
        });
        alertDialog.show();
    }

    @Override
    public void onBindViewHolder(final myViewHolder holder, final int position) {

        holder.card_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, AnswerScreenActivity.class);
                intent.putExtra("questionID", questionArrayList.get(position).getQuestionID());
                intent.putExtra("userID", questionArrayList.get(position).getUserID());
                intent.putExtra("longitude", questionArrayList.get(position).getLongitude());
                intent.putExtra("questionImage", questionArrayList.get(position).getImageURL());
                intent.putExtra("latitude", questionArrayList.get(position).getLatitude());
                intent.putExtra("name", questionArrayList.get(position).getName());
                intent.putExtra("question", questionArrayList.get(position).getText());
                intent.putExtra("profilePicture", questionArrayList.get(position).getProfilePicture());
                intent.putExtra("date_time", questionArrayList.get(position).getDate_time());
                intent.putExtra("address", questionArrayList.get(position).getLocation());
                context.startActivity(intent);
            }
        });

        HashTagHelper mTextHashTagHelper = HashTagHelper.Creator.create(context.getResources().getColor(R.color.primary), new HashTagHelper.OnHashTagClickListener() {
            @Override
            public void onHashTagClicked(String hashTag) {
                Intent intent = new Intent(context, ExternalProfileFetch.class);
                intent.putExtra("ALIAS", hashTag);
                context.startActivity(intent);
            }
        });
        mTextHashTagHelper.handle(holder.questionTxt);

        holder.profilePicture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (questionArrayList.get(position).getUserID().equals(Profile.getCurrentProfile().getId())) {
                    // DO NOTHING
                } else {
                    Intent intent = new Intent(context, ExternalProfileFetch.class);
                    intent.putExtra("ALIAS", questionArrayList.get(position).getName());
                    context.startActivity(intent);
                }
            }
        });
        holder.name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (questionArrayList.get(position).getUserID().equals(Profile.getCurrentProfile().getId())) {
                    // DO NOTHING
                } else {
                    Intent intent = new Intent(context, ExternalProfileFetch.class);
                    intent.putExtra("ALIAS", questionArrayList.get(position).getName());
                    context.startActivity(intent);
                }
            }
        });

        if (!questionArrayList.get(position).getProfilePicture().equals(Constants.NO_IMAGE)) {
            Picasso.with(context)
                    .load(questionArrayList.get(position).getProfilePicture())
                    .fit()
                    .centerCrop()
                    .into(holder.profilePicture);
        } else if (questionArrayList.get(position).getProfilePicture().equals(Constants.NO_IMAGE)) {
            holder.profilePicture.setImageDrawable(context.getResources().getDrawable(R.drawable.profile_image));
        }

        holder.totalAnswersBtn.setText(questionArrayList.get(position).getAnswer_count());

        holder.questionImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, ZoomInActivity.class);
                intent.putExtra("IMAGE_URL", questionArrayList.get(position).getImageURL());
                context.startActivity(intent);
            }
        });

        holder.totalAnswersBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, AnswerScreenActivity.class);
                intent.putExtra("questionID", questionArrayList.get(position).getQuestionID());
                intent.putExtra("userID", questionArrayList.get(position).getUserID());
                intent.putExtra("longitude", questionArrayList.get(position).getLongitude());
                intent.putExtra("questionImage", questionArrayList.get(position).getImageURL());
                intent.putExtra("latitude", questionArrayList.get(position).getLatitude());
                intent.putExtra("name", questionArrayList.get(position).getName());
                intent.putExtra("question", questionArrayList.get(position).getText());
                intent.putExtra("profilePicture", questionArrayList.get(position).getProfilePicture());
                intent.putExtra("date_time", questionArrayList.get(position).getDate_time());
                intent.putExtra("address", questionArrayList.get(position).getLocation());
                context.startActivity(intent);
            }
        });
        holder.name.setText(questionArrayList.get(position).getName());
        holder.name.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                if (questionArrayList.get(position).getUserID().equals(Profile.getCurrentProfile().getId())) {
                    // DO NOTHING
                } else {
                    showAlertDialog(position);
                }
                return false;

            }
        });
        holder.questionTxt.setText(questionArrayList.get(position).getText());

        holder.questionDistance.setText(questionArrayList.get(position).getLocation());
        holder.questionTime.setText(questionArrayList.get(position).getDate_time());

        if (questionArrayList.get(position).getImageURL() != null) {
            Picasso.with(context)
                    .load(questionArrayList.get(position).getImageURL())
                    .into(holder.questionImage);
        }
    }

    static class myViewHolder extends RecyclerView.ViewHolder {

        TextView name;
        TextView questionTxt;
        TextView questionDistance;
        Button totalAnswersBtn;
        TextView questionTime;
        ImageView questionImage;
        RoundedImageView profilePicture;
        CardView card_view;

        public myViewHolder(View itemView) {
            super(itemView);

            questionDistance = (TextView) itemView.findViewById(R.id.questionLocation);
            name = (TextView) itemView.findViewById(R.id.name);
            questionTxt = (TextView) itemView.findViewById(R.id.questionTxt);
            totalAnswersBtn = (Button) itemView.findViewById(R.id.totalAnswersBtn);
            questionTime = (TextView) itemView.findViewById(R.id.date_time);
            questionImage = (ImageView) itemView.findViewById(R.id.questionImage);
            profilePicture = (RoundedImageView) itemView.findViewById(R.id.profilePicture);
            card_view = (CardView) itemView.findViewById(R.id.card_view);

        }
    }
}

