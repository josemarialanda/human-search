package com.landagmail.josemaria.humansearchinc.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.facebook.Profile;
import com.landagmail.josemaria.humansearchinc.Activities.AnswerScreenActivity;
import com.landagmail.josemaria.humansearchinc.Activities.ExternalProfileFetch;
import com.landagmail.josemaria.humansearchinc.Activities.ZoomInActivity;
import com.landagmail.josemaria.humansearchinc.Constants;
import com.landagmail.josemaria.humansearchinc.Fragments.MyActivityFragment;
import com.landagmail.josemaria.humansearchinc.R;
import com.landagmail.josemaria.humansearchinc.Tasks.POST.POJO.ResponseClasses.MyQuestionFetchResponseClass;
import com.landagmail.josemaria.humansearchinc.Utils.CommonItems;
import com.landagmail.josemaria.humansearchinc.Utils.CustomViews.RoundImageView.RoundedImageView;
import com.landagmail.josemaria.humansearchinc.Utils.HashTagHelper.HashTagHelper;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class MyActivityAdapter extends RecyclerView.Adapter<MyActivityAdapter.myViewHolder> {
    // FIX ME
    private LayoutInflater layoutInflater;
    Context context;
    private ArrayList<MyQuestionFetchResponseClass> questionArrayList = new ArrayList<>();
    MyActivityFragment fragmentContext;
    MyActivityAdapter adapter;

    public MyActivityAdapter(FragmentActivity activity, MyActivityFragment fragmentContext) {
        layoutInflater = LayoutInflater.from(activity);
        context = activity;
        this.fragmentContext = fragmentContext;
        adapter = this;
    }


    public void setData(ArrayList<MyQuestionFetchResponseClass> list) {
        questionArrayList = list;
        notifyDataSetChanged();
    }

    @Override
    public myViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.my_questions_list, parent, false);
        myViewHolder viewHolder = new myViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final myViewHolder holder, final int position) {

        holder.card_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(fragmentContext.getActivity(), AnswerScreenActivity.class);
                intent.putExtra("questionID", questionArrayList.get(position).getQuestionID());
                intent.putExtra("userID", questionArrayList.get(position).getUserID());
                intent.putExtra("longitude", questionArrayList.get(position).getLongitude());
                intent.putExtra("latitude", questionArrayList.get(position).getLatitude());
                intent.putExtra("questionImage", questionArrayList.get(position).getImageURL());
                intent.putExtra("name", questionArrayList.get(position).getName());
                intent.putExtra("question", questionArrayList.get(position).getQuestionText());
                intent.putExtra("profilePicture", questionArrayList.get(position).getProfilePicture());
                intent.putExtra("date_time", questionArrayList.get(position).getDate_time());
                intent.putExtra("address", questionArrayList.get(position).getAddress());
                fragmentContext.getActivity().startActivity(intent);
            }
        });

        HashTagHelper mTextHashTagHelper = HashTagHelper.Creator.create(context.getResources().getColor(R.color.primary), new HashTagHelper.OnHashTagClickListener() {
            @Override
            public void onHashTagClicked(String hashTag) {
                Intent intent = new Intent(context, ExternalProfileFetch.class);
                intent.putExtra("ALIAS", hashTag);
                context.startActivity(intent);
            }
        });
        mTextHashTagHelper.handle(holder.questionTxt);

        holder.profilePicture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (questionArrayList.get(position).getUserID().equals(Profile.getCurrentProfile().getId())) {
                    // DO NOTHING
                } else {
                    Intent intent = new Intent(context, ExternalProfileFetch.class);
                    intent.putExtra("ALIAS", questionArrayList.get(position).getName());
                    context.startActivity(intent);
                }
            }
        });
        holder.name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (questionArrayList.get(position).getUserID().equals(Profile.getCurrentProfile().getId())) {
                    // DO NOTHING
                } else {
                    Intent intent = new Intent(context, ExternalProfileFetch.class);
                    intent.putExtra("ALIAS", questionArrayList.get(position).getName());
                    context.startActivity(intent);
                }
            }
        });

        if (questionArrayList.get(position).getUserID().equals(Profile.getCurrentProfile().getId())) {
            holder.menuBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    PopupMenu popup = new PopupMenu(context, v);
                    MenuInflater inflater = popup.getMenuInflater();
                    inflater.inflate(R.menu.my_questions_menu, popup.getMenu());
                    popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                        @Override
                        public boolean onMenuItemClick(MenuItem item) {
                            int id = item.getItemId();
                            if (id == R.id.action_delete) {
                                CommonItems.delete_question(context, position, questionArrayList, adapter, Profile.getCurrentProfile().getId(), questionArrayList.get(position).getQuestionID());
                                return true;
                            }
                            return false;
                        }
                    });
                    popup.show();
                }
            });
        } else {
            holder.menuBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    PopupMenu popup = new PopupMenu(context, v);
                    MenuInflater inflater = popup.getMenuInflater();
                    inflater.inflate(R.menu.association_question_menu, popup.getMenu());
                    popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                        @Override
                        public boolean onMenuItemClick(MenuItem item) {
                            int id = item.getItemId();
                            if (id == R.id.action_unfollow_question) {
                                CommonItems.dissociate_question(context, position, questionArrayList, adapter, Profile.getCurrentProfile().getId(), questionArrayList.get(position).getQuestionID());
                                return true;
                            }
                            return false;
                        }
                    });
                    popup.show();
                }
            });
        }

        holder.totalAnswersBtn.setText(questionArrayList.get(position).getAnswerCount());

        if (!questionArrayList.get(position).getProfilePicture().equals(Constants.NO_IMAGE)) {
            Picasso.with(context)
                    .load(questionArrayList.get(position).getProfilePicture())
                    .fit()
                    .centerCrop()
                    .into(holder.profilePicture);
        } else if (questionArrayList.get(position).getProfilePicture().equals(Constants.NO_IMAGE)) {
            holder.profilePicture.setImageDrawable(context.getResources().getDrawable(R.drawable.profile_image));
        }

        if (questionArrayList.get(position).getImageURL() != null) {
            Picasso.with(context)
                    .load(questionArrayList.get(position).getImageURL())
                    .into(holder.questionImage);
        }

        holder.name.setText(questionArrayList.get(position).getName());
        holder.questionLocation.setText(questionArrayList.get(position).getAddress());
        holder.date_time.setText(questionArrayList.get(position).getDate_time());

        holder.questionImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, ZoomInActivity.class);
                intent.putExtra("IMAGE_URL", questionArrayList.get(position).getImageURL());
                context.startActivity(intent);
            }
        });

        holder.questionTxt.setText(questionArrayList.get(position).getQuestionText());

        holder.totalAnswersBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(fragmentContext.getActivity(), AnswerScreenActivity.class);
                intent.putExtra("questionID", questionArrayList.get(position).getQuestionID());
                intent.putExtra("userID", questionArrayList.get(position).getUserID());
                intent.putExtra("longitude", questionArrayList.get(position).getLongitude());
                intent.putExtra("latitude", questionArrayList.get(position).getLatitude());
                intent.putExtra("questionImage", questionArrayList.get(position).getImageURL());
                intent.putExtra("name", questionArrayList.get(position).getName());
                intent.putExtra("question", questionArrayList.get(position).getQuestionText());
                intent.putExtra("profilePicture", questionArrayList.get(position).getProfilePicture());
                intent.putExtra("date_time", questionArrayList.get(position).getDate_time());
                intent.putExtra("address", questionArrayList.get(position).getAddress());
                fragmentContext.getActivity().startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return questionArrayList.size();
    }

    static class myViewHolder extends RecyclerView.ViewHolder {

        TextView questionTxt;
        Button totalAnswersBtn;
        ImageView questionImage;
        TextView name;
        TextView questionLocation;
        TextView date_time;
        RoundedImageView profilePicture;
        CardView questionLayout;
        ImageButton menuBtn;
        CardView card_view;

        public myViewHolder(View itemView) {
            super(itemView);
            questionTxt = (TextView) itemView.findViewById(R.id.questionTxt);
            totalAnswersBtn = (Button) itemView.findViewById(R.id.totalAnswersBtn);
            questionImage = (ImageView) itemView.findViewById(R.id.questionImage);
            name = (TextView) itemView.findViewById(R.id.name);
            questionLocation = (TextView) itemView.findViewById(R.id.questionLocation);
            date_time = (TextView) itemView.findViewById(R.id.date_time);
            profilePicture = (RoundedImageView) itemView.findViewById(R.id.profilePicture);
            questionLayout = (CardView) itemView.findViewById(R.id.card_view);
            menuBtn = (ImageButton) itemView.findViewById(R.id.menuBtn);
            card_view = (CardView) itemView.findViewById(R.id.card_view);
        }
    }
}

