package com.landagmail.josemaria.humansearchinc.Activities.InitScreens;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.Profile;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.landagmail.josemaria.humansearchinc.Logging.L;
import com.landagmail.josemaria.humansearchinc.MainApplication;
import com.landagmail.josemaria.humansearchinc.R;
import com.landagmail.josemaria.humansearchinc.Tasks.POST.Queries.PostHelperClass;
import com.landagmail.josemaria.humansearchinc.Utils.UrbanAirshipUtils.DeviceAdressing;
import com.urbanairship.UAirship;

public class WelcomeActivity extends AppCompatActivity {

    private CallbackManager callbackManager;
    private ResponseReceiver receiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);

        receiver = new ResponseReceiver();

        final LoginButton loginButton = (LoginButton) findViewById(R.id.login_button);
        loginButton.setReadPermissions("user_friends", "email");
        callbackManager = CallbackManager.Factory.create();
        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                DeviceAdressing.NamedUsers.associateChannelToNamedUserID(loginResult.getAccessToken().getUserId());
            }

            @Override
            public void onCancel() {
                Toast.makeText(WelcomeActivity.this, R.string.cancelLogin, Toast.LENGTH_LONG).show();
            }

            @Override
            public void onError(FacebookException exception) {
                Toast.makeText(WelcomeActivity.this, R.string.loginError, Toast.LENGTH_LONG).show();
                Log.i("facebook_error", exception.getLocalizedMessage());
                Log.i("facebook_error", exception.getMessage());
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

    }

    @Override
    protected void onResume() {
        super.onResume();
        IntentFilter filter = new IntentFilter(ResponseReceiver.ACTION_RESP);
        filter.addCategory(Intent.CATEGORY_DEFAULT);
        registerReceiver(receiver, filter);
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(receiver);
    }

    public class ResponseReceiver extends BroadcastReceiver {

        public static final String ACTION_RESP = "ACCOUNT_CHECK";

        @Override
        public void onReceive(Context context, Intent intent) {

            int result = intent.getIntExtra("ACCOUNT_INFO", -1);

            switch (result) {
                case 1:
                    intent = new Intent(WelcomeActivity.this, SetupActivity.class);
                    WelcomeActivity.this.startActivity(intent);
                    WelcomeActivity.this.finish();
                    break;
                case 2:
                    PostHelperClass.ProfileQueries.fetch_profile(Profile.getCurrentProfile().getId());
                    MainApplication.getAppContext().getSharedPreferences("PREFERENCE", Context.MODE_PRIVATE).edit()
                            .putBoolean("firstTimeUser", false).apply();
                    WelcomeActivity.this.finish();
                    break;
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    public void siguienteBtn(View view) {
        L.m(getString(R.string.noLogin));
    }
}
