package com.landagmail.josemaria.humansearchinc;

import android.os.Build;

import com.landagmail.josemaria.humansearchinc.Utils.DeviceName;

public class Constants {

    // PERMISSIONS
    public static final int REQUEST_FINE_LOCATION = 0;
    public static final int REQUEST_CAMERA = 1;
    public static final int REQUEST_CONTACTS = 2;
    public static final int REQUEST_STORAGE = 3;

    public static final long INTERVAL_FIVE_MINUTES = 300l;
    public static final long INTERVAL_TEN_MINUTES = 600l;
    public static final long INTERVAL_FIFTEEN_MINUTES = 900l;

    public static final int ANSWERS_PRESENT = 1;
    public static final int ANSWERS_NOT_PRESENT = -2;

    public static final int QUESTIONS_PRESENT = 1;
    public static final int QUESTIONS_NOT_PRESENT = -2;

    public final static String NO_IMAGE = "NO_IMAGE";

    public static final String IDENTITY_POOL_ID = "us-east-1:1f694105-6524-4841-a431-df55689d9fbe";

    // CARTODB

    public static final String FETCH_ANSWERS = "http://lokl.elasticbeanstalk.com/fetch_answers";
    public static final String INSERT_ANSWERS = "http://lokl.elasticbeanstalk.com/push_answer";
    public static final String DELETE_QUESTIONS = "http://lokl.elasticbeanstalk.com/delete_question";
    public static final String FETCH_QUESTIONS = "http://lokl.elasticbeanstalk.com/fetch_questions";
    public static final String FETCH_MY_QUESTIONS = "http://lokl.elasticbeanstalk.com/fetch_my_questions";
    public static final String INSERT_QUESTIONS = "http://lokl.elasticbeanstalk.com/push_question";
    public static final String DISSACIATE_QUESTION = "http://lokl.elasticbeanstalk.com/dissociate_question";
    public static final String FETCH_ALL_QUESTIONS = "http://lokl.elasticbeanstalk.com/get_all_questions";
    public static final String FETCH_HASHTAG_QUESTIONS = "http://lokl.elasticbeanstalk.com/get_hashtag_questions";

    public static final String CHECK_FOR_ANSWERS = "http://lokl.elasticbeanstalk.com/check_for_answers";
    public static final String CHECK_FOR_QUESTIONS = "http://lokl.elasticbeanstalk.com/check_for_questions";


    // FRIENDS

    public static final String FOLLOW = "http://lokl.elasticbeanstalk.com/follow";
    public static final String UNFOLLOW = "http://lokl.elasticbeanstalk.com/unfollow";
    public static final String CHECK_IF_FRIEND = "http://lokl.elasticbeanstalk.com/check_if_friend";
    public static final String FIND_FRIENDS = "http://lokl.elasticbeanstalk.com/find_friends";

    // PROFILE

    public static final String UPDATE_USER_SETTINGS = "http://lokl.elasticbeanstalk.com/update_user_settings";
    public static final String UPDATE_PROFILE = "http://lokl.elasticbeanstalk.com/update_profile";
    public static final String PROFILE_FOLLOW_FETCH = "http://lokl.elasticbeanstalk.com/fetch_follow_info";
    public static final String FETCH_FOLLOWING = "http://lokl.elasticbeanstalk.com/fetch_following";
    public static final String FETCH_FOLLOWERS = "http://lokl.elasticbeanstalk.com/fetch_followers";
    public static final String FETCH_PROFILE = "http://lokl.elasticbeanstalk.com/fetch_profile";
    public static final String FETCH_EXTERNAl_PROFILE = "http://lokl.elasticbeanstalk.com/fetch_external_profile";
    public static final String CHECK_PROFILE = "http://lokl.elasticbeanstalk.com/check_user_existance";

    public static final String NEW_ACCOUNT = "NEW_USER";
    public static final String OLD_USER = "OLD_USER";

    public final static int PHOTO_UPLOAD_OR_NO_CHANGE = 1;
    public final static int USERNAME_IN_USE = 2;
    public final static int PHOTO_AND_OR_USERNAME_UPDATED = 3;
    public final static int NEW_USER_ADDED = 4;


    public final static String PHOTO_UPLOAD_OR_NO_CHANGE_STRING = "PHOTO_UPLOAD_OR_NO_CHANGE_STRING";
    public final static String USERNAME_IN_USE_STRING = "USERNAME_IN_USE_STRING";
    public final static String PHOTO_AND_OR_USERNAME_UPDATED_STRING = "PHOTO_AND_OR_USERNAME_UPDATED_STRING";
    public final static String USER_SETTINGS_UPDATED_STRING = "USER_SETTINGS_UPDATED";


}
