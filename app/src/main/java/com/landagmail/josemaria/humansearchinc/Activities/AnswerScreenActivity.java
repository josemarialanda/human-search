package com.landagmail.josemaria.humansearchinc.Activities;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.Profile;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.single.PermissionListener;
import com.landagmail.josemaria.humansearchinc.Camera.CameraActivity;
import com.landagmail.josemaria.humansearchinc.Logging.L;
import com.landagmail.josemaria.humansearchinc.MainApplication;
import com.landagmail.josemaria.humansearchinc.R;
import com.landagmail.josemaria.humansearchinc.Tasks.POST.POJO.ResponseClasses.FetchResponseClass;
import com.landagmail.josemaria.humansearchinc.Tasks.POST.Queries.PostHelperClass;
import com.landagmail.josemaria.humansearchinc.Utils.CustomViews.PreCachingLayoutManager;
import com.landagmail.josemaria.humansearchinc.Utils.CustomViews.RoundImageView.RoundedImageView;
import com.landagmail.josemaria.humansearchinc.Utils.DeviceUtils;
import com.landagmail.josemaria.humansearchinc.adapters.AnswerScreenAdapter;
import com.rockerhieu.emojicon.EmojiconGridFragment;
import com.rockerhieu.emojicon.EmojiconsFragment;
import com.rockerhieu.emojicon.emoji.Emojicon;
import com.sothree.slidinguppanel.SlidingUpPanelLayout;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.ArrayList;

public class AnswerScreenActivity extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, EmojiconGridFragment.OnEmojiconClickedListener, EmojiconsFragment.OnEmojiconBackspaceClickedListener {

    private AnswerScreenAdapter adapter;
    private String questionID;
    private String userID;
    private String questionImage = null;
    private LatLng location;
    private String name;
    private String question;
    private String address;
    private String time;
    private String profilePicture;

    private ImageView emoticonImageView;
    //private ImageView cameraImageView;

    private RoundedImageView profilePictureImageView;
    private TextView questionLocation;
    private TextView date_time;
    private TextView questionTxt;
    private TextView nameTxtView;
    private ImageButton sendBtn;
    private ResponseReceiver receiver;
    private EditText answerEditTxt;
    private ImageView picture;
    private ImageView questionImageView;
    private static final int REQUEST_CAMERA = 0;
    public static final int CAMERA_REQUEST_CODE = 1999;
    private String filePath = "";
    private GoogleApiClient mGoogleApiClient;
    private GoogleMap map;
    private SlidingUpPanelLayout sliding_layout;
    public RecyclerView commentsRecyclerView;
    private RelativeLayout emojicons;
    boolean isEmojiconFragmentOpen = false;

    @Override
    public void onConnected(Bundle bundle) {
        Location location = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        MainApplication.setIsLocationSet(true);
        MainApplication.setLocation(location);
        refreshItems();
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    @Override
    protected void onStart() {
        super.onStart();
        Dexter.checkPermission(new PermissionListener() {
            @Override
            public void onPermissionGranted(PermissionGrantedResponse response) {
                mGoogleApiClient.connect();
            }

            @Override
            public void onPermissionDenied(PermissionDeniedResponse response) {
                L.m(getString(R.string.locationPermissionDenied));
            }

            @Override
            public void onPermissionRationaleShouldBeShown(PermissionRequest permission, PermissionToken token) {
                token.continuePermissionRequest();
            }
        }, Manifest.permission.ACCESS_FINE_LOCATION);

    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comment_screen);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);

        /*
        Toolbar toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        setTitle(null);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(null);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        */

        questionImage = getIntent().getStringExtra("questionImage");
        questionID = getIntent().getStringExtra("questionID");
        userID = getIntent().getStringExtra("userID");
        location = new LatLng(Double.parseDouble(getIntent().getStringExtra("longitude")), Double.parseDouble(getIntent().getStringExtra("latitude")));
        name = getIntent().getStringExtra("name");
        question = getIntent().getStringExtra("question");
        profilePicture = getIntent().getStringExtra("profilePicture");
        time = getIntent().getStringExtra("date_time");
        address = getIntent().getStringExtra("address");

        mGoogleApiClient = new GoogleApiClient.Builder(this).addApi(LocationServices.API).addConnectionCallbacks(this).addOnConnectionFailedListener(this).build();

        sliding_layout = (SlidingUpPanelLayout) findViewById(R.id.sliding_layout);
        sliding_layout.setPanelSlideListener(new SlidingUpPanelLayout.PanelSlideListener() {
            @Override
            public void onPanelSlide(View panel, float slideOffset) {

            }

            @Override
            public void onPanelCollapsed(View panel) {
                map.getUiSettings().setAllGesturesEnabled(true);
            }

            @Override
            public void onPanelExpanded(View panel) {
                map.getUiSettings().setAllGesturesEnabled(false);
            }

            @Override
            public void onPanelAnchored(View panel) {

            }

            @Override
            public void onPanelHidden(View panel) {

            }
        });

        sliding_layout.setPanelState(SlidingUpPanelLayout.PanelState.EXPANDED);

        map = ((SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map)).getMap();
        map.getUiSettings().setAllGesturesEnabled(false);
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(location, 15));
        map.animateCamera(CameraUpdateFactory.zoomTo(10), 2000, null);

        map.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                if (sliding_layout.getPanelState() == SlidingUpPanelLayout.PanelState.EXPANDED) {
                    sliding_layout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
                }
            }
        });

        CircleOptions circleOptions = new CircleOptions()
                .center(location)
                .radius(1000)
                .fillColor(Color.parseColor("#882cadff"))
                .strokeColor(Color.TRANSPARENT)
                .strokeWidth(0);
        map.addCircle(circleOptions);
        map.addMarker(new MarkerOptions().position(location));


        // MAKE THIS WORK!
            /*
            float yPixel = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 50, r.getDisplayMetrics());
            map.moveCamera(CameraUpdateFactory.scrollBy(0, yPixel));
            */

        receiver = new ResponseReceiver();

        PreCachingLayoutManager layoutManager = new PreCachingLayoutManager(this);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        layoutManager.setExtraLayoutSpace(DeviceUtils.getScreenHeight(this));
        layoutManager.setStackFromEnd(true);

        commentsRecyclerView = (RecyclerView) findViewById(R.id.questionRecyclerView);

        commentsRecyclerView.setLayoutManager(layoutManager);
        adapter = new AnswerScreenAdapter(this, questionImage);
        commentsRecyclerView.setAdapter(adapter);

        questionLocation = (TextView) findViewById(R.id.questionLocation);
        date_time = (TextView) findViewById(R.id.date_time);
        profilePictureImageView = (RoundedImageView) findViewById(R.id.profilePicture);
        questionTxt = (TextView) findViewById(R.id.questionTxt);
        questionImageView = (ImageView) findViewById(R.id.questionImage);
        nameTxtView = (TextView) findViewById(R.id.name);
        answerEditTxt = (EditText) findViewById(R.id.answerEditTxt);
        sendBtn = (ImageButton) findViewById(R.id.sendBtn);
        picture = (ImageView) findViewById(R.id.picture);
        picture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PopupMenu popup = new PopupMenu(AnswerScreenActivity.this, v);
                MenuInflater inflater = popup.getMenuInflater();
                inflater.inflate(R.menu.manu_delete, popup.getMenu());
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        int id = item.getItemId();
                        if (id == R.id.action_remove_picture) {
                            picture.setImageDrawable(null);
                            filePath = "";
                            return true;
                        }
                        return false;
                    }
                });
                popup.show();
            }
        });

        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.emojicons, EmojiconsFragment.newInstance(true), "EMOJICON")
                .commit();

        emoticonImageView = (ImageView) findViewById(R.id.emoticonImageView);
        emojicons = (RelativeLayout) findViewById(R.id.emojicons);
        emoticonImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (isEmojiconFragmentOpen) {
                    isEmojiconFragmentOpen = false;
                    hideEmojiconFragment();

                } else {
                    if (answerEditTxt.isFocused()) {
                        View view = AnswerScreenActivity.this.getCurrentFocus();
                        if (view != null) {
                            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                        }
                        isEmojiconFragmentOpen = true;
                        showEmojiconFragment();
                    } else {
                        isEmojiconFragmentOpen = true;
                        showEmojiconFragment();
                    }
                }
            }
        });

        profilePictureImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(AnswerScreenActivity.this, ExternalProfileFetch.class);
                intent.putExtra("ALIAS", name);
                AnswerScreenActivity.this.startActivity(intent);
            }
        });

        nameTxtView.setText(name);
        questionTxt.setText(question);
        questionLocation.setText(address);
        date_time.setText(time);

        if (profilePicture != null) {
            Picasso.with(this)
                    .load(profilePicture)
                    .fit()
                    .centerCrop()
                    .into(profilePictureImageView);
        } else {
            profilePictureImageView.setImageDrawable(getResources().getDrawable(R.drawable.profile_image));
        }

        sendBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (answerEditTxt.getText() != null && answerEditTxt.getText().length() != 0) {

                    if (isOnline()) {
                        if (MainApplication.isLocationSet()) {

                            if (!getFilePath().equals("")) {
                                PostHelperClass.CartoDBQueries.insert_answers(MainApplication.latitude, MainApplication.longitude, Profile.getCurrentProfile().getId(), answerEditTxt.getText().toString(), questionID, getFilePath(), userID);
                                picture.setImageDrawable(null);
                                filePath = "";
                                File file = new File(getSelectedImagePath());
                                boolean deleted = file.delete();
                                Log.i("Temp_file", String.valueOf(deleted));

                            } else {
                                PostHelperClass.CartoDBQueries.insert_answers(MainApplication.latitude, MainApplication.longitude, Profile.getCurrentProfile().getId(), answerEditTxt.getText().toString(), questionID, null, userID);
                            }

                            answerEditTxt.setText("");
                        } else {
                            L.m(getString(R.string.gettingLocation));
                        }

                    } else {
                        Snackbar.make(findViewById(android.R.id.content), R.string.noInternetConnection, Snackbar.LENGTH_LONG)
                                .setAction(R.string.connectToInternet, new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        startActivity(new Intent(Settings.ACTION_WIFI_SETTINGS));
                                    }
                                })
                                .setActionTextColor(Color.RED)
                                .show();
                    }
                } else {
                    Toast.makeText(AnswerScreenActivity.this,
                            R.string.startAsking,
                            Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void showEmojiconFragment() {
        emojicons.setVisibility(View.VISIBLE);
    }

    private void hideEmojiconFragment() {
        emojicons.setVisibility(View.GONE);
    }

    @Override
    public void onEmojiconClicked(Emojicon emojicon) {
        EmojiconsFragment.input(answerEditTxt, emojicon);
    }

    @Override
    public void onEmojiconBackspaceClicked(View v) {
        EmojiconsFragment.backspace(answerEditTxt);
    }

    private boolean isOnline() {
        ConnectivityManager cm =
                (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }

    private void refreshItems() {
        fetchData();
    }

    private void fetchData() {
        PostHelperClass.CartoDBQueries.fetch_answers(MainApplication.latitude, MainApplication.longitude, questionID);
    }

    private void setListData(ArrayList<FetchResponseClass> answersArrayList) {
        adapter.setData(answersArrayList);
    }

    public class ResponseReceiver extends BroadcastReceiver {

        public static final String ACTION_RESP = "QUESTIONS_FETCHED";
        public static final String QUESTIONS_LIST = "QUESTIONS_LIST";
        public static final String ANSWER_PUSHED = "ANSWER_PUSHED";

        @Override
        public void onReceive(Context context, Intent intent) {

            if (intent.getIntExtra(ANSWER_PUSHED, 2) == 2) {
                refreshItems();
            }
            if (intent.getSerializableExtra(QUESTIONS_LIST) != null) {
                ArrayList<FetchResponseClass> answersArrayList = (ArrayList<FetchResponseClass>) intent.getSerializableExtra(QUESTIONS_LIST);
                setListData(answersArrayList);
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        IntentFilter filter = new IntentFilter(ResponseReceiver.ACTION_RESP);
        filter.addCategory(Intent.CATEGORY_DEFAULT);
        registerReceiver(receiver, filter);
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(receiver);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_answer_screen, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            Intent intent = new Intent(this, ProfileActivity.class);
            startActivity(intent);
        }
        if (id == R.id.takePicture) {
            requestForCameraPermission();

        }
        if (id == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        Log.d("CDA", "onBackPressed Called");
        if (isEmojiconFragmentOpen) {
            isEmojiconFragmentOpen = false;
            hideEmojiconFragment();
        } else if (sliding_layout.getPanelState() == SlidingUpPanelLayout.PanelState.COLLAPSED) {
            sliding_layout.setPanelState(SlidingUpPanelLayout.PanelState.EXPANDED);
        } else {
            super.onBackPressed();
        }
    }

    public void requestForCameraPermission() {
        final String permission = Manifest.permission.CAMERA;
        if (ContextCompat.checkSelfPermission(this, permission)
                != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, permission)) {
                // Show permission rationale
            } else {
                // Handle the result in Activity#onRequestPermissionResult(int, String[], int[])
                ActivityCompat.requestPermissions(this, new String[]{permission}, CAMERA_REQUEST_CODE);
            }
        } else {
            Intent startCustomCameraIntent = new Intent(this, CameraActivity.class);
            startActivityForResult(startCustomCameraIntent, REQUEST_CAMERA);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode != RESULT_OK) return;

        if (requestCode == REQUEST_CAMERA) {
            filePath = getRealPathFromURI(String.valueOf(data.getData()));
            picture.setImageBitmap(decodeFile(filePath));

        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private String getRealPathFromURI(String contentURI) {
        Uri contentUri = Uri.parse(contentURI);
        Cursor cursor = getContentResolver().query(contentUri, null, null, null, null);
        if (cursor == null) {
            return contentUri.getPath();
        } else {
            cursor.moveToFirst();
            int index = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            return cursor.getString(index);
        }
    }

    public Bitmap decodeFile(String path) {
        try {
            // Decode image size
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            BitmapFactory.decodeFile(path, o);
            // The new size we want to scale to
            final int REQUIRED_SIZE = 500;

            // Find the correct scale value. It should be the power of 2.
            int scale = 1;
            while (o.outWidth / scale / 2 >= REQUIRED_SIZE && o.outHeight / scale / 2 >= REQUIRED_SIZE)
                scale *= 2;

            // Decode with inSampleSize
            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize = scale;
            return BitmapFactory.decodeFile(path, o2);
        } catch (Throwable e) {
            e.printStackTrace();
        }
        return null;
    }

    public String getSelectedImagePath() {
        return "";
    }

    public String getFilePath() {
        return filePath;
    }

    private void setErrorMessage(int res) {
        L.t(getString(res));
    }
}
