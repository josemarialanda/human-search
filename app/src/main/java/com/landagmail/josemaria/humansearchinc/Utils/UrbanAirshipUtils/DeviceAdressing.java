package com.landagmail.josemaria.humansearchinc.Utils.UrbanAirshipUtils;

import android.content.Context;

import com.facebook.Profile;
import com.landagmail.josemaria.humansearchinc.MainApplication;
import com.landagmail.josemaria.humansearchinc.Tasks.POST.Queries.PostHelperClass;
import com.urbanairship.UAirship;

import java.util.HashSet;
import java.util.Set;

public class DeviceAdressing {

    public static class Tags {

        private static void addTag(String tag) {
            Set<String> tags = new HashSet<String>();
            tags = UAirship.shared().getPushManager().getTags();
            tags.add(tag);
            UAirship.shared().getPushManager().setTags(tags);
        }

        private static class TagGroups {

            private static void addTagToTagGroup(String tagGroup, String tag) {
                UAirship.shared().getPushManager().editTagGroups()
                        .addTag(tagGroup, tag)
                        .apply();
            }

            private static void removeTagFromTagGroup(String tagGroup, String tag) {
                UAirship.shared().getPushManager().editTagGroups()
                        .removeTag(tagGroup, tag)
                        .apply();
            }

            private static void addSetOfTagsToTagGroup(String tagGroup, String... tags) {
                Set<String> tagsToAdd = new HashSet<>();
                for (String tag : tags) {
                    tagsToAdd.add(tag);
                }
                UAirship.shared().getPushManager().editTagGroups()
                        .addTags(tagGroup, tagsToAdd)
                        .apply();
            }

            private static void removeSetOfTagsFromTagGroup(String tagGroup, String... tags) {
                Set<String> tagsToRemove = new HashSet<>();
                for (String tag : tags) {
                    tagsToRemove.add(tag);
                }
                UAirship.shared().getPushManager().editTagGroups()
                        .removeTags(tagGroup, tagsToRemove)
                        .apply();
            }

            private static void addAndRemoveTagForTagGroup(String tagGroup, String tagToAdd, String tagToRemove) {
                UAirship.shared().getPushManager().editTagGroups()
                        .addTag(tagGroup, tagToAdd)
                        .removeTag(tagGroup, tagToRemove)
                        .apply();
            }
        }
    }

    public static class NamedUsers {

        /*
        Note
        Associating the channel with a Named User ID, will implicitly disassociate
        the channel from the previously associated Named User ID, if it existed.
         */

        public static void associateChannelToNamedUserID(String namedUserID) {
            UAirship.shared().getPushManager().getNamedUser().setId(namedUserID);
            PostHelperClass.ProfileQueries.profile_check(namedUserID);
        }

        public static void disassociateChannelFromNamedUserID() {
            UAirship.shared().getPushManager().getNamedUser().setId(null);
        }
    }
}
