package com.landagmail.josemaria.humansearchinc.Tasks.POST.POJO;

public class UserSettings {

    boolean enableNotifications;

    public UserSettings(boolean enableNotifications) {
        this.enableNotifications = enableNotifications;
    }

    public boolean isEnableNotifications() {
        return enableNotifications;
    }

    public void setEnableNotifications(boolean enableNotifications) {
        this.enableNotifications = enableNotifications;
    }
}
