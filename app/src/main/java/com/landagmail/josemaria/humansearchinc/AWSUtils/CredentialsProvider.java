package com.landagmail.josemaria.humansearchinc.AWSUtils;

import android.content.Context;
import com.amazonaws.auth.CognitoCachingCredentialsProvider;
import com.amazonaws.regions.Regions;
import com.landagmail.josemaria.humansearchinc.Constants;

public class CredentialsProvider {

    private static CognitoCachingCredentialsProvider credentialsProvider;

    public static CognitoCachingCredentialsProvider getCredentials(Context context){

            credentialsProvider = new CognitoCachingCredentialsProvider(
                    context,
                    Constants.IDENTITY_POOL_ID,
                    Regions.US_EAST_1
            );
            return credentialsProvider;

    }
}
