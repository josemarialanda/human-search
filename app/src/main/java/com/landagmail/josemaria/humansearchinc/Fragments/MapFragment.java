package com.landagmail.josemaria.humansearchinc.Fragments;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.provider.Settings;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;

import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.landagmail.josemaria.humansearchinc.Activities.AnswerScreenActivity;
import com.landagmail.josemaria.humansearchinc.Dialogs.CommonDialogs;
import com.landagmail.josemaria.humansearchinc.Logging.L;
import com.landagmail.josemaria.humansearchinc.MainApplication;
import com.landagmail.josemaria.humansearchinc.R;
import com.landagmail.josemaria.humansearchinc.Tasks.POST.POJO.ResponseClasses.FetchResponseClass;
import com.landagmail.josemaria.humansearchinc.Tasks.POST.Queries.PostHelperClass;
import com.landagmail.josemaria.humansearchinc.Utils.CommonItems;
import com.landagmail.josemaria.humansearchinc.Utils.CustomViews.CustomAutoCompleteTextView;
import com.landagmail.josemaria.humansearchinc.Utils.CustomViews.PreCachingLayoutManager;
import com.landagmail.josemaria.humansearchinc.Utils.CustomViews.materialdialogsearchview.SearchAdapter;
import com.landagmail.josemaria.humansearchinc.Utils.CustomViews.materialdialogsearchview.SharedPreference;
import com.landagmail.josemaria.humansearchinc.Utils.CustomViews.materialdialogsearchview.Utils;
import com.landagmail.josemaria.humansearchinc.Utils.DeviceUtils;
import com.landagmail.josemaria.humansearchinc.adapters.LocalQuestionsAdapter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

public class MapFragment extends Fragment implements View.OnClickListener {

    private GoogleMap map;
    private ResponseReceiver receiver;
    private HashMap<String, FetchResponseClass> questions = new HashMap<>();
    private ProgressDialog dialog;

    SwipeRefreshLayout swipeRefreshLayout;
    RecyclerView questionRecyclerView;
    private LocalQuestionsAdapter adapter;
    private RelativeLayout mapView;
    private static final int PLACE_AUTOCOMPLETE_REQUEST_CODE = 0;

    public MapFragment() {
    }

    private void autoCompleteIntent() {
        try {
            Intent intent =
                    new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_FULLSCREEN)
                            .build(getActivity());
            startActivityForResult(intent, PLACE_AUTOCOMPLETE_REQUEST_CODE);
        } catch (GooglePlayServicesRepairableException e) {
            GooglePlayServicesUtil
                    .getErrorDialog(e.getConnectionStatusCode(), getActivity(), 0);
        } catch (GooglePlayServicesNotAvailableException e) {
            // Handle the exception
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PLACE_AUTOCOMPLETE_REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                Place place = PlaceAutocomplete.getPlace(getActivity(), data);
                goToLocation(place);
                Log.i("PLACE_TAG", "Place:" + place.toString());
            } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                Status status = PlaceAutocomplete.getStatus(getActivity(), data);
                Log.i("PLACE_TAG", status.getStatusMessage());
            } else if (requestCode == Activity.RESULT_CANCELED) {

            }
        }
    }

    private boolean isOnline() {
        ConnectivityManager cm =
                (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }

    private void goToLocation(Place place) {
        toolbarSearchDialog.dismiss();
        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(place.getLatLng())
                .zoom(15)
                .build();
        map.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        receiver = new ResponseReceiver();
    }

    @Override
    public void onResume() {
        super.onResume();
        IntentFilter filter = new IntentFilter(ResponseReceiver.ACTION_RESP);
        filter.addCategory(Intent.CATEGORY_DEFAULT);
        getActivity().registerReceiver(receiver, filter);

    }

    @Override
    public void onPause() {
        super.onPause();
        getActivity().unregisterReceiver(receiver);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_map, container, false);
        getMap();
        setHasOptionsMenu(true);
        ImageButton reloadMapBtn = (ImageButton) view.findViewById(R.id.reloadMapBtn);
        reloadMapBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PostHelperClass.CartoDBQueries.fetch_all_questions();
                dialog.show();
            }
        });

        toolbarSearchDialog = new Dialog(getActivity(), R.style.MaterialSearch);
        toolbarSearchDialog.setOnKeyListener(new DialogInterface.OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    if (toolbarSearchDialog.isShowing()) {
                        toolbarSearchDialog.dismiss();
                    }
                    return true;
                }
                return false;
            }
        });

        questionRecyclerView = (RecyclerView) view.findViewById(R.id.questionRecyclerView);
        PreCachingLayoutManager layoutManager = new PreCachingLayoutManager(getActivity());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        layoutManager.setExtraLayoutSpace(DeviceUtils.getScreenHeight(getActivity()));
        questionRecyclerView.setLayoutManager(layoutManager);
        adapter = new LocalQuestionsAdapter(getActivity());
        questionRecyclerView.setAdapter(adapter);

        mapView = (RelativeLayout) view.findViewById(R.id.mapView);

        swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipeRefreshLayout);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (isOnline()) {
                    PostHelperClass.CartoDBQueries.fetch_all_questions();
                } else {
                    Snackbar.make(getActivity().findViewById(android.R.id.content), getString(R.string.noInternetConnection), Snackbar.LENGTH_LONG)
                            .setAction(getString(R.string.connectToInternet), new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    startActivity(new Intent(Settings.ACTION_WIFI_SETTINGS));
                                }
                            })
                                    .setActionTextColor(Color.RED)
                                    .show();
                }
            }
        });

        final Switch viewSwitch = (Switch) view.findViewById(R.id.viewSwitch);
        viewSwitch.setChecked(CommonItems.getFragmentViewPrefs());

        if (CommonItems.getFragmentViewPrefs()) {
            viewSwitch.setText(getString(R.string.listMode));
            swipeRefreshLayout.setVisibility(View.GONE);
            mapView.setVisibility(View.VISIBLE);
            mapView.setEnabled(true);
        } else {
            viewSwitch.setText(getString(R.string.mapMode));
            swipeRefreshLayout.setVisibility(View.VISIBLE);
            mapView.setVisibility(View.GONE);
            mapView.setEnabled(false);
        }

        viewSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                CommonItems.setFragmentViewPrefs(isChecked);

                if (isChecked) {
                    viewSwitch.setText(getString(R.string.listMode));
                    swipeRefreshLayout.setVisibility(View.GONE);
                    mapView.setVisibility(View.VISIBLE);
                    mapView.setEnabled(true);
                } else {
                    viewSwitch.setText(getString(R.string.mapMode));
                    CommonItems.setFragmentViewPrefs(isChecked);
                    swipeRefreshLayout.setVisibility(View.VISIBLE);
                    mapView.setVisibility(View.GONE);
                    mapView.setEnabled(false);
                }
            }
        });

        dialog = new ProgressDialog(getActivity());
        dialog.setIndeterminate(true);
        dialog.setMessage(getString(R.string.gettingLocation));
        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_map_fragment_search, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_search:
                loadToolBarSearch();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public void getMap() {
        if (map == null) {
            SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager()
                    .findFragmentById(R.id.map);
            mapFragment.getMapAsync(new OnMapReadyCallback() {
                @Override
                public void onMapReady(GoogleMap googleMap) {
                    map = googleMap;
                }
            });
        }
    }

    private void setUpMap() {
        PostHelperClass.CartoDBQueries.fetch_all_questions();
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(MainApplication.latitude, MainApplication.longitude), 15));
        map.animateCamera(CameraUpdateFactory.zoomTo(10), 2000, new GoogleMap.CancelableCallback() {
            @Override
            public void onFinish() {

            }

            @Override
            public void onCancel() {

            }
        });

        map.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                Intent intent = new Intent(getActivity(), AnswerScreenActivity.class);
                intent.putExtra("questionID", questions.get(marker.getId()).getQuestionID());
                intent.putExtra("userID", questions.get(marker.getId()).getUserID());
                intent.putExtra("longitude", questions.get(marker.getId()).getLongitude());
                intent.putExtra("latitude", questions.get(marker.getId()).getLatitude());
                intent.putExtra("name", questions.get(marker.getId()).getName());
                intent.putExtra("question", questions.get(marker.getId()).getText());
                intent.putExtra("profilePicture", questions.get(marker.getId()).getProfilePicture());
                intent.putExtra("date_time", questions.get(marker.getId()).getDate_time());
                intent.putExtra("address", questions.get(marker.getId()).getLocation());
                getActivity().startActivity(intent);
                return false;
            }
        });

    }

    public class ResponseReceiver extends BroadcastReceiver {

        public static final String ACTION_RESP = "ACTION_RESP";
        public static final String ACTION_LOCATION_SET = "ACTION_LOCATION_SET";
        public static final String ALL_QUESTIONS_FETCHED = "ALL_QUESTIONS_FETCHED";


        @Override
        public void onReceive(Context context, Intent intent) {

            if (intent.getStringExtra(ACTION_LOCATION_SET) != null) {
                setUpMap();
            }
            if (intent.getSerializableExtra(ALL_QUESTIONS_FETCHED) != null) {

                if (swipeRefreshLayout.isRefreshing()) {
                    swipeRefreshLayout.setRefreshing(false);
                }

                if (dialog.isShowing()) {
                    dialog.dismiss();
                }

                if (toolbarSearchDialog.isShowing()) {
                    toolbarSearchDialog.dismiss();
                }
                map.clear();
                questions.clear();

                try {
                    ArrayList<FetchResponseClass> resultList = (ArrayList<FetchResponseClass>) intent.getSerializableExtra(ALL_QUESTIONS_FETCHED);
                    adapter.setData(resultList);

                    if (!resultList.isEmpty()) {
                        for (FetchResponseClass response : resultList) {

                            String id = map.addMarker(
                                    new MarkerOptions().position(new LatLng(
                                            Double.parseDouble(response.getLongitude()),
                                            Double.parseDouble(response.getLatitude())))/*.
                                    icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_action_pin))*/).getId();

                            questions.put(id, response);
                        }
                    } else {
                        L.m(context.getString(R.string.noResults));
                    }

                } catch (Exception e) {
                    L.m("Response empty");
                }
            }
        }
    }

    RelativeLayout userTab;
    View userTabRibbon;

    RelativeLayout tagTab;
    View tagTabRibbon;

    RelativeLayout locationTab;
    View locationTabRibbon;

    AutoCompleteTextView edtToolSearch;
    Dialog toolbarSearchDialog;


    public void loadToolBarSearch() {

        ArrayList<String> tagsStored = SharedPreference.loadList(getActivity(), Utils.PREFS_NAME, Utils.KEY_SEARCHES);

        View view = getActivity().getLayoutInflater().inflate(R.layout.view_toolbar_search_map_fragment, null);
        LinearLayout parentToolbarSearch = (LinearLayout) view.findViewById(R.id.parent_toolbar_search);
        ImageView imgToolBack = (ImageView) view.findViewById(R.id.img_tool_back);
        edtToolSearch = (CustomAutoCompleteTextView) view.findViewById(R.id.edt_tool_search);
        ImageView img_tool_search = (ImageButton) view.findViewById(R.id.img_tool_search);
        final ListView listSearch = (ListView) view.findViewById(R.id.list_search);
        final TextView txtEmpty = (TextView) view.findViewById(R.id.txt_empty);

        userTab = (RelativeLayout) view.findViewById(R.id.userTablayout);
        userTabRibbon = view.findViewById(R.id.userTabRibbon);

        tagTab = (RelativeLayout) view.findViewById(R.id.tagTabLayout);
        tagTabRibbon = view.findViewById(R.id.tagTabRibbon);

        locationTab = (RelativeLayout) view.findViewById(R.id.locationTabLayout);
        locationTabRibbon = view.findViewById(R.id.locationTabRibbon);

        userTab.setOnClickListener(this);
        tagTab.setOnClickListener(this);
        locationTab.setOnClickListener(this);

        Utils.setListViewHeightBasedOnChildren(listSearch);

        edtToolSearch.setHint(R.string.searchHashtag);

        toolbarSearchDialog.setContentView(view);
        toolbarSearchDialog.setCancelable(false);
        toolbarSearchDialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
        toolbarSearchDialog.getWindow().setGravity(Gravity.BOTTOM);
        toolbarSearchDialog.show();
        toolbarSearchDialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);

        tagsStored = (tagsStored != null && tagsStored.size() > 0) ? tagsStored : new ArrayList<String>();
        final SearchAdapter searchAdapter = new SearchAdapter(getActivity(), tagsStored, false);

        listSearch.setVisibility(View.VISIBLE);
        listSearch.setAdapter(searchAdapter);

        listSearch.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int position, long id) {
                CommonDialogs.delete_question(MapFragment.this.getActivity(), position, adapterView, searchAdapter);
                return true;
            }
        });

        edtToolSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String result = s.toString().replaceAll(" ", "");
                if (!s.toString().equals(result)) {
                    edtToolSearch.setText(result);
                    edtToolSearch.setSelection(result.length());
                }
            }
        });


        listSearch.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                String tag = String.valueOf(adapterView.getItemAtPosition(position));
                edtToolSearch.setText(tag);
                listSearch.setVisibility(View.GONE);
            }
        });

        imgToolBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                toolbarSearchDialog.dismiss();
            }
        });

        img_tool_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (userTabRibbon.getVisibility() == View.VISIBLE) {

                }
                if (tagTabRibbon.getVisibility() == View.VISIBLE) {

                    if (!edtToolSearch.getText().toString().isEmpty()) {
                        PostHelperClass.CartoDBQueries.fetch_hashtag_questions(edtToolSearch.getText().toString());
                        dialog.show();
                        SharedPreference.addList(getActivity(), Utils.PREFS_NAME, Utils.KEY_SEARCHES, edtToolSearch.getText().toString());
                    }
                }
                if (locationTabRibbon.getVisibility() == View.VISIBLE) {

                }
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.userTablayout:
                L.m(getString(R.string.comingSoon));
                /*
                userTabRibbon.setVisibility(View.VISIBLE);
                edtToolSearch.setHint("Busca por nombre de usuario");
                tagTabRibbon.setVisibility(View.GONE);
                locationTabRibbon.setVisibility(View.GONE);
                */
                break;
            case R.id.tagTabLayout:
                tagTabRibbon.setVisibility(View.VISIBLE);
                edtToolSearch.setHint(getString(R.string.searchHashtag));
                locationTabRibbon.setVisibility(View.GONE);
                userTabRibbon.setVisibility(View.GONE);
                break;
            case R.id.locationTabLayout:
                autoCompleteIntent();
                break;
        }
    }
}
