package com.landagmail.josemaria.humansearchinc.Activities;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.Button;

import com.landagmail.josemaria.humansearchinc.Logging.L;
import com.landagmail.josemaria.humansearchinc.MainApplication;
import com.landagmail.josemaria.humansearchinc.R;
import com.landagmail.josemaria.humansearchinc.Tasks.POST.Queries.Utils;
import com.landagmail.josemaria.humansearchinc.Utils.CustomViews.ZoomableImageView;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.io.File;

public class ZoomInActivity extends Activity {

    ZoomableImageView imgPoster;
    Button shareBtn;
    private MediaScannerConnection msConn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_zoom_in);
        imgPoster = (ZoomableImageView) findViewById(R.id.imgLogo);
        final String imageUrl = getIntent().getStringExtra("IMAGE_URL");


        Picasso.with(MainApplication.getAppContext())
                .load(imageUrl)
                .into(new Target() {
                    @Override
                    public void onBitmapLoaded(final Bitmap loadedImage, Picasso.LoadedFrom from) {
                        imgPoster.setImageBitmap(loadedImage);
                    }

                    @Override
                    public void onBitmapFailed(Drawable errorDrawable) {

                    }

                    @Override
                    public void onPrepareLoad(Drawable placeHolderDrawable) {

                    }
                });

    }

    /*
    public void savePhoto(Bitmap bmp) {
        File imageFileFolder = new File(Environment.getExternalStorageDirectory(), "HumanSearch");
        imageFileFolder.mkdir();
        FileOutputStream out;
        Calendar c = Calendar.getInstance();
        String date = fromInt(c.get(Calendar.MONTH))
                + fromInt(c.get(Calendar.DAY_OF_MONTH))
                + fromInt(c.get(Calendar.YEAR))
                + fromInt(c.get(Calendar.HOUR_OF_DAY))
                + fromInt(c.get(Calendar.MINUTE))
                + fromInt(c.get(Calendar.SECOND));
        File imageFileName = new File(imageFileFolder, date + ".jpg");
        try {
            out = new FileOutputStream(imageFileName);
            bmp.compress(Bitmap.CompressFormat.JPEG, 100, out);
            out.flush();
            out.close();
            scanPhoto(imageFileName.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
        L.m("image saved at: " + imageFileFolder.getAbsolutePath());
    }


    public String fromInt(int val) {
        return String.valueOf(val);
    }


    public void scanPhoto(final String imageFileName) {
        msConn = new MediaScannerConnection(this, new MediaScannerConnection.MediaScannerConnectionClient() {
            public void onMediaScannerConnected() {
                msConn.scanFile(imageFileName, null);
                //Log.i("msClient obj  in Photo Utility", "connection established");
            }

            public void onScanCompleted(String path, Uri uri) {
                msConn.disconnect();
                //Log.i("msClient obj in Photo Utility", "scan completed");
            }
        });
        msConn.connect();
    }
    */
}