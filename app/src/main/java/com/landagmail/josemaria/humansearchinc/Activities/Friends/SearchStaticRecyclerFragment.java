package com.landagmail.josemaria.humansearchinc.Activities.Friends;

import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.facebook.Profile;
import com.landagmail.josemaria.humansearchinc.Constants;
import com.landagmail.josemaria.humansearchinc.MainApplication;
import com.landagmail.josemaria.humansearchinc.R;
import com.landagmail.josemaria.humansearchinc.Tasks.POST.POJO.ResponseClasses.FetchResponseClass;
import com.landagmail.josemaria.humansearchinc.Tasks.POST.POJO.ResponseClasses.FriendResponseClass;
import com.landagmail.josemaria.humansearchinc.Tasks.POST.Queries.PostHelperClass;
import com.landagmail.josemaria.humansearchinc.Utils.CustomViews.RoundImageView.RoundedImageView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Collections;

public class SearchStaticRecyclerFragment extends Fragment {

    public SearchStaticRecyclerFragment() {
        // Required empty public constructor
    }

    private ResponseReceiver receiver;
    ListViewAdapter adapter;
    ProgressDialog dialog;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        receiver = new ResponseReceiver();
    }

    @Override
    public void onResume() {
        super.onResume();
        IntentFilter filter = new IntentFilter(ResponseReceiver.ACTION_RESP);
        filter.addCategory(Intent.CATEGORY_DEFAULT);
        getActivity().registerReceiver(receiver, filter);
    }

    @Override
    public void onPause() {
        super.onPause();
        getActivity().unregisterReceiver(receiver);
    }

    private void setData(ArrayList<FriendResponseClass> list) {
        adapter.setData(list);
    }

    private ArrayList<String> friendIds;

    public void fetchData(String searchKeyword, ArrayList<String> friendIds) {
        this.friendIds = friendIds;
        PostHelperClass.FriendsQueries.find_friends(searchKeyword, Profile.getCurrentProfile().getId());
        dialog.show();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_search_static_recycler, container, false);
        RecyclerView recyclerView = ((RecyclerView) rootView.findViewById(R.id.search_static_recycler));
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        adapter = new ListViewAdapter();
        recyclerView.setAdapter(adapter);
        recyclerView.setOnTouchListener(new View.OnTouchListener() {
            // Setting on Touch Listener for handling the touch inside ScrollView
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                // Disallow the touch request for parent scroll on touch of child view
                v.getParent().requestDisallowInterceptTouchEvent(true);
                return false;
            }
        });

        dialog = new ProgressDialog(getActivity());
        dialog.setIndeterminate(true);
        dialog.setMessage(getString(R.string.loading));

        return rootView;
    }

    public class ResponseReceiver extends BroadcastReceiver {

        public static final String ACTION_RESP = "ACTION_RESP";

        @Override
        public void onReceive(Context context, Intent intent) {
            dialog.dismiss();
            setData((ArrayList<FriendResponseClass>) intent.getSerializableExtra(ACTION_RESP));
        }
    }

    public class ListViewAdapter extends RecyclerView.Adapter<ListViewAdapter.ListViewHolder> {

        private ArrayList<FriendResponseClass> friendsDataArrayList = new ArrayList<>();

        public ListViewAdapter() {
        }

        public void setData(ArrayList<FriendResponseClass> list) {
            friendsDataArrayList = list;
            notifyDataSetChanged();
        }

        @Override
        public ListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            return new ListViewHolder(LayoutInflater
                    .from(parent.getContext())
                    .inflate(R.layout.search_static_list_item, parent, false));
        }

        @Override
        public int getItemCount() {
            return friendsDataArrayList.size();
        }

        private boolean isFriend(String userId) {
            for (String id : friendIds) {
                if (userId.equals(id)) {
                    return true;
                }
            }
            return false;
        }

        @Override
        public void onBindViewHolder(final ListViewHolder viewHolder, final int position) {

            if (isFriend(friendsDataArrayList.get(position).getUser_id())) {
                viewHolder.addBtn.setText(UNFOLLOW);
            } else {
                viewHolder.addBtn.setText(FOLLOW);
            }

            viewHolder.name.setText(friendsDataArrayList.get(position).getName());

            if (!friendsDataArrayList.get(position).getProfilePicture().equals(Constants.NO_IMAGE)) {
                Picasso.with(SearchStaticRecyclerFragment.this.getActivity())
                        .load(friendsDataArrayList.get(position).getProfilePicture())
                        .fit()
                        .centerCrop()
                        .into(viewHolder.profilePicture);
            } else if (friendsDataArrayList.get(position).getProfilePicture().equals(Constants.NO_IMAGE)) {
                viewHolder.profilePicture.setImageDrawable(SearchStaticRecyclerFragment.this.getActivity().getResources().getDrawable(R.drawable.profile_image));
            }

            viewHolder.addBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (viewHolder.addBtn.getText().toString().equals(FOLLOW)) {
                        showAlertDialogFollow(position, viewHolder);
                    }
                    if (viewHolder.addBtn.getText().toString().equals(UNFOLLOW)) {
                        showAlertDialogUnfollow(position, viewHolder);
                    }
                }
            });
        }

        private final static String FOLLOW = "follow";
        private final static String UNFOLLOW = "unfollow";

        private void showAlertDialogUnfollow(final int position, final ListViewHolder viewHolder) {
            AlertDialog alertDialog = new AlertDialog.Builder(getActivity()).create();
            alertDialog.setMessage(getString(R.string.unfollow) + friendsDataArrayList.get(position).getName());
            alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, getString(R.string.unfollow) + friendsDataArrayList.get(position).getName(), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    String friendUserId = friendsDataArrayList.get(position).getUser_id();
                    String userId = Profile.getCurrentProfile().getId();
                    PostHelperClass.FriendsQueries.unfollow(friendUserId, userId);
                    friendIds.remove(friendsDataArrayList.get(position).getUser_id());
                    viewHolder.addBtn.setText(FOLLOW);
                }
            });

            alertDialog.setButton(DialogInterface.BUTTON_NEGATIVE, getString(R.string.cancel), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    // DO NOTHING
                }
            });
            alertDialog.show();
        }

        private void showAlertDialogFollow(final int position, final ListViewHolder viewHolder) {
            AlertDialog alertDialog = new AlertDialog.Builder(getActivity()).create();
            alertDialog.setMessage(getString(R.string.follow) + friendsDataArrayList.get(position).getName());
            alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, getString(R.string.follow) + friendsDataArrayList.get(position).getName(), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    String friendUserId = friendsDataArrayList.get(position).getUser_id();
                    String userId = Profile.getCurrentProfile().getId();
                    PostHelperClass.FriendsQueries.follow(friendUserId, userId);
                    friendIds.add(friendsDataArrayList.get(position).getUser_id());
                    viewHolder.addBtn.setText(UNFOLLOW);
                }
            });
            alertDialog.setButton(DialogInterface.BUTTON_NEGATIVE, getString(R.string.cancel), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    // DO NOTHING
                }
            });
            alertDialog.show();
        }


        class ListViewHolder extends RecyclerView.ViewHolder {
            private final TextView name;
            private final RoundedImageView profilePicture;
            Button addBtn;

            public ListViewHolder(View itemView) {
                super(itemView);
                profilePicture = (RoundedImageView) itemView.findViewById(R.id.profilePicture);
                name = (TextView) itemView.findViewById(R.id.name);
                addBtn = (Button) itemView.findViewById(R.id.addBtn);
            }
        }
    }
}