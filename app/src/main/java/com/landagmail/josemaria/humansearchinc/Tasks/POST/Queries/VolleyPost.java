package com.landagmail.josemaria.humansearchinc.Tasks.POST.Queries;

import com.google.gson.Gson;
import com.landagmail.josemaria.humansearchinc.Constants;
import com.landagmail.josemaria.humansearchinc.Tasks.POST.POJO.RequestClasses.AnswerFetchRequestClass;
import com.landagmail.josemaria.humansearchinc.Tasks.POST.POJO.RequestClasses.DeleteRequestClass;
import com.landagmail.josemaria.humansearchinc.Tasks.POST.POJO.RequestClasses.FetchRequestClass;
import com.landagmail.josemaria.humansearchinc.Tasks.POST.POJO.RequestClasses.FriendInput;
import com.landagmail.josemaria.humansearchinc.Tasks.POST.POJO.RequestClasses.ProfileRequestClass;
import com.landagmail.josemaria.humansearchinc.Tasks.POST.POJO.RequestClasses.PushRequestClass;
import com.landagmail.josemaria.humansearchinc.Tasks.POST.POJO.RequestClasses.UserSettingsRequestClass;
import com.landagmail.josemaria.humansearchinc.Tasks.POST.POJO.ResponseClasses.FetchResponseClass;
import com.landagmail.josemaria.humansearchinc.Tasks.POST.POJO.ResponseClasses.FollowResponseClass;
import com.landagmail.josemaria.humansearchinc.Tasks.POST.POJO.ResponseClasses.FriendResponseClass;
import com.landagmail.josemaria.humansearchinc.Tasks.POST.POJO.ResponseClasses.MyQuestionFetchResponseClass;
import com.landagmail.josemaria.humansearchinc.Tasks.POST.POJO.ResponseClasses.ProfileResponseClass;
import com.landagmail.josemaria.humansearchinc.Tasks.POST.Queries.Interfaces.CartoDBQueries;
import com.landagmail.josemaria.humansearchinc.Tasks.POST.Queries.Interfaces.FriendsQueries;
import com.landagmail.josemaria.humansearchinc.Tasks.POST.Queries.Interfaces.ProfileQueries;

import org.json.JSONException;

import java.util.ArrayList;

public class VolleyPost extends VolleyPostType {

    public static CartoDBPost getCartoDBClient() {
        return new CartoDBPost();
    }

    public static FriendsPost getFriendsClient() {
        return new FriendsPost();
    }

    public static ProfilePost getProfileClient() {
        return new ProfilePost();
    }

    public static class CartoDBPost implements CartoDBQueries {

        @Override
        public ArrayList<FetchResponseClass> fetch_answers(AnswerFetchRequestClass Input) throws JSONException {
            VolleyPostType.jsonArrayRequest(Constants.FETCH_ANSWERS, new Gson().toJson(Input));
            return null;
        }

        @Override
        public ArrayList<FetchResponseClass> fetch_questions(FetchRequestClass Input) throws JSONException {
            VolleyPostType.jsonArrayRequest(Constants.FETCH_QUESTIONS, new Gson().toJson(Input));
            return null;
        }

        @Override
        public ArrayList<MyQuestionFetchResponseClass> fetch_my_questions(String Input) throws JSONException {
            VolleyPostType.jsonArrayRequest(Constants.FETCH_MY_QUESTIONS, Input);
            return null;
        }

        @Override
        public Integer insert_answers(PushRequestClass Input) throws JSONException {
            VolleyPostType.jsonObjectRequest(Constants.INSERT_ANSWERS, new Gson().toJson(Input));
            return null;
        }

        @Override
        public Integer delete_questions(DeleteRequestClass Input) throws JSONException {
            VolleyPostType.jsonObjectRequest(Constants.DELETE_QUESTIONS, new Gson().toJson(Input));
            return null;
        }

        @Override
        public Integer dissociate_question(DeleteRequestClass Input) throws JSONException {
            VolleyPostType.jsonObjectRequest(Constants.DISSACIATE_QUESTION, new Gson().toJson(Input));
            return null;
        }

        @Override
        public Integer insert_questions(PushRequestClass Input) throws JSONException {
            VolleyPostType.jsonObjectRequest(Constants.INSERT_QUESTIONS, new Gson().toJson(Input));
            return null;
        }

        public void fetch_all_questions() {
            VolleyPostType.getJsonArrayRequest(Constants.FETCH_ALL_QUESTIONS);
        }

        @Override
        public void fetch_hashtag_questions(String hashTag) {
            VolleyPostType.jsonArrayRequest(Constants.FETCH_HASHTAG_QUESTIONS, hashTag);
        }

        @Override
        public void check_for_answers(String input) {
            VolleyPostType.jsonObjectRequest(Constants.CHECK_FOR_ANSWERS, input);
        }

        @Override
        public void check_for_questions(FetchRequestClass Input) throws JSONException {
            VolleyPostType.jsonObjectRequest(Constants.CHECK_FOR_QUESTIONS, new Gson().toJson(Input));
        }
    }

    public static class FriendsPost implements FriendsQueries {

        @Override
        public Integer follow(FriendInput Input) {
            VolleyPostType.jsonObjectRequest(Constants.FOLLOW, new Gson().toJson(Input));
            return null;
        }

        @Override
        public Integer unfollow(FriendInput Input) {
            VolleyPostType.jsonObjectRequest(Constants.UNFOLLOW, new Gson().toJson(Input));
            return null;
        }

        @Override
        public void check_if_friend(FriendInput Input) {
            VolleyPostType.jsonObjectRequest(Constants.CHECK_IF_FRIEND, new Gson().toJson(Input));
        }

        @Override
        public void find_friends(FriendInput Input) {
            VolleyPostType.jsonArrayRequest(Constants.FIND_FRIENDS, new Gson().toJson(Input));
        }
    }

    public static class ProfilePost implements ProfileQueries {

        @Override
        public Integer update_user_settings(UserSettingsRequestClass Input) {
            VolleyPostType.jsonObjectRequest(Constants.UPDATE_USER_SETTINGS, new Gson().toJson(Input));
            return null;
        }

        @Override
        public String profile_check(String Input) {
            VolleyPostType.jsonObjectRequest(Constants.CHECK_PROFILE, Input);
            return null;
        }

        @Override
        public ProfileResponseClass fetch_profile(String Input) {
            VolleyPostType.jsonObjectRequest(Constants.FETCH_PROFILE, Input);
            return null;
        }

        @Override
        public FollowResponseClass profile_follow_fetch(String Input) {
            VolleyPostType.jsonObjectRequest(Constants.PROFILE_FOLLOW_FETCH, Input);
            return null;
        }

        @Override
        public ArrayList<FriendResponseClass> fetch_following(String Input) {
            VolleyPostType.jsonArrayRequest(Constants.FETCH_FOLLOWING, Input);
            return null;
        }

        @Override
        public ArrayList<FriendResponseClass> fetch_followers(String Input) {
            VolleyPostType.jsonArrayRequest(Constants.FETCH_FOLLOWERS, Input);
            return null;
        }

        @Override
        public void fetch_external_profile(String input) {
            VolleyPostType.jsonObjectRequest(Constants.FETCH_EXTERNAl_PROFILE, input);
        }
    }
}
