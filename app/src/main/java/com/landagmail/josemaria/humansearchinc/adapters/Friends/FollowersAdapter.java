package com.landagmail.josemaria.humansearchinc.adapters.Friends;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.landagmail.josemaria.humansearchinc.MainApplication;
import com.landagmail.josemaria.humansearchinc.R;
import com.landagmail.josemaria.humansearchinc.Tasks.POST.POJO.ResponseClasses.FriendResponseClass;
import com.landagmail.josemaria.humansearchinc.Utils.CustomViews.RoundImageView.RoundedImageView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class FollowersAdapter extends RecyclerView.Adapter<FollowersAdapter.myViewHolder> {

    private LayoutInflater layoutInflater;
    private Context context;
    private ArrayList<FriendResponseClass> friendsDataArrayList = new ArrayList<>();

    public FollowersAdapter(Activity activity) {
        layoutInflater = LayoutInflater.from(activity);
        context = activity;
    }

    @Override
    public myViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.followers_screen_list, parent, false);
        myViewHolder viewHolder = new myViewHolder(view);
        viewHolder.setIsRecyclable(false);
        return viewHolder;
    }

    public void setData(ArrayList<FriendResponseClass> list) {
        friendsDataArrayList = list;
        notifyDataSetChanged();
    }

    @Override
    public void onBindViewHolder(final myViewHolder holder, final int position) {

        holder.nameTxt.setText(friendsDataArrayList.get(position).getName());

        if (!friendsDataArrayList.get(position).getProfilePicture().equals("NO_IMAGE")) {
            Picasso.with(context)
                    .load(friendsDataArrayList.get(position).getProfilePicture())
                    .fit()
                    .centerCrop()
                    .into(holder.profilePicture);
        }
    }

    @Override
    public int getItemCount() {
        return friendsDataArrayList.size();
    }

    static class myViewHolder extends RecyclerView.ViewHolder {

        TextView nameTxt;
        RoundedImageView profilePicture;

        public myViewHolder(View itemView) {
            super(itemView);

            nameTxt = (TextView) itemView.findViewById(R.id.nameTxt);
            profilePicture = (RoundedImageView) itemView.findViewById(R.id.profilePicture);

        }
    }
}
