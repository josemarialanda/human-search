package com.landagmail.josemaria.humansearchinc.adapters.Friends;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.facebook.Profile;
import com.landagmail.josemaria.humansearchinc.MainApplication;
import com.landagmail.josemaria.humansearchinc.R;
import com.landagmail.josemaria.humansearchinc.Tasks.POST.POJO.RequestClasses.FriendInput;
import com.landagmail.josemaria.humansearchinc.Tasks.POST.POJO.ResponseClasses.FriendResponseClass;
import com.landagmail.josemaria.humansearchinc.Tasks.POST.Queries.PostHelperClass;
import com.landagmail.josemaria.humansearchinc.Tasks.POST.Queries.VolleyPost;
import com.landagmail.josemaria.humansearchinc.Utils.CustomViews.RoundImageView.RoundedImageView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class FollowingAdapter extends RecyclerView.Adapter<FollowingAdapter.myViewHolder> {
    // FIX ME
    private LayoutInflater layoutInflater;
    private Context context;
    private ArrayList<FriendResponseClass> friendsDataArrayList = new ArrayList<>();

    public FollowingAdapter(Activity activity) {
        layoutInflater = LayoutInflater.from(activity);
        context = activity;
    }

    @Override
    public myViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.following_screen_list, parent, false);
        myViewHolder viewHolder = new myViewHolder(view);
        viewHolder.setIsRecyclable(false);
        return viewHolder;
    }

    private void showAlertDialog(final int position) {
        AlertDialog alertDialog = new AlertDialog.Builder(context).create();
        alertDialog.setMessage(context.getString(R.string.unfollowConfirmation) + friendsDataArrayList.get(position).getName() + "?");
        alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, context.getString(R.string.unfollow), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                PostHelperClass.FriendsQueries.unfollow(friendsDataArrayList.get(position).getUser_id(), Profile.getCurrentProfile().getId());

                friendsDataArrayList.remove(position);
                setData(friendsDataArrayList);
            }
        });
        alertDialog.setButton(DialogInterface.BUTTON_NEGATIVE, context.getString(R.string.cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // DO NOTHING
            }
        });
        alertDialog.show();
    }

    public void setData(ArrayList<FriendResponseClass> list) {
        friendsDataArrayList = list;
        notifyDataSetChanged();
    }

    @Override
    public void onBindViewHolder(final myViewHolder holder, final int position) {

        holder.nameTxt.setText(friendsDataArrayList.get(position).getName());

        holder.deleteBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showAlertDialog(position);
            }
        });

        if (!friendsDataArrayList.get(position).getProfilePicture().equals("NO_IMAGE")) {
            Picasso.with(context)
                    .load(friendsDataArrayList.get(position).getProfilePicture())
                    .fit()
                    .centerCrop()
                    .into(holder.profilePicture);
        }
    }

    @Override
    public int getItemCount() {
        return friendsDataArrayList.size();
    }

    static class myViewHolder extends RecyclerView.ViewHolder {

        TextView nameTxt;
        Button deleteBtn;
        RoundedImageView profilePicture;

        public myViewHolder(View itemView) {
            super(itemView);
            deleteBtn = (Button) itemView.findViewById(R.id.unfollowBtn);
            nameTxt = (TextView) itemView.findViewById(R.id.nameTxt);
            profilePicture = (RoundedImageView) itemView.findViewById(R.id.profilePicture);
        }
    }
}
