package com.landagmail.josemaria.humansearchinc.Tasks.POST.POJO.RequestClasses;

public class DeleteRequestClass {
	
	String userID;
	String questionID;
	
	public DeleteRequestClass(String userID, String questionID) {
		this.userID = userID;
		this.questionID = questionID;
	}
	
	public String getUserID() {
		return userID;
	}
	public void setUserID(String userID) {
		this.userID = userID;
	}
	public String getQuestionID() {
		return questionID;
	}
	public void setQuestionID(String questionID) {
		this.questionID = questionID;
	}

}
