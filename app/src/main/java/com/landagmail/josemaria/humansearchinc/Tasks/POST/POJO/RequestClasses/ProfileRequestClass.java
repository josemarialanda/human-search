package com.landagmail.josemaria.humansearchinc.Tasks.POST.POJO.RequestClasses;

public class ProfileRequestClass {
	
	String userID;
	String pictureUrl;
	String alias;
	
	public ProfileRequestClass(){
		
	}
	
	public ProfileRequestClass(String userID, String pictureUrl, String alias) {
		this.userID = userID;
		this.pictureUrl = pictureUrl;
		this.alias = alias;
	}
	
	public String getUserID() {
		return userID;
	}
	public void setUserID(String userID) {
		this.userID = userID;
	}
	public String getPictureUrl() {
		return pictureUrl;
	}
	public void setPictureUrl(String pictureUrl) {
		this.pictureUrl = pictureUrl;
	}
	public String getAlias() {
		return alias;
	}
	public void setAlias(String alias) {
		this.alias = alias;
	}

}
