package com.landagmail.josemaria.humansearchinc.Activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.facebook.Profile;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.landagmail.josemaria.humansearchinc.Activities.Friends.FollowersActivity;
import com.landagmail.josemaria.humansearchinc.Logging.L;
import com.landagmail.josemaria.humansearchinc.MainApplication;
import com.landagmail.josemaria.humansearchinc.R;
import com.landagmail.josemaria.humansearchinc.Tasks.POST.POJO.ResponseClasses.FetchResponseClass;
import com.landagmail.josemaria.humansearchinc.Tasks.POST.POJO.ResponseClasses.FollowResponseClass;
import com.landagmail.josemaria.humansearchinc.Tasks.POST.POJO.ResponseClasses.ProfileResponseClass;
import com.landagmail.josemaria.humansearchinc.Tasks.POST.Queries.PostHelperClass;
import com.landagmail.josemaria.humansearchinc.Utils.CommonItems;
import com.landagmail.josemaria.humansearchinc.Utils.CustomViews.RoundImageView.RoundedImageView;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.util.ArrayList;

public class ExternalProfileFetch extends AppCompatActivity {

    private ResponseReceiver receiver;
    private RoundedImageView profilePicture;
    private TextView aliasTxt;
    private TextView followingYouNumTxt;
    private String userID;
    private Button followBtn;
    private boolean isFriend = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_external_profile_fetch);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(null);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        final String alias = getIntent().getStringExtra("ALIAS");

        PostHelperClass.ProfileQueries.fetch_external_profile(alias);

        aliasTxt = (TextView) findViewById(R.id.aliasTxt);
        profilePicture = (RoundedImageView) findViewById(R.id.profilePicture);
        followingYouNumTxt = (TextView) findViewById(R.id.followingYouNumTxt);
        followBtn = (Button) findViewById(R.id.followBtn);
        followBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isFriend) {
                    unfollowDialog(alias);
                } else {
                    followDialog(alias);
                }
            }
        });
        followBtn.setClickable(false);

        aliasTxt.setText(alias);
        receiver = new ResponseReceiver();
    }

    private void followDialog(String alias) {
        AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        alertDialog.setMessage(getString(R.string.follow) + alias + " ?");
        alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, getString(R.string.follow) + alias + " ?", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                PostHelperClass.FriendsQueries.follow(userID, Profile.getCurrentProfile().getId());
                followBtn.setText(getString(R.string.unfollow));
                isFriend = true;
            }
        });
        alertDialog.setButton(DialogInterface.BUTTON_NEGATIVE, getString(R.string.cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // DO NOTHING
            }
        });
        alertDialog.show();
    }

    private void unfollowDialog(String alias) {
        AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        alertDialog.setMessage(getString(R.string.unfollow) + alias + " ?");
        alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, getString(R.string.unfollow) + alias + " ?", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                PostHelperClass.FriendsQueries.unfollow(userID, Profile.getCurrentProfile().getId());
                followBtn.setText(getString(R.string.follow));
                isFriend = false;
            }
        });
        alertDialog.setButton(DialogInterface.BUTTON_NEGATIVE, getString(R.string.cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // DO NOTHING
            }
        });
        alertDialog.show();
    }


    @Override
    public void onResume() {
        super.onResume();
        MainApplication.activityResumed();
        IntentFilter filter = new IntentFilter(ResponseReceiver.ACTION_RESP);
        filter.addCategory(Intent.CATEGORY_DEFAULT);
        registerReceiver(receiver, filter);

    }

    @Override
    public void onPause() {
        super.onPause();
        MainApplication.activityPaused();
        unregisterReceiver(receiver);
    }

    public class ResponseReceiver extends BroadcastReceiver {

        public static final String ACTION_RESP = "ACTION_RESP";
        public static final String IS_FRIEND = "IS_FRIEND";
        public static final String FOLLOW_DATA = "FOLLOW_DATA";


        @Override
        public void onReceive(Context context, Intent intent) {

            if (intent.getSerializableExtra(FOLLOW_DATA) != null) {
                FollowResponseClass response = (FollowResponseClass) intent.getSerializableExtra(FOLLOW_DATA);
                followingYouNumTxt.setText(response.getFollowed_by() + "");
            }

            if (intent.getStringExtra(IS_FRIEND) != null) {
                switch (intent.getStringExtra(IS_FRIEND)) {
                    case "200":
                        followBtn.setClickable(true);
                        followBtn.setText(getString(R.string.unfollow));
                        isFriend = true;
                        break;
                    case "0":
                        followBtn.setClickable(true);
                        followBtn.setText(getString(R.string.follow));
                        isFriend = false;
                        break;
                }
            }

            if (intent.getSerializableExtra(ACTION_RESP) != null) {
                final ProfileResponseClass response = (ProfileResponseClass) intent.getSerializableExtra(ACTION_RESP);
                userID = response.getJson();
                Picasso.with(MainApplication.getAppContext())
                        .load(response.getProfile_picture())
                        .into(new Target() {
                            @Override
                            public void onBitmapLoaded(final Bitmap loadedImage, Picasso.LoadedFrom from) {
                                profilePicture.setImageBitmap(loadedImage);
                            }

                            @Override
                            public void onBitmapFailed(Drawable errorDrawable) {

                            }

                            @Override
                            public void onPrepareLoad(Drawable placeHolderDrawable) {

                            }
                        });
                PostHelperClass.ProfileQueries.profile_follow_fetch(userID);
                if (userID.equals(Profile.getCurrentProfile().getId())) {
                    followBtn.setText(R.string.me);
                } else {
                    PostHelperClass.FriendsQueries.check_if_friend(userID, Profile.getCurrentProfile().getId());
                }
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return true;
    }

}
