package com.landagmail.josemaria.humansearchinc.Fragments;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.provider.Settings;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.facebook.Profile;
import com.landagmail.josemaria.humansearchinc.Activities.AskActivity;
import com.landagmail.josemaria.humansearchinc.Animations.TogicLoadingView.TogicLoadingView;
import com.landagmail.josemaria.humansearchinc.Logging.L;
import com.landagmail.josemaria.humansearchinc.MainApplication;
import com.landagmail.josemaria.humansearchinc.R;
import com.landagmail.josemaria.humansearchinc.Tasks.POST.POJO.ResponseClasses.FetchResponseClass;
import com.landagmail.josemaria.humansearchinc.Tasks.POST.Queries.PostHelperClass;
import com.landagmail.josemaria.humansearchinc.Utils.CustomViews.PreCachingLayoutManager;
import com.landagmail.josemaria.humansearchinc.Utils.DeviceUtils;
import com.landagmail.josemaria.humansearchinc.adapters.LocalQuestionsAdapter;

import java.util.ArrayList;

public class LocalQuestionsFragment extends Fragment {

    private static boolean isGettingLocation = true;
    private ResponseReceiver receiver;
    private SwipeRefreshLayout swipeRefreshLayout;
    public RecyclerView questionRecyclerView;
    private LocalQuestionsAdapter adapter;
    FloatingActionButton newQuestionBtn;

    float longitude = 0;
    float latitude = 0;

    public LocalQuestionsFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        receiver = new ResponseReceiver();
    }

    @Override
    public void onResume() {
        super.onResume();
        IntentFilter filter = new IntentFilter(ResponseReceiver.ACTION_RESP);
        filter.addCategory(Intent.CATEGORY_DEFAULT);
        getActivity().registerReceiver(receiver, filter);
    }

    @Override
    public void onPause() {
        super.onPause();
        getActivity().unregisterReceiver(receiver);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_question_feed, container, false);

        newQuestionBtn = (FloatingActionButton) view.findViewById(R.id.newQuestionBtn);
        newQuestionBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), AskActivity.class));
            }
        });

        questionRecyclerView = (RecyclerView) view.findViewById(R.id.questionRecyclerView);

        PreCachingLayoutManager layoutManager = new PreCachingLayoutManager(getActivity());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        layoutManager.setExtraLayoutSpace(DeviceUtils.getScreenHeight(getActivity()));

        questionRecyclerView.setLayoutManager(layoutManager);
        adapter = new LocalQuestionsAdapter(getActivity());
        questionRecyclerView.setAdapter(adapter);

        swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipeRefreshLayout);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (isGettingLocation) {
                    swipeRefreshLayout.setRefreshing(false);
                    L.m(getString(R.string.gettingLocation));
                } else {
                    refreshItems();
                }
            }
        });

        return view;
    }

    private void refreshItems() {
        if (isOnline()) {
            PostHelperClass.CartoDBQueries.fetch_questions(latitude, longitude, Profile.getCurrentProfile().getId());
            swipeRefreshLayout.setRefreshing(true);
        } else {
            Snackbar.make(getActivity().findViewById(android.R.id.content), getString(R.string.noInternetConnection), Snackbar.LENGTH_LONG)
                    .setAction(getString(R.string.connectToInternet), new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            startActivity(new Intent(Settings.ACTION_WIFI_SETTINGS));
                        }
                    })
                            .setActionTextColor(Color.RED)
                            .show();
        }
    }

    private boolean isOnline() {
        ConnectivityManager cm =
                (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }

    private void setListData(ArrayList<FetchResponseClass> resultList) {
        adapter.setData(resultList);
    }

    public class ResponseReceiver extends BroadcastReceiver {

        public static final String ACTION_RESP = "QUESTIONS_FETCHED";
        public static final String INIT = "INIT";
        public static final String QUESTIONS_LIST = "QUESTIONS_LIST";

        @Override
        public void onReceive(Context context, Intent intent) {

            if (intent.getStringExtra(INIT) != null) {

                latitude = (float) MainApplication.latitude;
                longitude = (float) MainApplication.longitude;
                isGettingLocation = false;
                refreshItems();

            } else {

                ArrayList<FetchResponseClass> resultList = (ArrayList<FetchResponseClass>) intent.getSerializableExtra(QUESTIONS_LIST);
                setListData(resultList);

                swipeRefreshLayout.setRefreshing(false);
            }
        }
    }
}
