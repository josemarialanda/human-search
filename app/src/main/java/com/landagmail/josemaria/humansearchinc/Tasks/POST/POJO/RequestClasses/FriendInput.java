package com.landagmail.josemaria.humansearchinc.Tasks.POST.POJO.RequestClasses;

public class FriendInput {
	
	String friendUserId;
	String userId;
	
	public FriendInput() {

	}

	public FriendInput(String friendUserId, String userId) {
		super();
		this.friendUserId = friendUserId;
		this.userId = userId;
	}

	public String getFriendUserId() {
		return friendUserId;
	}

	public void setFriendUserId(String friendUserId) {
		this.friendUserId = friendUserId;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

}
