package com.landagmail.josemaria.humansearchinc.Tasks.POST.Queries;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.facebook.Profile;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.landagmail.josemaria.humansearchinc.Activities.AnswerScreenActivity;
import com.landagmail.josemaria.humansearchinc.Activities.AskActivity;
import com.landagmail.josemaria.humansearchinc.Activities.ExternalProfileFetch;
import com.landagmail.josemaria.humansearchinc.Activities.Friends.FollowersActivity;
import com.landagmail.josemaria.humansearchinc.Activities.Friends.FollowingActivity;
import com.landagmail.josemaria.humansearchinc.Activities.Friends.SearchStaticRecyclerFragment;
import com.landagmail.josemaria.humansearchinc.Activities.InitScreens.SetupActivity;
import com.landagmail.josemaria.humansearchinc.Activities.InitScreens.WelcomeActivity;
import com.landagmail.josemaria.humansearchinc.Activities.ProfileActivity;
import com.landagmail.josemaria.humansearchinc.Constants;
import com.landagmail.josemaria.humansearchinc.Fragments.LocalQuestionsFragment;
import com.landagmail.josemaria.humansearchinc.Fragments.MapFragment;
import com.landagmail.josemaria.humansearchinc.Fragments.MyActivityFragment;
import com.landagmail.josemaria.humansearchinc.Logging.L;
import com.landagmail.josemaria.humansearchinc.MainApplication;
import com.landagmail.josemaria.humansearchinc.POJO.ProfileData;
import com.landagmail.josemaria.humansearchinc.PushNotifications.PushNotifier;
import com.landagmail.josemaria.humansearchinc.R;
import com.landagmail.josemaria.humansearchinc.Tasks.POST.POJO.ResponseClasses.FetchResponseClass;
import com.landagmail.josemaria.humansearchinc.Tasks.POST.POJO.ResponseClasses.FollowResponseClass;
import com.landagmail.josemaria.humansearchinc.Tasks.POST.POJO.ResponseClasses.FriendResponseClass;
import com.landagmail.josemaria.humansearchinc.Tasks.POST.POJO.ResponseClasses.MyQuestionFetchResponseClass;
import com.landagmail.josemaria.humansearchinc.Tasks.POST.POJO.ResponseClasses.ProfileResponseClass;
import com.landagmail.josemaria.humansearchinc.Utils.CommonItems;
import com.landagmail.josemaria.humansearchinc.Utils.Storage.SharedPreferences.ComplexPreferences;
import com.landagmail.josemaria.humansearchinc.Utils.Storage.SharedPreferences.TinyDB;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;
import com.urbanairship.UAirship;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class VolleyPostType {

    private final static String NAME_PREFERENCE = "NAME_PREFERENCE";
    private final static String USERNAME = "USERNAME";

    public static void getJsonArrayRequest(final String API_ENDPOINT) {
        JsonArrayRequest request = new JsonArrayRequest(API_ENDPOINT, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                switch (API_ENDPOINT) {
                    case Constants.FETCH_ALL_QUESTIONS:
                        sendResults(Constants.FETCH_ALL_QUESTIONS, response.toString());
                        break;
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                try {
                    Log.i("info", error.getLocalizedMessage());
                } catch (Exception e) {
                    Log.i("info", "Couldn't print error message");
                }
            }
        });

        request.setRetryPolicy(new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        MainApplication.getInstance().addToRequestQueue(request);
    }

    public static void jsonArrayRequest(final String API_ENDPOINT, String requestString) {

        JsonArrayRequest request = new JsonArrayRequest(Request.Method.POST, API_ENDPOINT, requestString, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                switch (API_ENDPOINT) {
                    case Constants.FETCH_ANSWERS:
                        sendResults(Constants.FETCH_ANSWERS, response.toString());
                        break;
                    case Constants.FETCH_QUESTIONS:
                        sendResults(Constants.FETCH_QUESTIONS, response.toString());
                        break;
                    case Constants.FETCH_MY_QUESTIONS:
                        sendResults(Constants.FETCH_MY_QUESTIONS, response.toString());
                        break;
                    case Constants.FETCH_FOLLOWING:
                        sendResults(Constants.FETCH_FOLLOWING, response.toString());
                        break;
                    case Constants.FETCH_FOLLOWERS:
                        sendResults(Constants.FETCH_FOLLOWERS, response.toString());
                        break;
                    case Constants.FETCH_HASHTAG_QUESTIONS:
                        sendResults(Constants.FETCH_HASHTAG_QUESTIONS, response.toString());
                        break;
                    case Constants.FIND_FRIENDS:
                        sendResults(Constants.FIND_FRIENDS, response.toString());
                        break;
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                try {
                    Log.i("info", error.getLocalizedMessage());
                } catch (Exception e) {
                    Log.i("info", "Couldn't print error message");
                }
            }
        });

        request.setRetryPolicy(new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        MainApplication.getInstance().addToRequestQueue(request);
    }

    public static void jsonObjectRequest(final String API_ENDPOINT, String requestString) {

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, API_ENDPOINT, requestString, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                switch (API_ENDPOINT) {
                    case Constants.CHECK_FOR_ANSWERS:
                        try {
                            sendResults(Constants.CHECK_FOR_ANSWERS, response.getString("response"));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;
                    case Constants.CHECK_FOR_QUESTIONS:
                        try {
                            sendResults(Constants.CHECK_FOR_QUESTIONS, response.getString("response"));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;
                    case Constants.FETCH_EXTERNAl_PROFILE:
                        sendResults(Constants.FETCH_EXTERNAl_PROFILE, response.toString());
                        break;
                    case Constants.FETCH_PROFILE:
                        sendResults(Constants.FETCH_PROFILE, response.toString());
                        break;
                    case Constants.PROFILE_FOLLOW_FETCH:
                        sendResults(Constants.PROFILE_FOLLOW_FETCH, response.toString());
                        break;
                    case Constants.INSERT_QUESTIONS:
                        try {
                            sendResults(Constants.INSERT_QUESTIONS, response.getString("response"));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;
                    case Constants.INSERT_ANSWERS:
                        try {
                            sendResults(Constants.INSERT_ANSWERS, response.getString("response"));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;
                    case Constants.DELETE_QUESTIONS:
                        try {
                            sendResults(Constants.DELETE_QUESTIONS, response.getString("response"));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;
                    case Constants.DISSACIATE_QUESTION:
                        try {
                            sendResults(Constants.DISSACIATE_QUESTION, response.getString("response"));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;
                    case Constants.FOLLOW:
                        try {
                            sendResults(Constants.FOLLOW, response.getString("response"));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;
                    case Constants.UNFOLLOW:
                        try {
                            sendResults(Constants.UNFOLLOW, response.getString("response"));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;
                    case Constants.CHECK_IF_FRIEND:
                        try {
                            sendResults(Constants.CHECK_IF_FRIEND, response.getString("response"));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;
                    case Constants.UPDATE_PROFILE:
                        try {
                            sendResults(Constants.UPDATE_PROFILE, response.getString("response"));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;
                    case Constants.UPDATE_USER_SETTINGS:
                        try {
                            sendResults(Constants.UPDATE_USER_SETTINGS, response.getString("response"));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;
                    case Constants.CHECK_PROFILE:
                        try {
                            sendResults(Constants.CHECK_PROFILE, response.getString("response"));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.getMessage();
            }
        });

        request.setRetryPolicy(new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        MainApplication.getInstance().addToRequestQueue(request);
    }

    public static void sendResults(String result, String data) {

        ArrayList<FriendResponseClass> friendResponse;
        ArrayList<FetchResponseClass> fetch_general_response_class;
        final Intent intent;

        switch (result) {
            case Constants.CHECK_FOR_ANSWERS:
                switch (Integer.parseInt(data)) {
                    case Constants.ANSWERS_PRESENT:
                        PushNotifier.sendShortPush(MainApplication.getAppContext().getString(R.string.answerNotification));
                        break;
                    case Constants.ANSWERS_NOT_PRESENT:
                }
                break;
            case Constants.CHECK_FOR_QUESTIONS:
                switch (Integer.parseInt(data)) {
                    case Constants.QUESTIONS_PRESENT:
                        PushNotifier.sendShortPush(MainApplication.getAppContext().getString(R.string.newQuestionNotification));
                        break;
                    case Constants.QUESTIONS_NOT_PRESENT:
                }
                break;

            case Constants.FIND_FRIENDS:
                friendResponse = new Gson().fromJson(data, new TypeToken<ArrayList<FriendResponseClass>>() {
                }.getType());

                intent = new Intent(SearchStaticRecyclerFragment.ResponseReceiver.ACTION_RESP);
                intent.putExtra(SearchStaticRecyclerFragment.ResponseReceiver.ACTION_RESP, friendResponse);
                MainApplication.getAppContext().sendBroadcast(intent);

                break;
            case Constants.FETCH_HASHTAG_QUESTIONS:

                fetch_general_response_class = new Gson().fromJson(data, new TypeToken<ArrayList<FetchResponseClass>>() {
                }.getType());
                intent = new Intent(MapFragment.ResponseReceiver.ACTION_RESP);
                intent.putExtra(MapFragment.ResponseReceiver.ALL_QUESTIONS_FETCHED, fetch_general_response_class);
                MainApplication.getAppContext().sendBroadcast(intent);

                break;
            case Constants.FETCH_ALL_QUESTIONS:

                fetch_general_response_class = new Gson().fromJson(data, new TypeToken<ArrayList<FetchResponseClass>>() {
                }.getType());
                intent = new Intent(MapFragment.ResponseReceiver.ACTION_RESP);
                intent.putExtra(MapFragment.ResponseReceiver.ALL_QUESTIONS_FETCHED, fetch_general_response_class);
                MainApplication.getAppContext().sendBroadcast(intent);

                break;
            case Constants.FETCH_ANSWERS:

                fetch_general_response_class = new Gson().fromJson(data, new TypeToken<ArrayList<FetchResponseClass>>() {
                }.getType());
                intent = new Intent(AnswerScreenActivity.ResponseReceiver.ACTION_RESP);
                intent.putExtra(AnswerScreenActivity.ResponseReceiver.QUESTIONS_LIST, fetch_general_response_class);
                MainApplication.getAppContext().sendBroadcast(intent);

                break;
            case Constants.FETCH_QUESTIONS:

                fetch_general_response_class = new Gson().fromJson(data, new TypeToken<ArrayList<FetchResponseClass>>() {
                }.getType());
                intent = new Intent(LocalQuestionsFragment.ResponseReceiver.ACTION_RESP);
                intent.putExtra(LocalQuestionsFragment.ResponseReceiver.QUESTIONS_LIST, fetch_general_response_class);
                MainApplication.getAppContext().sendBroadcast(intent);

                break;
            case Constants.FETCH_MY_QUESTIONS:

                ArrayList<MyQuestionFetchResponseClass> myQuestions = new Gson().fromJson(data, new TypeToken<ArrayList<MyQuestionFetchResponseClass>>() {
                }.getType());
                intent = new Intent(MyActivityFragment.ResponseReceiver.ACTION_RESP);
                intent.putExtra("OUT_SEND", "questionList");
                intent.putExtra("QUESTIONS_LIST", myQuestions);
                MainApplication.getAppContext().sendBroadcast(intent);

                break;
            case Constants.FETCH_FOLLOWING:

                friendResponse = new Gson().fromJson(data, new TypeToken<ArrayList<FriendResponseClass>>() {
                }.getType());

                if (MainApplication.isFriendsActivityVisible()) {
                    intent = new Intent(FollowingActivity.ResponseReceiver.ACTION_RESP);
                    intent.putExtra("FRIENDS_DATA", friendResponse);
                    MainApplication.getAppContext().sendBroadcast(intent);
                }

                break;
            case Constants.FETCH_FOLLOWERS:

                friendResponse = new Gson().fromJson(data, new TypeToken<ArrayList<FriendResponseClass>>() {
                }.getType());
                intent = new Intent(FollowersActivity.ResponseReceiver.ACTION_RESP);
                intent.putExtra("FRIENDS_DATA", friendResponse);
                MainApplication.getAppContext().sendBroadcast(intent);

                break;
            case Constants.INSERT_ANSWERS:

                switch (Integer.parseInt(data)) {
                    case 2:
                        L.m(MainApplication.getAppContext().getString(R.string.answerUploadSuccess));

                        intent = new Intent(AnswerScreenActivity.ResponseReceiver.ACTION_RESP);
                        intent.putExtra(AnswerScreenActivity.ResponseReceiver.ANSWER_PUSHED, Integer.parseInt(data));
                        MainApplication.getAppContext().sendBroadcast(intent);

                        break;
                    case -2:
                        L.m(MainApplication.getAppContext().getString(R.string.errorMessage));
                }

                break;
            case Constants.DELETE_QUESTIONS:

                switch (Integer.parseInt(data)) {
                    case 2:
                        L.m(MainApplication.getAppContext().getString(R.string.questionDeleted));
                        break;
                    case -2:
                        L.m(MainApplication.getAppContext().getString(R.string.errorMessage));
                }

                break;
            case Constants.DISSACIATE_QUESTION:

                switch (Integer.parseInt(data)) {
                    case 2:
                        L.m(MainApplication.getAppContext().getString(R.string.unfollowQuestion));
                        break;
                    case -2:
                        L.m(MainApplication.getAppContext().getString(R.string.errorMessage));
                }

                break;
            case Constants.INSERT_QUESTIONS:

                int numResponse;

                try {
                    numResponse = Integer.parseInt(data);
                } catch (NumberFormatException e) {
                    e.getMessage();
                    numResponse = 2;
                }

                if (numResponse == -2) {
                    L.m(MainApplication.getAppContext().getString(R.string.errorMessage));
                } else {
                    L.m(MainApplication.getAppContext().getString(R.string.questionUploadSuccess));
                }
                break;
            case Constants.FOLLOW:
                switch (Integer.parseInt(data)) {
                    case 2:
                        L.m(MainApplication.getAppContext().getString(R.string.following));
                        break;
                    case -2:
                        L.m(MainApplication.getAppContext().getString(R.string.errorMessage));
                }

                break;
            case Constants.UNFOLLOW:

                switch (Integer.parseInt(data)) {
                    case 2:
                        L.m(MainApplication.getAppContext().getString(R.string.unfollowing));
                        break;
                    case -2:
                        L.m(MainApplication.getAppContext().getString(R.string.errorMessage));
                }

                break;
            case Constants.CHECK_IF_FRIEND:

                switch (Integer.parseInt(data)) {
                    case 200:
                        intent = new Intent(ExternalProfileFetch.ResponseReceiver.ACTION_RESP);
                        intent.putExtra(ExternalProfileFetch.ResponseReceiver.IS_FRIEND, "200");
                        MainApplication.getAppContext().sendBroadcast(intent);
                        break;
                    default:
                        intent = new Intent(ExternalProfileFetch.ResponseReceiver.ACTION_RESP);
                        intent.putExtra(ExternalProfileFetch.ResponseReceiver.IS_FRIEND, "0");
                        MainApplication.getAppContext().sendBroadcast(intent);
                        break;
                }

                break;
            case Constants.UPDATE_PROFILE:

                switch (Integer.parseInt(data)) {
                    case Constants.PHOTO_UPLOAD_OR_NO_CHANGE:
                        intent = new Intent(ProfileActivity.ResponseReceiver.ACTION_RESP);
                        intent.putExtra("UPDATE_INFO", Constants.PHOTO_UPLOAD_OR_NO_CHANGE_STRING);
                        MainApplication.getAppContext().sendBroadcast(intent);
                        break;
                    case Constants.USERNAME_IN_USE:
                        Boolean firstTimeUser = MainApplication.getAppContext().getSharedPreferences("PREFERENCE", Context.MODE_PRIVATE)
                                .getBoolean("firstTimeUser", true);

                        if (firstTimeUser) {
                            intent = new Intent(MainApplication.getAppContext(), SetupActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            MainApplication.getAppContext().startActivity(intent);
                        } else {
                            intent = new Intent(ProfileActivity.ResponseReceiver.ACTION_RESP);
                            intent.putExtra("UPDATE_INFO", Constants.USERNAME_IN_USE_STRING);
                            MainApplication.getAppContext().sendBroadcast(intent);
                        }

                        break;
                    case Constants.PHOTO_AND_OR_USERNAME_UPDATED:

                        intent = new Intent(ProfileActivity.ResponseReceiver.ACTION_RESP);
                        intent.putExtra("UPDATE_INFO", Constants.PHOTO_AND_OR_USERNAME_UPDATED_STRING);
                        MainApplication.getAppContext()
                                .sendBroadcast(intent);
                        break;
                    case Constants.NEW_USER_ADDED:
                        MainApplication.getAppContext().getSharedPreferences("PREFERENCE", Context.MODE_PRIVATE).edit()
                                .putBoolean("firstTimeUser", false).apply();
                        break;
                    default:
                        break;
                }

                break;
            case Constants.CHECK_PROFILE:

                MainApplication.getAppContext().getSharedPreferences("PREFERENCE", Context.MODE_PRIVATE).edit()
                        .putBoolean("isFirstRun", false).commit();

                switch (data) {
                    case Constants.NEW_ACCOUNT:

                        intent = new Intent(WelcomeActivity.ResponseReceiver.ACTION_RESP);
                        intent.putExtra("ACCOUNT_INFO", 1);
                        MainApplication.getAppContext().sendBroadcast(intent);

                        break;
                    case Constants.OLD_USER:
                        L.m(MainApplication.getAppContext().getString(R.string.welcomeMsg));

                        intent = new Intent(WelcomeActivity.ResponseReceiver.ACTION_RESP);
                        intent.putExtra("ACCOUNT_INFO", 2);
                        MainApplication.getAppContext().sendBroadcast(intent);

                        break;
                    default:
                        break;
                }

                break;
            case Constants.FETCH_EXTERNAl_PROFILE:
                final ProfileResponseClass externalProfileResponse = new Gson().fromJson(data, new TypeToken<ProfileResponseClass>() {
                }.getType());

                intent = new Intent(ExternalProfileFetch.ResponseReceiver.ACTION_RESP);
                intent.putExtra(ExternalProfileFetch.ResponseReceiver.ACTION_RESP, externalProfileResponse);
                MainApplication.getAppContext().sendBroadcast(intent);

                break;
            case Constants.FETCH_PROFILE:

                final ProfileResponseClass response = new Gson().fromJson(data, new TypeToken<ProfileResponseClass>() {
                }.getType());

                Boolean isPictureAndNameSet = MainApplication.getAppContext().getSharedPreferences("PREFERENCE", Context.MODE_PRIVATE)
                        .getBoolean("isPictureAndNameSet", false);

                if (!isPictureAndNameSet) {

                    CommonItems.setProfilePictureUrl(response.getProfile_picture());
                    CommonItems.setName(response.getAlias());

                    MainApplication.getAppContext().getSharedPreferences("PREFERENCE", Context.MODE_PRIVATE).edit()
                            .putBoolean("isPictureAndNameSet", true).commit();

                    if (response.getJson() != null) {
                        try {
                            UAirship.shared().getPushManager().setUserNotificationsEnabled(new JSONObject(response.getJson()).getBoolean("enableNotifications"));

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                } else {

                    intent = new Intent(ProfileActivity.ResponseReceiver.ACTION_RESP);
                    intent.putExtra("PROFILE_RESPONSE_DATA", response);
                    MainApplication.getAppContext().sendBroadcast(intent);
                }

                break;
            case Constants.PROFILE_FOLLOW_FETCH:

                FollowResponseClass followResponseClass = new Gson().fromJson(data, new TypeToken<FollowResponseClass>() {
                }.getType());

                if (MainApplication.isExternalProfileFetchActivityVisible()) {
                    intent = new Intent(ExternalProfileFetch.ResponseReceiver.ACTION_RESP);
                    intent.putExtra("FOLLOW_DATA", followResponseClass);
                    MainApplication.getAppContext().sendBroadcast(intent);
                } else {
                    intent = new Intent(ProfileActivity.ResponseReceiver.ACTION_RESP);
                    intent.putExtra("FOLLOW_DATA", followResponseClass);
                    MainApplication.getAppContext().sendBroadcast(intent);
                }

                break;
            case Constants.UPDATE_USER_SETTINGS:

                switch (Integer.parseInt(data)) {
                    case -2:
                        L.m(MainApplication.getAppContext().getString(R.string.errorMessage));
                        break;
                    case 2:
                        intent = new Intent(ProfileActivity.ResponseReceiver.ACTION_RESP);
                        intent.putExtra("UPDATE_INFO", Constants.USER_SETTINGS_UPDATED_STRING);
                        MainApplication.getAppContext().sendBroadcast(intent);
                        break;
                }
                break;
        }
    }
}
