package com.landagmail.josemaria.humansearchinc.Tasks.POST.POJO.ResponseClasses;

import java.io.Serializable;

public class ProfileResponseClass implements Serializable{
	
	String profile_picture;
	String alias;
	String json;

	public ProfileResponseClass(String profile_picture, String alias, String json) {
		this.profile_picture = profile_picture;
		this.alias = alias;
		this.json = json;
	}

	public String getJson() {
		return json;
	}

	public void setJson(String json) {
		this.json = json;
	}

	public String getProfile_picture() {
		return profile_picture;
	}

	public void setProfile_picture(String profile_picture) {
		this.profile_picture = profile_picture;
	}

	public String getAlias() {
		return alias;
	}

	public void setAlias(String alias) {
		this.alias = alias;
	}

}
