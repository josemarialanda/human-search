package com.landagmail.josemaria.humansearchinc.Tasks.POST.Queries;

import android.os.AsyncTask;
import android.util.Log;

import com.amazonaws.auth.CognitoCachingCredentialsProvider;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.AmazonS3Exception;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.landagmail.josemaria.humansearchinc.AWSUtils.CredentialsProvider;
import com.landagmail.josemaria.humansearchinc.Constants;
import com.landagmail.josemaria.humansearchinc.MainApplication;
import com.landagmail.josemaria.humansearchinc.Tasks.POST.POJO.RequestClasses.AnswerFetchRequestClass;
import com.landagmail.josemaria.humansearchinc.Tasks.POST.POJO.RequestClasses.DeleteRequestClass;
import com.landagmail.josemaria.humansearchinc.Tasks.POST.POJO.RequestClasses.FetchRequestClass;
import com.landagmail.josemaria.humansearchinc.Tasks.POST.POJO.RequestClasses.FriendInput;
import com.landagmail.josemaria.humansearchinc.Tasks.POST.POJO.RequestClasses.ProfileRequestClass;
import com.landagmail.josemaria.humansearchinc.Tasks.POST.POJO.RequestClasses.PushRequestClass;
import com.landagmail.josemaria.humansearchinc.Tasks.POST.POJO.RequestClasses.UserSettingsRequestClass;
import com.landagmail.josemaria.humansearchinc.Utils.CommonItems;

import org.json.JSONException;

import java.io.File;
import java.util.List;

public class PostHelperClass {

    static String pictureUrl;

    public static class CartoDBQueries {

        public static void check_for_answers(String userID) {
            VolleyPost.getCartoDBClient().check_for_answers(userID);
        }

        public static void check_for_questions(String userID, double latitude, double longitude) {
            try {
                VolleyPost.getCartoDBClient().check_for_questions(new FetchRequestClass(latitude, longitude, userID));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        public static void dissociate_question(String userID, String questionID) {
            try {
                VolleyPost.getCartoDBClient().dissociate_question(new DeleteRequestClass(userID, questionID));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        public static void fetch_answers(double latitude, double longitude, String questionID) {
            try {
                VolleyPost.getCartoDBClient().fetch_answers(new AnswerFetchRequestClass(latitude, longitude, questionID));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        public static void delete_questions(String userID, String questionID) {
            try {
                VolleyPost.getCartoDBClient().delete_questions(new DeleteRequestClass(userID, questionID));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        public static void fetch_questions(double latitude, double longitude, String userID) {
            try {
                VolleyPost.getCartoDBClient().fetch_questions(new FetchRequestClass(latitude, longitude, userID));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        public static void fetch_my_questions(String userID) {
            try {
                VolleyPost.getCartoDBClient().fetch_my_questions(userID);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        public static void insert_answers(double latitude, double longitude, String userID,
                                          String text, String questionID, String imageURL, String namedUser) {

            String name = CommonItems.getName();
            pictureUrl = CommonItems.getProfilePictureUrl();

            if (imageURL != null) {
                new S3ImageUploadTask(Constants.INSERT_ANSWERS, latitude, longitude, name, text, questionID, userID, 0, namedUser, null, null).execute(new File(imageURL));

            } else {
                PushRequestClass input = new PushRequestClass(latitude, longitude, name, text, questionID, userID, null, namedUser, 0, pictureUrl, null, null);
                try {
                    VolleyPost.getCartoDBClient().insert_answers(input);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }

        public static void insert_questions(double latitude, double longitude,
                                            String text, String questionID, String userID, String imageURL, int radius, List<String> targetUsers, List<String> hashtags) {

            String name = CommonItems.getName();
            pictureUrl = CommonItems.getProfilePictureUrl();

            if (imageURL != null) {
                new S3ImageUploadTask(Constants.INSERT_QUESTIONS, latitude, longitude, name, text, questionID, userID, radius, null, targetUsers, hashtags).execute(new File(imageURL));

            } else {
                PushRequestClass input = new PushRequestClass(latitude, longitude, name, text, questionID, userID, null, null, radius, pictureUrl, targetUsers, hashtags);
                try {
                    VolleyPost.getCartoDBClient().insert_questions(input);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }

        public static void fetch_all_questions() {
            VolleyPost.getCartoDBClient().fetch_all_questions();
        }

        public static void fetch_hashtag_questions(String hashTag) {
            VolleyPost.getCartoDBClient().fetch_hashtag_questions(hashTag);
        }
    }

    public static class ProfileQueries {

        public static void update_user_settings(String userID, String json) {
            VolleyPost.getProfileClient().update_user_settings(new UserSettingsRequestClass(userID, json));
        }

        public static void profile_check(String userID) {
            VolleyPost.getProfileClient().profile_check(userID);
        }

        public static void fetch_profile(String userID) {
            VolleyPost.getProfileClient().fetch_profile(userID);
        }

        public static void fetch_external_profile(String alias) {
            VolleyPost.getProfileClient().fetch_external_profile(alias);
        }

        public static void profile_follow_fetch(String userID) {
            VolleyPost.getProfileClient().profile_follow_fetch(userID);
        }

        public static void fetch_following(String userID) {
            VolleyPost.getProfileClient().fetch_following(userID);
        }

        public static void fetch_followers(String userID) {
            VolleyPost.getProfileClient().fetch_followers(userID);
        }
    }

    public static class FriendsQueries {

        public static void follow(String friendUserId, String userId) {
            VolleyPost.getFriendsClient().follow(new FriendInput(friendUserId, userId));
        }

        public static void unfollow(String friendUserId, String userId) {
            VolleyPost.getFriendsClient().unfollow(new FriendInput(friendUserId, userId));
        }

        public static void check_if_friend(String friendUserId, String userId) {
            VolleyPost.getFriendsClient().check_if_friend(new FriendInput(friendUserId, userId));
        }

        public static void find_friends(String name, String userId) {
            VolleyPost.getFriendsClient().find_friends(new FriendInput(name, userId));
        }

    }

    public static class S3ImageUploadTask extends AsyncTask<File, String, String> {

        String TYPE;
        double latitude;
        double longitude;
        String name;
        String text;
        String questionID;
        String userID;
        int radius;
        String namedUser;
        List<String> targetUsers;
        List<String> allHashTags;

        public S3ImageUploadTask(String TYPE, double latitude, double longitude, String name, String text, String questionID, String userID, int radius, String namedUser, List<String> targetUsers, List<String> allHashTags) {
            this.TYPE = TYPE;
            this.latitude = latitude;
            this.longitude = longitude;
            this.name = name;
            this.text = text;
            this.questionID = questionID;
            this.userID = userID;
            this.radius = radius;
            this.namedUser = namedUser;
            this.targetUsers = targetUsers;
            this.allHashTags = allHashTags;
        }

        @Override
        protected String doInBackground(File... params) {

            CognitoCachingCredentialsProvider credentialsProvider = CredentialsProvider.getCredentials(MainApplication.getAppContext());
            AmazonS3Client s3Client = new AmazonS3Client(credentialsProvider);

            try {
                PutObjectRequest putRequest = new PutObjectRequest("human.search.images", params[0].getName(), params[0]).withCannedAcl(CannedAccessControlList.PublicRead);
                s3Client.putObject(putRequest);
                return "https://s3-us-west-2.amazonaws.com/human.search.images/" + params[0].getName();

            } catch (AmazonS3Exception e) {
                Log.i("ERROR", e.getErrorMessage());
                return e.getErrorMessage();
            }
        }

        @Override
        protected void onPostExecute(String imageUrl) {
            super.onPostExecute(imageUrl);

            pictureUrl = CommonItems.getProfilePictureUrl();

            switch (TYPE) {
                case Constants.INSERT_QUESTIONS:
                    try {
                        VolleyPost.getCartoDBClient().insert_questions(new PushRequestClass(latitude, longitude, name, text, questionID, userID, imageUrl, null, radius, pictureUrl, targetUsers, allHashTags));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;
                case Constants.INSERT_ANSWERS:
                    try {
                        VolleyPost.getCartoDBClient().insert_answers(new PushRequestClass(latitude, longitude, name, text, questionID, userID, imageUrl, namedUser, radius, pictureUrl, targetUsers, allHashTags));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;
            }

        }
    }
}
