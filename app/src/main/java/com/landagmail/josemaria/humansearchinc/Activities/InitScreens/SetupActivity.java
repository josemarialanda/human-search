package com.landagmail.josemaria.humansearchinc.Activities.InitScreens;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.facebook.Profile;
import com.facebook.login.LoginManager;
import com.landagmail.josemaria.humansearchinc.Camera.CameraActivity;
import com.landagmail.josemaria.humansearchinc.Logging.L;
import com.landagmail.josemaria.humansearchinc.MainApplication;
import com.landagmail.josemaria.humansearchinc.R;
import com.landagmail.josemaria.humansearchinc.Services.UpdateProfileService;
import com.landagmail.josemaria.humansearchinc.Tasks.POST.Queries.PostHelperClass;
import com.landagmail.josemaria.humansearchinc.Utils.CustomViews.RoundImageView.RoundedImageView;
import com.landagmail.josemaria.humansearchinc.Utils.UrbanAirshipUtils.DeviceAdressing;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SetupActivity extends AppCompatActivity {

    TextInputLayout textInputLayout;
    Button acceptBtn;
    public RoundedImageView profilePicture;
    private static final int REQUEST_CAMERA = 0;
    public static final int CAMERA_REQUEST_CODE = 1999;
    private String filePath = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setup);

        textInputLayout = (TextInputLayout) findViewById(R.id.usernameTxt);

        profilePicture = (RoundedImageView) findViewById(R.id.profilePircture);
        profilePicture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // (FUTURE) CAN ALSO CHOOSE FROM GALLERY
                requestForCameraPermission();
            }
        });

        acceptBtn = (Button) findViewById(R.id.acceptBtn);

        acceptBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (textInputLayout.getEditText().getText() != null && !textInputLayout.getEditText().getText().toString().equals("")) {
                    if (textInputLayout.getEditText().getText().toString().length() > 3) {

                        Pattern p = Pattern.compile("[^a-z0-9]");
                        Matcher m = p.matcher(textInputLayout.getEditText().getText().toString());
                        boolean b = m.find();

                        if (b) {
                            L.m(getString(R.string.setNameError));
                        } else {

                            Intent intentService;

                            if (!getFilePath().equals("")) {

                                intentService = new Intent(SetupActivity.this, UpdateProfileService.class);
                                intentService.putExtra("userID", Profile.getCurrentProfile().getId());
                                intentService.putExtra("pictureURL", getFilePath());
                                intentService.putExtra("alias", textInputLayout.getEditText().getText().toString());
                                intentService.putExtra("newPicture", true);
                                startService(intentService);

                                SetupActivity.this.finish();

                            } else {

                                intentService = new Intent(SetupActivity.this, UpdateProfileService.class);
                                intentService.putExtra("userID", Profile.getCurrentProfile().getId());
                                intentService.putExtra("pictureURL", "NO_IMAGE");
                                intentService.putExtra("alias", textInputLayout.getEditText().getText().toString());
                                intentService.putExtra("newPicture", false);
                                startService(intentService);

                                SetupActivity.this.finish();
                            }
                        }
                    } else {
                        L.m(getString(R.string.setNameError2));
                    }
                } else {
                    L.m(getString(R.string.setNameError3));
                }
            }
        });
    }

    public void requestForCameraPermission() {
        final String permission = Manifest.permission.CAMERA;
        if (ContextCompat.checkSelfPermission(this, permission)
                != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, permission)) {
                // Show permission rationale
            } else {
                // Handle the result in Activity#onRequestPermissionResult(int, String[], int[])
                ActivityCompat.requestPermissions(this, new String[]{permission}, CAMERA_REQUEST_CODE);
            }
        } else {
            Intent startCustomCameraIntent = new Intent(this, CameraActivity.class);
            startActivityForResult(startCustomCameraIntent, REQUEST_CAMERA);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode != RESULT_OK) return;

        if (requestCode == REQUEST_CAMERA) {
            filePath = getRealPathFromURI(String.valueOf(data.getData()));
            profilePicture.setImageBitmap(decodeFile(filePath));

        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private String getRealPathFromURI(String contentURI) {
        Uri contentUri = Uri.parse(contentURI);
        Cursor cursor = getContentResolver().query(contentUri, null, null, null, null);
        if (cursor == null) {
            return contentUri.getPath();
        } else {
            cursor.moveToFirst();
            int index = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            return cursor.getString(index);
        }
    }

    public Bitmap decodeFile(String path) {
        try {
            // Decode image size
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            BitmapFactory.decodeFile(path, o);
            // The new size we want to scale to
            final int REQUIRED_SIZE = 500;

            // Find the correct scale value. It should be the power of 2.
            int scale = 1;
            while (o.outWidth / scale / 2 >= REQUIRED_SIZE && o.outHeight / scale / 2 >= REQUIRED_SIZE)
                scale *= 2;

            // Decode with inSampleSize
            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize = scale;
            return BitmapFactory.decodeFile(path, o2);
        } catch (Throwable e) {
            e.printStackTrace();
        }
        return null;
    }

    public String getSelectedImagePath() {
        return "";
    }

    public String getFilePath() {
        return filePath;
    }

    public void goBack(View view) {
        Intent intent = new Intent(SetupActivity.this, WelcomeActivity.class);
        startActivity(intent);
        finish();
        LoginManager.getInstance().logOut();
        DeviceAdressing.NamedUsers.disassociateChannelFromNamedUserID();
        MainApplication.getAppContext().getSharedPreferences("PREFERENCE", Context.MODE_PRIVATE).edit()
                .putBoolean("isFirstRun", true).apply();
    }
}
