package com.landagmail.josemaria.humansearchinc.Activities;

import android.Manifest;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Point;
import android.location.Address;
import android.location.Geocoder;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.Layout;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.MultiAutoCompleteTextView;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.facebook.Profile;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.karumi.dexter.listener.single.PermissionListener;
import com.landagmail.josemaria.humansearchinc.Activities.Friends.FollowersActivity;
import com.landagmail.josemaria.humansearchinc.Activities.Friends.FollowingActivity;
import com.landagmail.josemaria.humansearchinc.Activities.Friends.SearchStaticRecyclerFragment;
import com.landagmail.josemaria.humansearchinc.Activities.InitScreens.SetupActivity;
import com.landagmail.josemaria.humansearchinc.Activities.InitScreens.WelcomeActivity;
import com.landagmail.josemaria.humansearchinc.Camera.CameraActivity;
import com.landagmail.josemaria.humansearchinc.Camera.ImageUtility;
import com.landagmail.josemaria.humansearchinc.Constants;
import com.landagmail.josemaria.humansearchinc.Fragments.LocalQuestionsFragment;
import com.landagmail.josemaria.humansearchinc.Fragments.MapFragment;
import com.landagmail.josemaria.humansearchinc.Fragments.MyActivityFragment;
import com.landagmail.josemaria.humansearchinc.Logging.L;
import com.landagmail.josemaria.humansearchinc.MainApplication;
import com.landagmail.josemaria.humansearchinc.PushNotifications.PushNotifier;
import com.landagmail.josemaria.humansearchinc.R;
import com.landagmail.josemaria.humansearchinc.Tasks.POST.POJO.ResponseClasses.FetchResponseClass;
import com.landagmail.josemaria.humansearchinc.Tasks.POST.POJO.ResponseClasses.FollowResponseClass;
import com.landagmail.josemaria.humansearchinc.Tasks.POST.POJO.ResponseClasses.FriendResponseClass;
import com.landagmail.josemaria.humansearchinc.Tasks.POST.POJO.ResponseClasses.MyQuestionFetchResponseClass;
import com.landagmail.josemaria.humansearchinc.Tasks.POST.POJO.ResponseClasses.ProfileResponseClass;
import com.landagmail.josemaria.humansearchinc.Tasks.POST.Queries.PostHelperClass;
import com.landagmail.josemaria.humansearchinc.Utils.CommonItems;
import com.landagmail.josemaria.humansearchinc.Utils.HashTagHelper.TagFetch.GetTags;
import com.rockerhieu.emojicon.EmojiconGridFragment;
import com.rockerhieu.emojicon.EmojiconsFragment;
import com.rockerhieu.emojicon.emoji.Emojicon;
import com.sothree.slidinguppanel.SlidingUpPanelLayout;
import com.urbanairship.UAirship;

import net.yazeed44.imagepicker.model.ImageEntry;
import net.yazeed44.imagepicker.util.Picker;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class AskActivity extends AppCompatActivity implements EmojiconGridFragment.OnEmojiconClickedListener, EmojiconsFragment.OnEmojiconBackspaceClickedListener, Picker.PickListener, GoogleApiClient.OnConnectionFailedListener, GoogleApiClient.ConnectionCallbacks {

    private static final int REQUEST_CAMERA = 0;
    private static final int PLACE_AUTOCOMPLETE_REQUEST_CODE = 10;
    private Uri imageUri;
    private String filePath = "";
    private ImageView cameraImageView;
    private MultiAutoCompleteTextView inputTxt;
    boolean isEmojiconFragmentOpen = false;
    private RelativeLayout emojicons;
    private RelativeLayout cameraBtn;
    private SlidingUpPanelLayout sliding_layout;
    private SeekBar distanceSlider;
    private TextView radiusTxt;
    private GoogleMap map;
    private LatLng loc;
    private int radius = 1000;
    private CircleOptions circleOptions;
    private ArrayList<String> friends = new ArrayList<>();
    boolean areFriendsFetched = false;


    @Override
    protected void onStop() {
        super.onStop();
        if (mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
    }

    @Override
    public void onConnected(Bundle bundle) {
        // DO NOTHING
    }

    @Override
    public void onConnectionSuspended(int i) {
        // DO NOTHING
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        L.m(getString(R.string.connectionFailed));
    }

    private GoogleApiClient mGoogleApiClient;

    @Override
    protected void onStart() {
        super.onStart();
        Dexter.checkPermission(new PermissionListener() {
            @Override
            public void onPermissionGranted(PermissionGrantedResponse response) {
                mGoogleApiClient.connect();
            }

            @Override
            public void onPermissionDenied(PermissionDeniedResponse response) {
                L.m(getString(R.string.locationPermissionDenied));
            }

            @Override
            public void onPermissionRationaleShouldBeShown(PermissionRequest permission, PermissionToken token) {
                token.continuePermissionRequest();
            }
        }, Manifest.permission.ACCESS_FINE_LOCATION);
    }

    private void autoCompleteIntent() {
        try {
            Intent intent =
                    new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_FULLSCREEN)
                            .build(this);
            startActivityForResult(intent, PLACE_AUTOCOMPLETE_REQUEST_CODE);
        } catch (GooglePlayServicesRepairableException e) {
            GooglePlayServicesUtil
                    .getErrorDialog(e.getConnectionStatusCode(), this, 0);
        } catch (GooglePlayServicesNotAvailableException e) {
            // Handle the exception
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ask);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(null);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        fetch_friends_list();

        mGoogleApiClient = new GoogleApiClient
                .Builder(this)
                .addApi(Places.GEO_DATA_API)
                .addApi(Places.PLACE_DETECTION_API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();

        ProgressDialog dialog = new ProgressDialog(this);
        dialog.setIndeterminate(true);
        dialog.setMessage(getString(R.string.loading));

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        if (MainApplication.isLocationSet()) {
            loc = new LatLng(MainApplication.latitude, MainApplication.longitude);
        }

        sliding_layout = (SlidingUpPanelLayout) findViewById(R.id.sliding_layout);
        sliding_layout.setPanelState(SlidingUpPanelLayout.PanelState.EXPANDED);

        sliding_layout.setPanelSlideListener(new SlidingUpPanelLayout.PanelSlideListener() {
            @Override
            public void onPanelSlide(View panel, float slideOffset) {

            }

            @Override
            public void onPanelCollapsed(View panel) {
                map.getUiSettings().setAllGesturesEnabled(true);
            }

            @Override
            public void onPanelExpanded(View panel) {
                map.getUiSettings().setAllGesturesEnabled(false);
            }

            @Override
            public void onPanelAnchored(View panel) {

            }

            @Override
            public void onPanelHidden(View panel) {

            }
        });

        inputTxt = (MultiAutoCompleteTextView) findViewById(R.id.inputTxt);
        inputTxt.setThreshold(1);
        inputTxt.setTokenizer(new MultiAutoCompleteTextView.Tokenizer() {

            @Override
            public CharSequence terminateToken(CharSequence text) {
                int i = text.length();

                while (i > 0 && text.charAt(i - 1) == ' ') {
                    i--;
                }

                if (i > 0 && text.charAt(i - 1) == ' ') {
                    return text;
                } else {
                    if (text instanceof Spanned) {
                        SpannableString sp = new SpannableString(text + " ");
                        TextUtils.copySpansFrom((Spanned) text, 0, text.length(), Object.class, sp, 0);
                        return sp;
                    } else {
                        return text + " ";
                    }
                }
            }

            @Override
            public int findTokenStart(CharSequence text, int cursor) {
                int i = cursor;

                while (i > 0 && text.charAt(i - 1) != '@') {
                    i--;
                }
                if (i < 1 || text.charAt(i - 1) != '@') {
                    return cursor;
                }
                return i;
            }

            @Override
            public int findTokenEnd(CharSequence text, int cursor) {
                int i = cursor;
                int len = text.length();

                while (i < len) {
                    if (text.charAt(i) == ' ') {
                        return i;
                    } else {
                        i++;
                    }
                }

                return len;
            }
        });

        inputTxt.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                Layout layout = inputTxt.getLayout();
                int pos = inputTxt.getSelectionStart();
                int line = layout.getLineForOffset(pos);
                int baseline = layout.getLineBaseline(line);

                int bottom = inputTxt.getHeight();

                inputTxt.setDropDownVerticalOffset(baseline - bottom);

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        ImageButton emojiBtn = (ImageButton) findViewById(R.id.emojiBtn);
        emojicons = (RelativeLayout) findViewById(R.id.emojicons);

        cameraBtn = (RelativeLayout) findViewById(R.id.cameraBtn);
        cameraBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PopupMenu popup = new PopupMenu(AskActivity.this, v);
                MenuInflater inflater = popup.getMenuInflater();
                inflater.inflate(R.menu.menu_profile_picture, popup.getMenu());
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        int id = item.getItemId();
                        if (id == R.id.action_take_picture) {
                            requestForCameraPermission();
                            return true;
                        }
                        if (id == R.id.action_choose_image) {

                            Dexter.checkPermission(new PermissionListener() {
                                @Override
                                public void onPermissionGranted(PermissionGrantedResponse response) {
                                    pickImage();
                                }

                                @Override
                                public void onPermissionDenied(PermissionDeniedResponse response) {
                                    L.m(getString(R.string.storagePermissionDenied));
                                }

                                @Override
                                public void onPermissionRationaleShouldBeShown(PermissionRequest permission, PermissionToken token) {
                                    token.continuePermissionRequest();
                                }
                            }, Manifest.permission.READ_EXTERNAL_STORAGE);
                            return true;
                        }
                        if (id == R.id.action_remove_picture) {
                            cameraBtn.setBackgroundResource(R.color.DimGray);
                            cameraImageView.setImageResource(R.drawable.ic_action_image_photo_camera);
                            if (filePath != null) {
                                filePath = "";
                            }
                            if (bitmap != null) {
                                bitmap = null;
                            }
                            return true;
                        }
                        return false;
                    }
                });
                popup.show();
            }
        });

        cameraImageView = (ImageView) findViewById(R.id.cameraImageView);
        Button publishBtn = (Button) findViewById(R.id.publishBtn);

        publishBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (inputTxt.getText() != null && inputTxt.getText().length() != 0) {

                    if (isOnline()) {
                        if (loc != null) {
                            String questionID = UUID.randomUUID().toString();

                            List<String> hashTags = new ArrayList<>();
                            for (String hashtag : GetTags.getHashTags(inputTxt.getText().toString())) {
                                hashTags.add(hashtag.toLowerCase());
                            }

                            List<String> targetUsers = GetTags.getCallouts(inputTxt.getText().toString());

                            if (bitmap != null) {
                                filePath = ImageUtility.saveAlreadyCroppedPicture(AskActivity.this, bitmap);
                            }

                            if (!getFilePath().equals("")) {

                                PostHelperClass.CartoDBQueries.insert_questions(loc.latitude, loc.longitude, inputTxt.getText().toString(), questionID, Profile.getCurrentProfile().getId(), getFilePath(), radius, targetUsers, hashTags);
                                cameraBtn.setBackgroundResource(R.color.DimGray);
                                cameraImageView.setImageResource(R.drawable.ic_action_image_photo_camera);
                                filePath = "";
                                File file = new File(getSelectedImagePath());
                                boolean deleted = file.delete();
                                Log.i("Temp_file", String.valueOf(deleted));

                            } else {
                                PostHelperClass.CartoDBQueries.insert_questions(loc.latitude, loc.longitude, inputTxt.getText().toString(), questionID, Profile.getCurrentProfile().getId(), null, radius, targetUsers, hashTags);

                            }
                            inputTxt.setText("");
                        } else {
                            L.m(getString(R.string.gettingLocation));
                        }

                    } else {
                        Snackbar.make(findViewById(android.R.id.content), getString(R.string.noInternetConnection), Snackbar.LENGTH_LONG)
                                .setAction(getString(R.string.connectToInternet), new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        startActivity(new Intent(Settings.ACTION_WIFI_SETTINGS));
                                    }
                                })
                                .setActionTextColor(Color.RED)
                                .show();
                    }
                } else {
                    Toast.makeText(AskActivity.this,
                            R.string.askFirstAsk,
                            Toast.LENGTH_SHORT).show();
                }


            }
        });

        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.emojicons, EmojiconsFragment.newInstance(true), "EMOJICON")
                .commit();

        inputTxt.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                if (isEmojiconFragmentOpen) {
                    hideEmojiconFragment();

                } else {
                    // DO NOTHING SPECIAL
                }
                return false;
            }
        });

        emojiBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (isEmojiconFragmentOpen) {
                    isEmojiconFragmentOpen = false;
                    hideEmojiconFragment();

                } else {
                    if (inputTxt.isFocused()) {
                        View view = AskActivity.this.getCurrentFocus();
                        if (view != null) {
                            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                        }
                        isEmojiconFragmentOpen = true;
                        showEmojiconFragment();
                    } else {
                        isEmojiconFragmentOpen = true;
                        showEmojiconFragment();
                    }
                }
            }
        });

        radiusTxt = (TextView) findViewById(R.id.radiusTxt);

        distanceSlider = (SeekBar) findViewById(R.id.distanceSlider);
        distanceSlider.setMax(3000);
        distanceSlider.setProgress(radius);
        distanceSlider.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                radiusTxt.setText(progress + " m");
                radius = progress;
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                if (radius < 200) {
                    distanceSlider.setProgress(200);
                    radiusTxt.setText(200 + " m");
                }

                map.clear();
                circleOptions = new CircleOptions()
                        .center(loc)
                        .radius(radius)
                        .fillColor(Color.parseColor("#882cadff"))
                        .strokeColor(Color.TRANSPARENT)
                        .strokeWidth(0);
                map.addCircle(circleOptions);
                map.addMarker(new MarkerOptions().position(loc));
            }
        });

        map = ((SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map)).getMap();

        map.getUiSettings().setAllGesturesEnabled(false);

        if (map != null) {
            if (loc != null) {
                circleOptions = new CircleOptions()
                        .center(loc)
                        .radius(radius)
                        .fillColor(Color.parseColor("#882cadff"))
                        .strokeColor(Color.TRANSPARENT)
                        .strokeWidth(0);

                map.addCircle(circleOptions);
                Marker marker = map.addMarker(new MarkerOptions().position(loc));
                LatLng center = marker.getPosition();

                CameraPosition cameraPosition = new CameraPosition.Builder()
                        .target(center)
                        .zoom(15)
                        .build();
                map.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

            }
        }

        map.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {

                if (sliding_layout.getPanelState() == SlidingUpPanelLayout.PanelState.EXPANDED) {
                    sliding_layout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
                } else {
                    loc = latLng;
                    map.clear();
                    circleOptions = new CircleOptions()
                            .center(loc)
                            .radius(radius)
                            .fillColor(Color.parseColor("#882cadff"))
                            .strokeColor(Color.TRANSPARENT)
                            .strokeWidth(0);

                    map.addCircle(circleOptions);
                    map.addMarker(new MarkerOptions().position(latLng));
                }
            }
        });
        map.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                loc = marker.getPosition();
                return false;
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_ask, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
            case R.id.action_search:
                autoCompleteIntent();
                break;
        }
        return true;
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (Integer.parseInt(android.os.Build.VERSION.SDK) > 5
                && keyCode == KeyEvent.KEYCODE_BACK
                && event.getRepeatCount() == 0) {
            Log.d("CDA", "onKeyDown Called");
            onBackPressed();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public void onBackPressed() {
        Log.d("CDA", "onBackPressed Called");
        if (isEmojiconFragmentOpen) {
            isEmojiconFragmentOpen = false;
            hideEmojiconFragment();
        } else if (sliding_layout.getPanelState() == SlidingUpPanelLayout.PanelState.COLLAPSED) {
            sliding_layout.setPanelState(SlidingUpPanelLayout.PanelState.EXPANDED);
        } else {
            super.onBackPressed();
        }
    }

    private void showEmojiconFragment() {
        emojicons.setVisibility(View.VISIBLE);
    }

    private void hideEmojiconFragment() {
        emojicons.setVisibility(View.GONE);
    }

    @Override
    public void onEmojiconClicked(Emojicon emojicon) {
        EmojiconsFragment.input(inputTxt, emojicon);
    }

    @Override
    public void onEmojiconBackspaceClicked(View v) {
        EmojiconsFragment.backspace(inputTxt);
    }

    public void requestForCameraPermission() {
        Dexter.checkPermissions(new MultiplePermissionsListener() {
            @Override
            public void onPermissionsChecked(MultiplePermissionsReport report) {

                if (report.areAllPermissionsGranted()) {
                    Intent startCustomCameraIntent = new Intent(AskActivity.this, CameraActivity.class);
                    startActivityForResult(startCustomCameraIntent, REQUEST_CAMERA);
                }
            }

            @Override
            public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                token.continuePermissionRequest();
            }
        }, Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode != RESULT_OK) return;

        if (requestCode == REQUEST_CAMERA) {
            imageUri = data.getData();

            filePath = getRealPathFromURI(String.valueOf(imageUri));
            cameraImageView.setImageBitmap(decodeFile(filePath));
            cameraBtn.setBackgroundColor(Color.WHITE);

        }
        if (requestCode == PLACE_AUTOCOMPLETE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                Place place = PlaceAutocomplete.getPlace(this, data);
                goToLocation(place);
                Log.i("PLACE_TAG", "Place:" + place.toString());
            } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                Status status = PlaceAutocomplete.getStatus(this, data);
                Log.i("PLACE_TAG", status.getStatusMessage());
            } else if (requestCode == RESULT_CANCELED) {

            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void goToLocation(Place place) {

        loc = place.getLatLng();
        map.clear();

        circleOptions = new CircleOptions()
                .center(loc)
                .radius(radius)
                .fillColor(Color.parseColor("#882cadff"))
                .strokeColor(Color.TRANSPARENT)
                .strokeWidth(0);

        map.addCircle(circleOptions);
        Marker marker = map.addMarker(new MarkerOptions().position(loc));
        LatLng center = marker.getPosition();

        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(center)
                .zoom(15)
                .build();
        map.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
    }

    private String getRealPathFromURI(String contentURI) {
        Uri contentUri = Uri.parse(contentURI);
        Cursor cursor = getContentResolver().query(contentUri, null, null, null, null);
        if (cursor == null) {
            return contentUri.getPath();
        } else {
            cursor.moveToFirst();
            int index = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            return cursor.getString(index);
        }
    }

    public Bitmap decodeFile(String path) {
        try {
            // Decode image size
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            BitmapFactory.decodeFile(path, o);
            // The new size we want to scale to
            final int REQUIRED_SIZE = 500;

            // Find the correct scale value. It should be the power of 2.
            int scale = 1;
            while (o.outWidth / scale / 2 >= REQUIRED_SIZE && o.outHeight / scale / 2 >= REQUIRED_SIZE)
                scale *= 2;

            // Decode with inSampleSize
            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize = scale;
            return BitmapFactory.decodeFile(path, o2);
        } catch (Throwable e) {
            e.printStackTrace();
        }
        return null;
    }

    public String getSelectedImagePath() {
        return "";
    }

    public String getFilePath() {
        return filePath;
    }

    private boolean isOnline() {
        ConnectivityManager cm =
                (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }

    Bitmap bitmap = null;

    @Override
    public void onPickedSuccessfully(final ArrayList<ImageEntry> images) {
        bitmap = ImageUtility.cropPicture(this, decodeFile(images.get(0).path));
        cameraImageView.setImageBitmap(bitmap);
    }

    @Override
    public void onCancel() {

    }

    private void pickImage() {

        new Picker.Builder(this, this)
                .setPickMode(Picker.PickMode.SINGLE_IMAGE)
                .setAlbumNameTextColor(Color.WHITE)
                .setAlbumImagesCountTextColor(Color.WHITE)
                .setDoneFabIconTintColor(Color.WHITE)
                .disableCaptureImageFromCamera()
                .build()
                .startActivity();
    }

    public void fetch_friends_list() {
        JsonArrayRequest request = new JsonArrayRequest(Request.Method.POST, Constants.FETCH_FOLLOWING, Profile.getCurrentProfile().getId(), new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                sendResults(Constants.FETCH_FOLLOWING, response.toString());
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                try {
                    Log.i("info", error.getLocalizedMessage());
                } catch (Exception e) {
                    Log.i("info", "Couldn't print error message");
                }
            }
        });
        request.setRetryPolicy(new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        MainApplication.getInstance().addToRequestQueue(request);
    }

    public void sendResults(String result, String data) {
        switch (result) {
            case Constants.FETCH_FOLLOWING:
                ArrayList<FriendResponseClass> friendResponse = new Gson().fromJson(data, new TypeToken<ArrayList<FriendResponseClass>>() {
                }.getType());

                for (FriendResponseClass friend : friendResponse) {
                    friends.add(friend.getName());
                }
                inputTxt.setAdapter(new ArrayAdapter<>(AskActivity.this, android.R.layout.simple_dropdown_item_1line, friends));
                areFriendsFetched = true;
                break;
        }
    }
}
