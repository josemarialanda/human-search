package com.landagmail.josemaria.humansearchinc.adapters;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.facebook.Profile;
import com.landagmail.josemaria.humansearchinc.Activities.AnswerScreenActivity;
import com.landagmail.josemaria.humansearchinc.Activities.ZoomInActivity;
import com.landagmail.josemaria.humansearchinc.Constants;
import com.landagmail.josemaria.humansearchinc.R;
import com.landagmail.josemaria.humansearchinc.Tasks.POST.POJO.ResponseClasses.FetchResponseClass;
import com.landagmail.josemaria.humansearchinc.Tasks.POST.Queries.PostHelperClass;
import com.landagmail.josemaria.humansearchinc.Utils.CustomViews.RoundImageView.RoundedImageView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class AnswerScreenAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int TYPE_HEADER = 0;
    private static final int TYPE_ITEM = 1;
    private String image;

    private ArrayList<FetchResponseClass> answersArrayList = new ArrayList<>();
    AnswerScreenActivity context;

    public AnswerScreenAdapter(AnswerScreenActivity context, String image) {
        this.context = context;
        this.image = image;
    }

    public void setData(ArrayList<FetchResponseClass> list) {
        answersArrayList = list;
        notifyDataSetChanged();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == TYPE_HEADER) {
            View v;
            v = LayoutInflater.from(parent.getContext()).inflate(R.layout.section, parent, false);
            return new HeaderViewHolder(v);
        } else if (viewType == TYPE_ITEM) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.answers_list_view, parent, false);
            return new GenericViewHolder(v);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof HeaderViewHolder) {
            HeaderViewHolder headerHolder = (HeaderViewHolder) holder;

            if (image != null) {
                Picasso.with(context)
                        .load(image)
                        .fit()
                        .centerCrop()
                        .into(headerHolder.picture);

                headerHolder.picture.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(context, ZoomInActivity.class);
                        intent.putExtra("IMAGE_URL", image);
                        context.startActivity(intent);
                    }
                });
            }

        } else if (holder instanceof GenericViewHolder) {
            final FetchResponseClass currentItem = getItem(position - 1);
            GenericViewHolder genericViewHolder = (GenericViewHolder) holder;

            if (!currentItem.getProfilePicture().equals(Constants.NO_IMAGE)) {
                Picasso.with(context)
                        .load(currentItem.getProfilePicture())
                        .fit()
                        .centerCrop()
                        .into(genericViewHolder.profilePicture);
            } else if (currentItem.getProfilePicture().equals(Constants.NO_IMAGE)) {
                genericViewHolder.profilePicture.setImageDrawable(context.getResources().getDrawable(R.drawable.profile_image));
            }

            genericViewHolder.distanceTxt.setText(currentItem.getDistance() + " m");

            genericViewHolder.usernameTxtView.setText(currentItem.getName());
            genericViewHolder.usernameTxtView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    if (currentItem.getUserID().equals(Profile.getCurrentProfile().getId())) {

                        // DO NOTHING
                    } else {
                        showAlertDialog(position);
                    }
                    return false;

                }
            });
            genericViewHolder.timeSentTxtView.setText(currentItem.getDate_time());
            genericViewHolder.answerTextTxtView.setText(currentItem.getText());
        }
    }

    private void showAlertDialog(final int position) {
        final AlertDialog alertDialog = new AlertDialog.Builder(context).create();
        alertDialog.setMessage(context.getString(R.string.followConfirmation) + answersArrayList.get(position).getName());
        alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, context.getString(R.string.follow) + answersArrayList.get(position).getName(), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                String friendUserId = answersArrayList.get(position).getUserID();
                String userId = Profile.getCurrentProfile().getId();
                String name = answersArrayList.get(position).getName();

                PostHelperClass.FriendsQueries.follow(friendUserId, userId);

            }
        });
        alertDialog.setButton(DialogInterface.BUTTON_NEGATIVE, context.getString(R.string.cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // DO NOTHING
            }
        });
        alertDialog.show();
    }

    //    need to override this method
    @Override
    public int getItemViewType(int position) {
        if (isPositionHeader(position)) {
            return TYPE_HEADER;
        }
        return TYPE_ITEM;
    }

    private boolean isPositionHeader(int position) {
        return position == 0;
    }

    @Override
    public int getItemCount() {
        return answersArrayList.size() + 1;
    }

    private FetchResponseClass getItem(int position) {
        return answersArrayList.get(position);
    }

    class HeaderViewHolder extends RecyclerView.ViewHolder {

        ImageView picture;

        public HeaderViewHolder(View itemView) {
            super(itemView);
            this.picture = (ImageView) itemView.findViewById(R.id.picture);
        }
    }

    class GenericViewHolder extends RecyclerView.ViewHolder {

        TextView usernameTxtView;
        TextView timeSentTxtView;
        TextView answerTextTxtView;
        TextView distanceTxt;
        RoundedImageView profilePicture;

        public GenericViewHolder(View itemView) {
            super(itemView);

            this.usernameTxtView = (TextView) itemView.findViewById(R.id.usernameTxtView);
            this.timeSentTxtView = (TextView) itemView.findViewById(R.id.timeSentTxtView);
            this.answerTextTxtView = (TextView) itemView.findViewById(R.id.answerTextTxtView);
            this.distanceTxt = (TextView) itemView.findViewById(R.id.distanceTxt);
            this.profilePicture = (RoundedImageView) itemView.findViewById(R.id.profilePicture);

        }
    }
}