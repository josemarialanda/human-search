package com.landagmail.josemaria.humansearchinc.Dialogs;

import android.app.Activity;

import android.content.Context;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import com.landagmail.josemaria.humansearchinc.R;
import com.landagmail.josemaria.humansearchinc.Utils.CustomViews.PickerView;

import java.util.ArrayList;
import java.util.List;

public class TimePickerDialog extends AlertDialog.Builder {

    Context context;
    List<String> hours;
    List<String> minutes;
    PickerView hour_pv;
    PickerView minute_pv;
    public int hoursTime = 1;
    public int minutesTime = 30;

    public TimePickerDialog(Context context, Activity activity) {
        super(context);
        context = activity;
        this.setTitle("Establece la hora");
        LayoutInflater inflater = activity.getLayoutInflater();
        View dialoglayout = inflater.inflate(R.layout.time_picker_layout, null);
        this.setView(dialoglayout);

        hours = new ArrayList<>();
        minutes = new ArrayList<>();
        for (int i = 1; i < 13; i++) {
            if (i > 9) {
                hours.add(String.valueOf(i));
            } else {
                hours.add("0" + i);
            }
        }
        for (int i = 0; i < 60; i++) {
            minutes.add(i < 10 ? "0" + i : "" + i);
        }

        hour_pv = (PickerView) dialoglayout.findViewById(R.id.hour_pv);
        minute_pv = (PickerView) dialoglayout.findViewById(R.id.minute_pv);

        hour_pv.setData(hours);
        hour_pv.setOnSelectListener(new PickerView.onSelectListener() {

            @Override
            public void onSelect(String text) {
                hoursTime = Integer.parseInt(text);
            }
        });
        minute_pv.setData(minutes);
        minute_pv.setOnSelectListener(new PickerView.onSelectListener() {

            @Override
            public void onSelect(String text) {
                minutesTime = Integer.parseInt(text);
            }
        });
        hour_pv.setSelected(0);
    }
}


/*
private void showTimeDialog(final int time) {
        final TimePickerDialog timePickerDialog = new TimePickerDialog(ProfileActivity.this, ProfileActivity.this);

        timePickerDialog.setPositiveButton("Set time", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (time) {
                    case MORNING:
                        morningHours = timePickerDialog.hoursTime;
                        morningMinutes = timePickerDialog.minutesTime;
                        morningTimeTxt.setText(morningHours + ":" + morningMinutes + " AM");
                        break;
                    case EVENING:
                        eveningHours = timePickerDialog.hoursTime;
                        eveningMinutes = timePickerDialog.minutesTime;
                        eveningTimeTxt.setText(eveningHours + ":" + eveningMinutes + " PM");
                        break;
                }

            }
        });
        timePickerDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // DO NOTHING
            }
        });
        timePickerDialog.create().show();
    }
 */