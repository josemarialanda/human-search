package com.landagmail.josemaria.humansearchinc.Tasks.POST.Queries.Interfaces;


import com.landagmail.josemaria.humansearchinc.Tasks.POST.POJO.RequestClasses.FriendInput;

public interface FriendsQueries {

    Integer follow(FriendInput Input);

    Integer unfollow(FriendInput Input);

    void check_if_friend(FriendInput Input);

    void find_friends(FriendInput Input);

}
